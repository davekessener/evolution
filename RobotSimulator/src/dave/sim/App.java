package dave.sim;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

import dave.evo.common.NeuralNet;
import dave.evo.eval.robot.FoodWorld;
import dave.evo.eval.robot.MazeWorld;
import dave.evo.eval.robot.NeuralNetAI;
import dave.evo.eval.robot.RobotController;
import dave.evo.eval.robot.RobotControllerBase;
import dave.evo.eval.robot.SimpleRobotAI;
import dave.evo.eval.robot.SimulationOptions;
import dave.evo.eval.robot.World;
import dave.json.JSON;
import dave.json.JsonObject;
import dave.json.JsonUtils;
import dave.json.SevereIOException;
import dave.util.Actor;
import dave.util.MathUtils;
import dave.util.Utils;
import dave.util.XoRoRNG;
import dave.util.config.Configuration;
import dave.util.math.Vec2;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.transform.Affine;
import javafx.scene.transform.Transform;
import javafx.stage.Stage;

public class App implements Actor
{
	private final Pane mRoot;
	private final Canvas mView;
	private final Queue<World> mWorlds;
	
	public App()
	{
		mRoot = new Pane();
		mView = new Canvas();
		
		mRoot.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
		
		Configuration<SimulationOptions> config = new Configuration<>(SimulationOptions.class);
		SimulationOptions opt = config.proxy();
		
		List<Vec2> food = Arrays.asList(new Vec2(2, 2), new Vec2(18, 2), new Vec2(18, 18), new Vec2(18, 2), new Vec2(2, 2));
		
		mWorlds = new LinkedList<>();
		
		mWorlds.add(new FoodWorld(getAI(), 20, 20, food, opt));
		mWorlds.add(new MazeWorld(getAI(), opt.world_size(), opt.world_size(), new XoRoRNG(), opt));
		
		mRoot.widthProperty().addListener(e -> redraw());
		mRoot.heightProperty().addListener(e -> redraw());
		
		mRoot.getChildren().add(mView);
	}
	
	private static RobotController getAI()
	{
		File best = new File("snapshot.json");
		
		if(best.exists()) try
		{
			System.out.println("Loading from file ...");
			
			JsonObject json = (JsonObject) JsonUtils.fromFile(best);
			
			return new NeuralNetAI((NeuralNet) JSON.deserialize(json.get("neural-net")));
//			
//			Configuration<Options> c = new Configuration<>(Options.class);
//			Genome g = Genome.load(JsonUtils.fromFile(best));
//			SubstrateGenerator f = new SubstrateGenerator(NeuralNetAI.ROBOT_IO, c.proxy());
//			CPPNN cppnn = new CPPNN(BasicNeuralNet.build(g.chromosomes().findFirst().get()));
//			
//			return new NeuralNetAI(BasicNeuralNet.build(f.apply(cppnn).paint(HypercubeNeuralNetInstantiator.scalePainter(cppnn, c.proxy()))));
		}
		catch(IOException e)
		{
			throw new SevereIOException(e);
		}
		else
		{
			System.out.println("Using dummy AI.");
			
			return new SimpleRobotAI();
		}
	}
	
	public Parent ui()
	{
		return mRoot;
	}
	
	public void advance()
	{
		if(!mWorlds.isEmpty())
		{
			mWorlds.poll();
		}
		
		redraw();
	}
	
	private World world()
	{
		return mWorlds.peek();
	}
	
	private int mCounter = 0;
	
	public void tick()
	{
		if(mWorlds.isEmpty())
			return;
		
		if(world().done())
		{
			advance();
		}

		if(mWorlds.isEmpty())
			return;
		
		world().update();
		++mCounter;
		
		RobotControllerBase ai = (RobotControllerBase) world().robot().ai;
		
		String input = "(" + DoubleStream.of(ai.distances()).mapToObj(v -> String.format("%.2f", v)).collect(Collectors.joining(", ")) + ")";
		String output = String.format("(%.2f, %.2f, %.2f)", ai.left(), ai.forward(), ai.right());
		System.out.println("@" + mCounter + ": " + input + " -> " + output + ": " + world().fitness());
		
		redraw();
	}
	
	private void redraw()
	{
		if(world() == null)
			return;
		
		int l = (int) Math.min(mRoot.getWidth(), mRoot.getHeight());
		
		if(l <= 0)
			return;
		
		mView.setWidth(l);
		mView.setHeight(l);
		
		double scale = l / world().terrain().width();
		
		GraphicsContext gc = mView.getGraphicsContext2D();
		
		gc.setTransform(Utils.tap(new Affine(Transform.scale(1, -1)), t -> t.appendTranslation(0, -l)));
		
		gc.setFill(Color.WHITE);
		gc.fillRect(0, 0, l, l);
		
		gc.setFill(Color.DARKGRAY);
		world().terrain().blocks().forEach(aabb -> gc.fillRect(aabb.min().x * scale, aabb.min().y * scale, aabb.width() * scale, aabb.height() * scale));

		Vec2 p = world().robot().position.get();
		Vec2 f = world().robot().forward.get();
		double r = world().robot().radius;
		double[] distances = ((RobotControllerBase) world().robot().ai).distances();
		
		for(int i = 0 ; i < 5 ; ++i)
		{
			Vec2 d = MathUtils.rotate2D(f, (Math.PI / 4) * (i - 2));
			Vec2 o = p.add(d.scale(r));
			
			strokeLine(gc, o, o.add(d.scale(world().options().view_distance() * distances[i])), scale, Color.LIGHTGRAY);
		}

		fillCircle(gc, p, r, scale, Color.WHITE);
		strokeCircle(gc, p, r, scale, Color.BLACK);
		strokeLine(gc, p, p.add(f.scale(r)), scale, Color.BLACK);
		
		if(world() instanceof FoodWorld)
		{
			FoodWorld w = (FoodWorld) world();
			
			w.getCurrentFood().ifPresent(v -> fillCircle(gc, v, r/2, scale, Color.GREEN));
			
			for(int i = 0 ; i < 4 ; ++i)
			{
				if(i != w.getActiveSensor())
					continue;
				
				Vec2 v = p.add(MathUtils.rotate2D(f.scale(r), i * (2 * Math.PI) / 4));
				
				fillCircle(gc, v, r / 10, scale, Color.RED);
			}
		}
	}
	
	private static void strokeLine(GraphicsContext gc, Vec2 v1, Vec2 v2, double s, Paint c)
	{
		gc.setStroke(c);
		gc.strokeLine(v1.x * s, v1.y * s, v2.x * s, v2.y * s);
	}

	private static void fillCircle(GraphicsContext gc, Vec2 p, double r, double s, Paint c)
	{
		gc.setFill(c);
		gc.fillOval((p.x - r) * s, (p.y - r) * s, (2 * r) * s, (2 * r) * s);
	}

	private static void strokeCircle(GraphicsContext gc, Vec2 p, double r, double s, Paint c)
	{
		gc.setStroke(c);
		gc.strokeOval((p.x - r) * s, (p.y - r) * s, (2 * r) * s, (2 * r) * s);
	}
	
	public static void run(Stage primary)
	{
		App app = new App();
		Scene scene = new Scene(app.ui(), 800, 600);
		
		primary.setTitle("Robot Simulator");
		primary.setScene(scene);
		
		scene.setOnKeyPressed(e -> {
			switch(e.getCode())
			{
				case SPACE:
					app.tick();
					break;
					
				case ENTER:
					app.advance();
					break;
					
				default:
			}
		});
		
		primary.show();
		
		app.start();
	}
}
