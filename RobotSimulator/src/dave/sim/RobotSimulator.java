package dave.sim;

import dave.evo.common.Library;
import dave.util.ShutdownService;
import dave.util.ShutdownService.Priority;
import dave.util.log.LogBase;
import dave.util.log.LogSink;
import dave.util.log.service.LogService;
import javafx.application.Application;
import javafx.stage.Stage;

public class RobotSimulator extends Application
{
	public static void main(String[] args)
	{
		LogBase.INSTANCE.registerSink(e -> true, LogSink.build());
		
		LogBase.INSTANCE.start();
		
		LogService.INSTANCE.initialize(Library.DEBUG);
		
		ShutdownService.INSTANCE.register(Priority.LAST, LogBase.INSTANCE::stop);
		
		try
		{
			launch(args);
		}
		finally
		{
			ShutdownService.INSTANCE.shutdown();
		}
	}

	@Override
	public void start(Stage primary) throws Exception
	{
		App.run(primary);
	}
}
