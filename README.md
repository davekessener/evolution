# Optimierung rekurrenter neuronaler Netze mit genetischen Algorithmen der NEAT-Familie
### Daniel Kessener

Projekt im Order Evolution/

Ausfuehrbares JAR in release/ mit diversen start-scripts.

Usage:

```java -jar evolution.jar --script <sample_script.json>```

Highscore gibt den erreichten besten Fitness-Wert aller Populationen an.
Fitness ist ein Wert aus [0, 1] - ein Agent mit Fitness "1" gilt als perfekt.

