package dave;

import dave.nn.ui.App;
import javafx.application.Application;
import javafx.stage.Stage;

public class NeuralNetVisualizer extends Application
{
	public static void main(String[] args)
	{
		launch(args);
	}

	@Override
	public void start(Stage primary) throws Exception
	{
		App app = new App(primary);
		
		primary.setTitle(String.format("%s v%d", TITLE, VERSION));
		primary.show();
		
		app.start();
	}
	
	private static final String TITLE = "Neural Net Visualizer";
	private static final int VERSION = 1;
}
