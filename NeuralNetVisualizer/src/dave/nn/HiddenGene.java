package dave.nn;

import dave.json.JsonSerializable;
import dave.json.JsonSerializer;

@JsonSerializable
public class HiddenGene extends ConnectedGene
{
	@JsonSerializer({ "mID", "mActivation" })
	public HiddenGene(long id, Activation a)
	{
		super(id, a);
	}

	@Override
	public ConnectedNeuron create()
	{
		return new ConnectedNeuron(activation());
	}
	
	@Override
	public boolean equals(Object o)
	{
		return (o instanceof HiddenGene) && super.equals(o);
	}
}
