package dave.nn;

public class InputNeuron implements Neuron
{
	private double mValue = 0;
	
	public void set(double v)
	{
		mValue  = v;
	}
	
	@Override
	public double get()
	{
		return mValue;
	}
}
