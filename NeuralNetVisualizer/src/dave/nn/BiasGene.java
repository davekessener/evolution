package dave.nn;

import dave.json.JsonSerializable;
import dave.json.JsonSerializer;

@JsonSerializable
public class BiasGene extends BaseGene implements NeuronGene
{
	@JsonSerializer({ "mID" })
	public BiasGene(long id)
	{
		super(id);
	}
	
	@Override
	public BiasNeuron create()
	{
		return new BiasNeuron();
	}
}
