package dave.nn;

import dave.json.JsonSerializable;
import dave.json.JsonSerializer;

@JsonSerializable
public class LinkGene extends BaseGene
{
	private final long mFrom, mTo;
	private final double mWeight;
	
	@JsonSerializer({ "mFrom", "mTo", "mWeight", "mID" })
	public LinkGene(long from, long to, double weight, long id)
	{
		super(id);
		
		mFrom = from;
		mTo = to;
		mWeight = weight;
	}
	
	public long from() { return mFrom; }
	public long to() { return mTo; }
	public double weight() { return mWeight; }
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof LinkGene)
		{
			LinkGene g = (LinkGene) o;
			
			return super.equals(o) && g.mFrom == mFrom && g.mTo == mTo && g.mWeight == mWeight;
		}
		
		return false;
	}
}
