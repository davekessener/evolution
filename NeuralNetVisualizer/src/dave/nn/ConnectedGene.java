package dave.nn;

public abstract class ConnectedGene extends BaseGene implements NeuronGene
{
	private final Activation mActivation;
	
	protected ConnectedGene(long id, Activation a)
	{
		super(id);
		
		mActivation = a;
	}
	
	protected Activation activation() { return mActivation; }
	
	public abstract ConnectedNeuron create( );
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof ConnectedGene)
		{
			ConnectedGene g = (ConnectedGene) o;
			
			return super.equals(o) && g.mActivation.type() == mActivation.type();
		}
		
		return false;
	}
}
