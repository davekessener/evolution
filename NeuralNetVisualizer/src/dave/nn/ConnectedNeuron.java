package dave.nn;

import java.util.ArrayList;
import java.util.List;

public class ConnectedNeuron implements Neuron
{
	private final Activation mActivation;
	private final List<Connection> mConnections;

	public ConnectedNeuron(Activation a)
	{
		mActivation = a;
		mConnections = new ArrayList<>();
	}
	
	public void connect(double w, Neuron n)
	{
		mConnections.add(new Connection(n, w));
	}
	
	@Override
	public double get()
	{
		return mActivation.apply(mConnections.stream().mapToDouble(c -> c.weight * c.node.get()).sum());
	}
	
	private static class Connection
	{
		public final Neuron node;
		public final double weight;
		
		public Connection(Neuron node, double weight)
		{
			this.node = node;
			this.weight = weight;
		}
	}
}
