package dave.nn;

import dave.json.JsonSerializable;
import dave.json.JsonSerializer;

@JsonSerializable
public class InputGene extends BaseGene implements NeuronGene
{
	@JsonSerializer({ "mID" })
	public InputGene(long id)
	{
		super(id);
	}
	
	@Override
	public InputNeuron create()
	{
		return new InputNeuron();
	}
}
