package dave.nn;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dave.util.relay.Overloaded;
import dave.util.relay.Relay;
import dave.util.stream.StreamUtils;

public class Network
{
	private final List<InputNeuron> mInputs;
	private final List<OutputNeuron> mOutputs;
	
	private Network(List<InputNeuron> in, List<OutputNeuron> out)
	{
		mInputs = in;
		mOutputs = out;
	}
	
	public int inputs() { return mInputs.size(); }
	public int outputs() { return mOutputs.size(); }
	
	public double[] compute(double[] in)
	{
		if(in.length != mInputs.size())
			throw new IllegalArgumentException("Network sized " + mInputs.size() + " erroneously got" + in.length + " inputs!");
		
		mInputs.stream()
			.map(StreamUtils.stream_with_index())
			.forEach(e -> e.value.set(in[e.index]));
		
		return mOutputs.stream().mapToDouble(OutputNeuron::get).toArray();
	}
	
	public static class Builder
	{
		private final Relay mRelay;
		private final Map<Long, Neuron> mNodes;
		private final List<InputNeuron> mInputs;
		private final List<OutputNeuron> mOutputs;
		
		public Builder()
		{
			mRelay = new Relay(this);
			mNodes = new HashMap<>();
			mInputs = new ArrayList<>();
			mOutputs = new ArrayList<>();
		}
		
		public Builder add(Gene g)
		{
			mRelay.call(g);
			
			return this;
		}
		
		public Network build()
		{
			return new Network(mInputs, mOutputs);
		}
		
		@Overloaded
		private void addBias(BiasGene g)
		{
			mNodes.put(g.id(), g.create());
		}
		
		@Overloaded
		private void addInput(InputGene g)
		{
			InputNeuron in = g.create();
			
			mInputs.add(in);
			mNodes.put(g.id(), in);
		}
		
		@Overloaded
		private void addOutput(OutputGene g)
		{
			OutputNeuron out = g.create();
			
			mOutputs.add(out);
			mNodes.put(g.id(), out);
		}
		
		@Overloaded
		private void addHidden(HiddenGene g)
		{
			mNodes.put(g.id(), g.create());
		}
		
		@Overloaded
		private void addLink(LinkGene g)
		{
			Neuron from = mNodes.get(g.from());
			Neuron to = mNodes.get(g.to());
			
			if(from == null || to == null)
				throw new NullPointerException();
			
			((ConnectedNeuron) to).connect(g.weight(), from);
		}
	}
}
