package dave.nn;

import java.util.Arrays;
import java.util.stream.Stream;

import dave.json.JsonSerializable;
import dave.json.JsonSerializer;

@JsonSerializable
public class Genome
{
	private final Gene[] mGenes;
	
	@JsonSerializer({ "mGenes" })
	public Genome(Gene[] g)
	{
		mGenes = Arrays.copyOf(g, g.length);
	}
	
	public Stream<Gene> genes() { return Stream.of(mGenes); }
	
	public Network instantiate()
	{
		Network.Builder b = new Network.Builder();
		
		genes().forEach(b::add);
		
		return b.build();
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Genome)
		{
			Genome g = (Genome) o;
			
			if(g.mGenes.length == mGenes.length)
			{
				for(int i = 0 ; i < mGenes.length ; ++i)
				{
					if(!mGenes[i].equals(g.mGenes[i]))
						return false;
				}
				
				return true;
			}
		}
		
		return false;
	}
}
