package dave.nn.ui;

import dave.util.math.Vec2;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

public class LinkUI
{
	private final Node mFrom, mTo;
	private final Group mRoot;
	private final Line mLine;
	
	public LinkUI(Node from, Node to)
	{
		mFrom = from;
		mTo = to;
		mRoot = new Group();
		mLine = new Line();
		
		mFrom.translateXProperty().addListener($ -> redraw());
		mFrom.translateYProperty().addListener($ -> redraw());
		mTo.translateXProperty().addListener($ -> redraw());
		mTo.translateYProperty().addListener($ -> redraw());
		
		redraw();
	}
	
	public Vec2 from() { return new Vec2(mLine.getStartX(), mLine.getStartY()); }
	public Vec2 to() { return new Vec2(mLine.getEndX(), mLine.getEndY()); }
	
	public Node ui()
	{
		return mRoot;
	}
	
	private static Vec2 pos(Node n) { return new Vec2(n.getTranslateX(), n.getTranslateY()); }
	
	private void redraw()
	{
		Vec2 from = pos(mFrom);
		Vec2 to = pos(mTo);
		Vec2 dir = to.sub(from).normalized();
		
		from = from.add(dir.scale(NeuronUI.RADIUS));
		to = to.sub(dir.scale(NeuronUI.RADIUS));
		
		Vec2 center = to.add(dir.scale(-5));
		
		Circle c = new Circle(center.x, center.y, 5);

		mLine.setStartX(from.x);
		mLine.setStartY(from.y);
		mLine.setEndX(to.x);
		mLine.setEndY(to.y);
		
		mRoot.getChildren().clear();
		mRoot.getChildren().addAll(mLine, c);
	}
}
