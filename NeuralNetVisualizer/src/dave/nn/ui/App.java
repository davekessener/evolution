package dave.nn.ui;

import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class App
{
	private final GenomeEditor mGenomeEdit;
	
	public App(Stage primary)
	{
		mGenomeEdit = new GenomeEditor();
		
		BorderPane root = new BorderPane();
		
		root.setCenter(mGenomeEdit.ui());
		
		Scene scene = new Scene(root, 800, 600);
		
		primary.setScene(scene);
	}
	
	public void start()
	{
		mGenomeEdit.start();
	}
}
