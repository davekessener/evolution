package dave.nn.ui;

import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

public class InputNeuronUI extends NeuronUI
{
	public InputNeuronUI()
	{
		Polygon t = new Polygon();
		
		t.getPoints().addAll(new Double[] {
			-13.0, 33.0,
			 13.0, 33.0,
			 0.0, 18.0
		});
		
		t.setFill(Color.WHITE);
		t.setStroke(Color.BLACK);
		
		element().getChildren().add(0, t);
	}
}
