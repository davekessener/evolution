package dave.nn.ui;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class NeuronUI
{
	private final Group mUI;
	private final DoubleProperty mValue;
	
	public NeuronUI()
	{
		mUI = new Group();
		mValue = new SimpleDoubleProperty();
		
		Circle c = new Circle();
		
		c.setRadius(RADIUS);
		c.setFill(Color.WHITE);
		c.setStroke(Color.BLACK);
		
		Label lbl = new Label("0");
		
		lbl.translateXProperty().bind(lbl.widthProperty().multiply(-0.5));
		lbl.translateYProperty().bind(lbl.heightProperty().multiply(-0.5));
		
		mValue.addListener($ -> lbl.setText(String.format("%.2f", mValue.get())));
		
		mUI.getChildren().addAll(c, lbl);
	}
	
	public DoubleProperty valueProperty() { return mValue; }
	
	protected Group element() { return mUI; }
	
	public Node ui()
	{
		return mUI;
	}
	
	public static final double RADIUS = 25;
}
