package dave.nn.ui;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.text.Font;

public class BiasNeuronUI extends NeuronUI
{
	public BiasNeuronUI()
	{
		Node c = element().getChildren().get(0);
		Label lbl = new Label("B");
		
		lbl.setFont(Font.font(26));
		lbl.translateXProperty().bind(lbl.widthProperty().multiply(-0.5));
		lbl.translateYProperty().bind(lbl.heightProperty().multiply(-0.5));
		
		element().getChildren().clear();
		element().getChildren().addAll(c, lbl);
		
		valueProperty().set(1);
	}
}
