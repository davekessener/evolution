package dave.nn.ui;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class OutputNeuronUI extends NeuronUI
{
	public OutputNeuronUI()
	{
		Circle c = new Circle();
		
		c.setRadius(NeuronUI.RADIUS + 2);
		c.setFill(Color.WHITE);
		c.setStroke(Color.BLACK);
		
		element().getChildren().add(0, c);
	}
}
