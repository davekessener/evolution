package dave.nn.ui;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Stack;
import java.util.stream.Stream;

import dave.json.JSON;
import dave.json.PrettyPrinter;
import dave.nn.Activation;
import dave.nn.BiasGene;
import dave.nn.Gene;
import dave.nn.Genome;
import dave.nn.HiddenGene;
import dave.nn.InputGene;
import dave.nn.LinkGene;
import dave.nn.OutputGene;
import dave.util.SevereException;
import dave.util.math.Vec2;
import dave.util.relay.Overloaded;
import dave.util.relay.Relay;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;

@SuppressWarnings("unused")
public class GenomeEditor
{
	private final BorderPane mUI;
	private final Property<Genome> mGenome;
	private final Pane mCanvas;
	private final Group mRoot;
	private final List<Neuron> mNeurons;
	private final List<Link> mLinks;
	private final Property<Selectable> mSelected;
	private final Stack<Mode> mState;
	private final InfoPanel mInfo;
	private final ComboBox<Activation> mActivation;
	private long mNextID;
	
	public GenomeEditor()
	{
		mUI = new BorderPane();
		mGenome = new SimpleObjectProperty<>();
		mCanvas = new Pane();
		mRoot = new Group();
		mNeurons = new ArrayList<>();
		mLinks = new ArrayList<>();
		mSelected = new SimpleObjectProperty<>();
		mState = new Stack<>();
		mInfo = new InfoPanel();
		mActivation = createActivationComboBox();
		mNextID = 0;
		
		mCanvas.setOnMousePressed(this::mouseDown);
		mCanvas.setOnMouseDragged(this::mouseMove);
		mCanvas.setOnMouseReleased(this::mouseUp);
		
		mState.push(new DefaultMode());
		
		GridPane options = new GridPane();
		
		ComboBox<ModeSwitcher> mode = new ComboBox<>();

		mode.getItems().add(new ModeSwitcher("DRAG", DragMode.class));
		mode.getItems().add(new ModeSwitcher("INPUT", NeuronMode.class, InputNeuron.class));
		mode.getItems().add(new ModeSwitcher("HIDDEN", NeuronMode.class, HiddenNeuron.class));
		mode.getItems().add(new ModeSwitcher("OUTPUT", NeuronMode.class, OutputNeuron.class));
		mode.getItems().add(new ModeSwitcher("LINK", LinkMode.class));
		
		mode.getSelectionModel().select(0);
		mode.getSelectionModel().selectedItemProperty().addListener((v, o, n) -> n.fire());
		
		Button print = new Button("Print");
		
		print.setOnAction(e -> System.out.println(JSON.serialize(mGenome.getValue()).toString(new PrettyPrinter())));
		
		options.add(mode, 0, 0);
		options.add(mActivation, 1, 0);
		options.add(print, 2, 0);
		
		options.setPadding(new Insets(4, 4, 4, 4));
		options.setHgap(4);
		
		mSelected.addListener((v, o, n) -> {
			setSelectionColor(o, Color.BLACK);
			setSelectionColor(n, Color.BLUE);
			mInfo.update(n);
		});
		
		mUI.setTop(options);
		mUI.setRight(mInfo.ui());
		mUI.setCenter(mCanvas);
		mCanvas.getChildren().add(mRoot);
	}
	
	private static void setSelectionColor(Selectable s, Color p)
	{
		if(s != null)
		{
			Node o = s.ui();
			
			if(o instanceof Group)
			{
				((Group) o).getChildren().stream()
					.filter(c -> c instanceof Shape)
					.map(c -> (Shape) c)
					.forEach(c -> c.setStroke(p));
			}
		}
	}
	
	public void start()
	{
		mState.push(new DragMode());
		spawn(canvas().center(), new BiasNeuron());
	}
	
	public Property<Genome> genomeProperty() { return mGenome; }
	
	public Node ui()
	{
		return mUI;
	}
	
	private AABB canvas()
	{
		Bounds bb = mCanvas.getBoundsInParent();
		
		return new AABB(bb.getMinX(), bb.getMinY(), bb.getMaxX(), bb.getMaxY());
	}
	
	private Stream<Selectable> all()
	{
		return Stream.of(mNeurons, mLinks).flatMap(l -> l.stream());
	}
	
	private void spawn(Vec2 p, Neuron e)
	{
		Node ui = e.ui();

		ui.setTranslateX(p.x);
		ui.setTranslateY(p.y);
		
		mNeurons.add(e);
		mRoot.getChildren().add(ui);
		
		e.valueProperty().addListener($ -> recalculate());
	}
	
	private void connect(Neuron from, Neuron to)
	{
		if(from != to
			&& !mLinks.stream().anyMatch(l -> l.from() == from && l.to() == to)
			&& (to instanceof ConnectedNeuron))
		{
			Link l = new Link(mNextID++, from, (ConnectedNeuron) to);
			
			mLinks.add(l);
			mRoot.getChildren().add(l.ui());
			
			l.weightProperty().addListener($ -> recalculate());
			
			recalculate();
		}
	}
	
	private boolean mUpdating = false;
	private void recalculate()
	{
		if(!mUpdating)
		{
			mUpdating = true;

			mNeurons.forEach(Neuron::reset);
			mNeurons.forEach(Neuron::value);
			
			Genome g = assemble();
			
			if(!g.equals(mGenome.getValue()))
			{
				mGenome.setValue(g);
			}
			
			mUpdating = false;
		}
	}
	
	private Genome assemble()
	{
		List<Gene> genes = new ArrayList<>();
		
		all().forEach(n -> genes.add(n.gene()));
		
		return new Genome(genes.toArray(new Gene[0]));
	}
	
	private Vec2 mouse(MouseEvent e)
	{
		AABB bb = canvas();
		
		double x = e.getSceneX() - bb.x0;
		double y = e.getSceneY() - bb.y0;
		
		return new Vec2(x, y);
	}
	
	private Vec2 mouseInScene(MouseEvent e)
	{
		return mouse(e).sub(new Vec2(mRoot.getTranslateX(), mRoot.getTranslateY()));
	}
	
	private void mouseDown(MouseEvent e)
	{
		for(Mode m : mState)
		{
			if(m.mouseDown(e))
				break;
		}
	}
	
	private void mouseMove(MouseEvent e)
	{
		for(Mode m : mState)
		{
			if(m.mouseMove(e))
				break;
		}
	}
	
	private void mouseUp(MouseEvent e)
	{
		for(Mode m : mState)
		{
			if(m.mouseUp(e))
				break;
		}
	}

	private static interface Mode
	{
		public abstract boolean mouseDown(MouseEvent e);
		public abstract boolean mouseMove(MouseEvent e);
		public abstract boolean mouseUp(MouseEvent e);
	}
	
	private class ModeSwitcher
	{
		private final String mType;
		private final Class<? extends Mode> mClass;
		private final Object[] mArgs;
		private final Class<?>[] mTypes;
		
		protected ModeSwitcher(String type, Class<? extends Mode> c, Object ... a)
		{
			mType = type;
			mClass = c;
			mArgs = new Object[a.length + 1];
			mTypes = new Class[mArgs.length];
			
			mArgs[0] = GenomeEditor.this;
			for(int i = 0 ; i < a.length ; ++i)
			{
				mArgs[i + 1] = a[i];
			}
			
			for(int i = 0 ; i < mTypes.length ; ++i)
			{
				mTypes[i] = mArgs[i].getClass();
			}
		}
		
		public void fire()
		{
			try
			{
				Constructor<? extends Mode> c = mClass.getDeclaredConstructor(mTypes);
				
				c.setAccessible(true);
				
				Mode m = c.newInstance(mArgs);
				
				mState.pop();
				mState.push(m);
			}
			catch(InstantiationException 
				| IllegalAccessException 
				| IllegalArgumentException 
				| InvocationTargetException 
				| NoSuchMethodException 
				| SecurityException e)
			{
				throw new SevereException(e);
			}
		}
		
		@Override
		public String toString()
		{
			return mType;
		}
	}

	private class DefaultMode implements Mode
	{
		private Vec2 mOffset = null;
		
		@Override
		public boolean mouseDown(MouseEvent e)
		{
			if(e.getButton() == MouseButton.MIDDLE)
			{
				mOffset = (new Vec2(mRoot.getTranslateX(), mRoot.getTranslateY())).sub(mouse(e));
				
				return true;
			}
			
			return false;
		}

		@Override
		public boolean mouseMove(MouseEvent e)
		{
			if(!e.isMiddleButtonDown())
				return mouseUp(e);
			
			if(mOffset != null)
			{
				Vec2 p = mouse(e).add(mOffset);
				
				mRoot.setTranslateX(p.x);
				mRoot.setTranslateY(p.y);
				
				return true;
			}
			
			return false;
		}

		@Override
		public boolean mouseUp(MouseEvent e)
		{
			mOffset = null;
			
			return e.getButton() == MouseButton.MIDDLE;
		}
	}
	
	private class DragMode implements Mode
	{
		private Moveable mMoving = null;

		@Override
		public boolean mouseDown(MouseEvent e)
		{
			if(e.getButton() == MouseButton.PRIMARY)
			{
				Vec2 p = mouseInScene(e);
				Selectable[] sel = new Selectable[1];
				
				all()
					.filter(n -> n.hit(p))
					.findFirst().ifPresent(n -> sel[0] = n);
				
				mMoving = null;
				mSelected.setValue(sel[0]);
				
				if(sel[0] instanceof Moveable)
				{
					mMoving = (Moveable) sel[0];
				}
				
				return true;
			}
			
			return false;
		}

		@Override
		public boolean mouseMove(MouseEvent e)
		{
			if(mMoving != null)
			{
				mMoving.move(mouseInScene(e));
				
				return true;
			}
			
			return false;
		}

		@Override
		public boolean mouseUp(MouseEvent e)
		{
			mMoving = null;
			
			return false;
		}
	}
	
	private class NeuronMode implements Mode
	{
		private final Class<? extends Neuron> mClass;
		
		public NeuronMode(Class<? extends Neuron> c)
		{
			mClass = c;
		}
		
		@Override
		public boolean mouseDown(MouseEvent e)
		{
			if(e.getButton() == MouseButton.PRIMARY)
			{
				Vec2 p = mouseInScene(e);
				Optional<Neuron> clicked = mNeurons.stream()
					.filter(n -> n.hit(p))
					.findFirst();
				
				if(clicked.isPresent())
				{
					mSelected.setValue(clicked.get());
				}
				else
				{
					try
					{
						Neuron n = mClass.getDeclaredConstructor(GenomeEditor.class).newInstance(GenomeEditor.this);
						
						spawn(p, n);
						
						mSelected.setValue(n);
					}
					catch (InstantiationException
						| IllegalAccessException
						| IllegalArgumentException
						| SecurityException
						| InvocationTargetException
						| NoSuchMethodException ex)
					{
						throw new SevereException(ex);
					}
				}
				
				return true;
			}
			
			return false;
		}

		@Override
		public boolean mouseMove(MouseEvent e)
		{
			return false;
		}

		@Override
		public boolean mouseUp(MouseEvent e)
		{
			return false;
		}
	}
	
	private class LinkMode implements Mode
	{
		private Neuron mFrom = null;
		private Line mLine = null;

		@Override
		public boolean mouseDown(MouseEvent e)
		{
			if(e.getButton() == MouseButton.PRIMARY)
			{
				Vec2 p = mouseInScene(e);
				
				mFrom = null;
				mNeurons.stream()
					.filter(n -> n.hit(p))
					.findFirst().ifPresent(n -> mFrom = n);
				
				mSelected.setValue(mFrom);
				
				if(mFrom != null)
				{
					Vec2 c = mFrom.bb().center();
					
					mLine = new Line(c.x, c.y, c.x, c.y);
					
					mRoot.getChildren().add(mLine);
				}
				
				return true;
			}
			return false;
		}

		@Override
		public boolean mouseMove(MouseEvent e)
		{
			if(mLine != null)
			{
				Vec2 p = mouseInScene(e);

				mLine.setEndX(p.x);
				mLine.setEndY(p.y);
				
				return true;
			}
			
			return false;
		}

		@Override
		public boolean mouseUp(MouseEvent e)
		{
			if(mLine != null)
			{
				Vec2 p = mouseInScene(e);
				
				mNeurons.stream()
					.filter(n -> n.hit(p))
					.findFirst().ifPresent(to -> connect(mFrom, to));
				
				mRoot.getChildren().remove(mLine);
				
				mFrom = null;
				mLine = null;
				
				return true;
			}
			
			return false;
		}
	}
	
	private static class AABB
	{
		public final double x0, y0, x1, y1;
		
		public AABB(double x0, double y0, double x1, double y1)
		{
			this.x0 = x0;
			this.y0 = y0;
			this.x1 = x1;
			this.y1 = y1;
		}
		
		public boolean inside(Vec2 p)
		{
			return x0 <= p.x && p.x <= x1 && y0 <= p.y && p.y <= y1;
		}
		
		public Vec2 ll() { return new Vec2(x0, y0); }
		public Vec2 ur() { return new Vec2(x1, y1); }
		public double width() { return x1 - x0; }
		public double height() { return y1 - y0; }
		public Vec2 center() { return new Vec2((x0 + x1) / 2, (y0 + y1) / 2); }
		
		@Override
		public String toString()
		{
			return String.format("[(%.2f, %.2f) -> (%.2f, %.2f)]", x0, y0, x1, y1);
		}
	}
	
	private static interface Inheritable
	{
		public abstract Gene gene();
	}
	
	private static interface Selectable extends Inheritable
	{
		public abstract long id( );
		public abstract Node ui( );
		public abstract boolean hit(Vec2 p);
	}
	
	private static interface Moveable extends Selectable
	{
		public void move(Vec2 p);
	}
	
	private static abstract class BaseElement implements Selectable
	{
		private final long mID;
		
		public BaseElement(long id)
		{
			mID = id;
		}
		
		@Override
		public long id()
		{
			return mID;
		}
	}
	
	private abstract class Neuron extends BaseElement implements Moveable
	{
		private final NeuronUI mUI;
		private boolean mDone;
		
		public Neuron(long id, NeuronUI ui)
		{
			super(id);
			
			mUI = ui;
			mDone = false;
		}
		
		public Node ui() { return mUI.ui(); }
		public DoubleProperty valueProperty() { return mUI.valueProperty(); }

		public AABB bb()
		{
			Bounds bb = ui().getBoundsInParent();
			
			return new AABB(bb.getMinX(), bb.getMinY(), bb.getMaxX(), bb.getMaxY());
		}
		
		public void reset()
		{
			mDone = false;
		}
		
		public final double value()
		{
			if(!mDone)
			{
				valueProperty().set(compute());
				mDone = true;
			}
			
			return valueProperty().get();
		}
		
		protected abstract double compute();
		
		@Override
		public boolean hit(Vec2 p)
		{
			return bb().inside(p);
		}
		
		@Override
		public void move(Vec2 p)
		{
			ui().setTranslateX(p.x);
			ui().setTranslateY(p.y);
		}
	}
	
	private class BiasNeuron extends Neuron
	{
		public BiasNeuron()
		{
			super(mNextID++, new BiasNeuronUI());
		}
		
		@Override
		public Gene gene()
		{
			return new BiasGene(id());
		}
		
		@Override
		protected double compute()
		{
			return 1;
		}
	}
	
	private class InputNeuron extends Neuron
	{
		public InputNeuron()
		{
			super(mNextID++, new InputNeuronUI());
		}
		
		@Override
		public Gene gene()
		{
			return new InputGene(id());
		}
		
		@Override
		protected double compute()
		{
			return valueProperty().get();
		}
	}
	
	private abstract class ConnectedNeuron extends Neuron
	{
		private final Property<Activation> mActivation;
		private final List<Link> mLinks;
		
		public ConnectedNeuron(long id, Activation a, NeuronUI ui)
		{
			super(id, ui);
			
			mActivation = new SimpleObjectProperty<>(a);
			mLinks = new ArrayList<>();
			
			mActivation.addListener($ -> recalculate());
		}
		
		public Property<Activation> activationProperty() { return mActivation; }
		
		@Override
		protected double compute()
		{
			return mActivation.getValue().apply(
					mLinks.stream()
						.mapToDouble(l -> l.weightProperty().get() * l.from().value())
						.sum());
		}
		
		public void connect(Link l)
		{
			if(l.to() == this)
			{
				mLinks.add(l);
			}
		}
	}
	
	private class HiddenNeuron extends ConnectedNeuron
	{
		public HiddenNeuron()
		{
			super(mNextID++, mActivation.getSelectionModel().getSelectedItem(), new NeuronUI());
		}
		
		@Override
		public Gene gene()
		{
			return new HiddenGene(id(), activationProperty().getValue());
		}
	}
	
	private class OutputNeuron extends ConnectedNeuron
	{
		public OutputNeuron()
		{
			super(mNextID++, mActivation.getSelectionModel().getSelectedItem(), new OutputNeuronUI());
		}
		
		@Override
		public Gene gene()
		{
			return new OutputGene(id(), activationProperty().getValue());
		}
	}
	
	private class Link extends BaseElement
	{
		private final Neuron mFrom, mTo;
		private final DoubleProperty mWeight;
		private final LinkUI mUI;
		
		public Link(long id, Neuron from, ConnectedNeuron to)
		{
			super(id);
			
			mFrom = from;
			mTo = to;
			mWeight = new SimpleDoubleProperty(0);
			mUI = new LinkUI(mFrom.ui(), mTo.ui());
			
			to.connect(this);
		}
		
		@Override
		public Gene gene()
		{
			return new LinkGene(mFrom.id(), mTo.id(), mWeight.get(), id());
		}
		
		@Override
		public boolean hit(Vec2 p)
		{
			Vec2 from = mUI.from();
			Vec2 to = mUI.to();
			Vec2 v = to.sub(from);
			Vec2 u = v.normalized();
			Vec2 w = p.sub(from);
			double l = w.mul(u);
			
			if(l < 0 || l > v.length())
				return false;
			
			double d = p.sub(from.add(u.scale(l))).length();
			
			return d <= 5;
		}
		
		public Node ui() { return mUI.ui(); }
		
		public Neuron from() { return mFrom; }
		public Neuron to() { return mTo; }
		public DoubleProperty weightProperty() { return mWeight; }
		public double getWeight() { return mWeight.get(); }
		public void setWeight(double w) { mWeight.set(w); }
	}
	
	private static class InfoPanel
	{
		private final GridPane mRoot;
		private final Relay mRelay;
		
		public InfoPanel()
		{
			mRoot = new GridPane();
			mRelay = new Relay(this);
			
			mRoot.setHgap(12);
			mRoot.setVgap(4);
			mRoot.setPadding(new Insets(8, 8, 8, 8));
			mRoot.setStyle("-fx-border-style: solid;");
			
			mRoot.setPrefWidth(300);
		}
		
		public Node ui() { return mRoot; }
		
		public void update(Selectable o)
		{
			mRoot.getChildren().clear();
			
			if(o != null)
			{
				mRelay.call(o);
			}
		}
		
		@Overloaded
		private void updateBias(BiasNeuron n)
		{
			mRoot.addRow(0, new Label("Type"), new Label("Bias"));
		}
		
		@Overloaded
		private void updateInput(InputNeuron n)
		{
			mRoot.addRow(0, new Label("Type"), new Label("Input"));
			mRoot.addRow(1, new Label("Value"), input(n.valueProperty()));
		}
		
		@Overloaded
		private void updateNeuron(ConnectedNeuron n)
		{
			Label val = new Label("" + n.valueProperty().get());
			ComboBox<Activation> act = createActivationComboBox();
			
			act.getSelectionModel().select(n.activationProperty().getValue());
			
			n.valueProperty().addListener((v, o, r) -> val.setText("" + r));
			act.getSelectionModel().selectedItemProperty().addListener((v, o, r) -> n.activationProperty().setValue(r));
			
			mRoot.addRow(0, new Label("Type"), new Label(n instanceof HiddenNeuron ? "Hidden" : "Output"));
			mRoot.addRow(1, new Label("Value"), val);
			mRoot.addRow(2, new Label("Activation"), act);
		}
		
		@Overloaded
		private void updateLink(Link l)
		{
			mRoot.addRow(0, new Label("Type"), new Label("Link"));
			mRoot.addRow(1, new Label("Value"), input(l.weightProperty()));
		}
		
		private TextField input(DoubleProperty v)
		{
			TextField r = new TextField("" + v.get());
			
			r.setPrefWidth(100);
			
			r.textProperty().addListener((ob, o, n) -> {
				try
				{
					v.set(Double.parseDouble(n));
				}
				catch(NumberFormatException e) { }
			});
			
			return r;
		}
	}
	
	private static ComboBox<Activation> createActivationComboBox()
	{
		ComboBox<Activation> cb = new ComboBox<>();
		
		Stream.of(Activation.Type.values())
			.forEach(a -> cb.getItems().add(Activation.get(a)));
		
		cb.getSelectionModel().select(0);
		
		return cb;
	}
}
