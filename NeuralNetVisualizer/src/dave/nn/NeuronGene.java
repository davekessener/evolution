package dave.nn;

public interface NeuronGene extends Gene
{
	public abstract Neuron create( );
}
