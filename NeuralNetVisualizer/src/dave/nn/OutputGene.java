package dave.nn;

import dave.json.JsonSerializable;
import dave.json.JsonSerializer;

@JsonSerializable
public class OutputGene extends ConnectedGene
{
	@JsonSerializer({ "mID", "mActivation" })
	public OutputGene(long id, Activation a)
	{
		super(id, a);
	}
	
	@Override
	public OutputNeuron create()
	{
		return new OutputNeuron(activation());
	}
	
	@Override
	public boolean equals(Object o)
	{
		return (o instanceof OutputGene) && super.equals(o);
	}
}
