package dave.nn;

public abstract class BaseGene implements Gene
{
	private final long mID;
	
	protected BaseGene(long id)
	{
		mID = id;
	}
	
	@Override
	public long id()
	{
		return mID;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Gene)
		{
			return id() == ((Gene) o).id();
		}
		
		return false;
	}
}
