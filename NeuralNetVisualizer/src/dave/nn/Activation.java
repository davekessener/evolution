package dave.nn;

import java.util.Map;
import java.util.function.DoubleUnaryOperator;

import dave.json.Container;
import dave.json.JsonString;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;
import dave.util.ImmutableMapBuilder;

@Container
public class Activation implements Saveable
{
	private final DoubleUnaryOperator mF;
	private final Type mType;
	
	private Activation(Type t, DoubleUnaryOperator f)
	{
		mType = t;
		mF = f;
	}
	
	@Saver
	@Override
	public JsonValue save()
	{
		return new JsonString(mType.toString());
	}
	
	@Loader
	public static Activation load(JsonValue json)
	{
		return get(Type.valueOf(((JsonString) json).get()));
	}
	
	@Override
	public String toString()
	{
		return mType.toString();
	}
	
	public double apply(double v) { return mF.applyAsDouble(v); }
	public Type type() { return mType; }
	
	public static enum Type
	{
		SIGMOID,
		TANH,
		FULL_STEP,
		HALF_STEP,
		GAUSS,
		SINE,
		RELU,
		LINEAR,
		ABSOLUTE
	}
	
	private static final DoubleUnaryOperator SIGMA = x -> 1 / (1 + Math.exp(-x));
	
	public static final Activation SIGMOID = new Activation(Type.SIGMOID, SIGMA);
	public static final Activation TANH = new Activation(Type.TANH, x -> 2 * SIGMA.applyAsDouble(2 * x) - 1);
	public static final Activation FULL_STEP = new Activation(Type.FULL_STEP, x -> x > 0 ? 1 : -1);
	public static final Activation HALF_STEP = new Activation(Type.HALF_STEP, x -> x > 0 ? 1 : 0);
	public static final Activation GAUSS = new Activation(Type.GAUSS, x -> Math.exp(-(x * x) * 4));
	public static final Activation SINE = new Activation(Type.SINE, x -> Math.sin(x));
	public static final Activation RELU = new Activation(Type.RELU, x -> x < 0 ? 0 : x);
	public static final Activation LINEAR = new Activation(Type.LINEAR, x -> x);
	public static final Activation ABSOLUTE = new Activation(Type.ABSOLUTE, x -> Math.abs(x));
	
	public static Activation get(Type t)
	{
		Activation a = sActivations.get(t);
		
		if(a == null || a.type() != t)
			throw new IllegalStateException();
		
		return a;
	}
	
	private static final Map<Type, Activation> sActivations = (new ImmutableMapBuilder<Type, Activation>())
		.put(Type.SIGMOID, SIGMOID)
		.put(Type.TANH, TANH)
		.put(Type.FULL_STEP, FULL_STEP)
		.put(Type.HALF_STEP, HALF_STEP)
		.put(Type.GAUSS, GAUSS)
		.put(Type.SINE, SINE)
		.put(Type.RELU, RELU)
		.put(Type.LINEAR, LINEAR)
		.put(Type.ABSOLUTE, ABSOLUTE)
		.build();
}
