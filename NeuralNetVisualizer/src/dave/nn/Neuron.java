package dave.nn;

public interface Neuron
{
	public abstract double get();
}
