package dave.evo.config;

import dave.util.config.Default;

public interface HypercubeOptions
{
	@Default("5.0")
	public abstract double cppn_output_scale( );
}
