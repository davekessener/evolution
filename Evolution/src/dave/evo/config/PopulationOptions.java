package dave.evo.config;

import dave.util.config.Default;

public interface PopulationOptions
{
	@Default("0.2")
	public abstract double asexual_reproduction( );
	
	@Default("0.05")
	public abstract double large_species_threshold( );
	
	@Default("5")
	public abstract int large_species_limit( );
	
	@Default("10")
	public abstract int find_breed_parter_max_tries( );
}
