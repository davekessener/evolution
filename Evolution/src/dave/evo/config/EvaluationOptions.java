package dave.evo.config;

import dave.util.config.Default;

public interface EvaluationOptions
{
	@Default("-1")
	public abstract double fitness_bias( );
}
