package dave.evo.config;

import dave.util.config.Default;

public interface StorageOptions
{
	@Default("state.json")
	public abstract String save_filename( );
	
	@Default("60000")
	public abstract int save_frequency( );
}
