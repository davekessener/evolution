package dave.evo.config;

import dave.util.config.Default;

public interface SubstrateOptions
{
	@Default("3")
	public abstract int min_division( );
	
	@Default("4")
	public abstract int max_division( );
	
	@Default("0.05")
	public abstract double division_variance( );
	
	@Default("0.05")
	public abstract double prune_variance( );
	
	@Default("0.3")
	public abstract double banding_threshold( );
	
	@Default("2")
	public abstract int recursion_depth( );
	
	@Default("0.05")
	public abstract double minimum_distance( );
}
