package dave.evo.config;

import dave.util.config.Default;

public interface FlatOptions
{
	@Default("0.1")
	public abstract double flat_mutation_chance( );
	
	@Default("0.15")
	public abstract double flat_mutation_weight( );
}
