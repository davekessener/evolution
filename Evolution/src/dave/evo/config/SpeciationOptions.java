package dave.evo.config;

import java.util.Map;

import dave.util.config.Default;

public interface SpeciationOptions
{
	@Default("1.0")
	public abstract double weight_excess( );
	
	@Default("1.0")
	public abstract double weight_disjoint( );
	
	@Default("3.0")
	public abstract double speciation_delta( );
	
	@Default("1")
	public abstract int large_genome_threshold( );
	
	@Default("150")
	public abstract int population_size( );
	
	@Default("20")
	public abstract int improvement_deadline( );
	
	@Default("5")
	public abstract int minimum_species_count( );
	
	@Default("0.1")
	public abstract double stale_reduction_size( );
	
	@Default("0.075")
	public abstract double elites( );
	
	@Default(value="{weights:0.1,activation:3}", valueType=Double.class)
	public abstract Map<String, Double> distance_weights( );
}
