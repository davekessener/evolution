package dave.evo.config;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.LongFunction;

import dave.evo.common.GeneHistory;
import dave.evo.common.NetworkInterface;
import dave.evo.dna.GeneSequence;
import dave.evo.dna.LinkGene;
import dave.evo.dna.NeuronGene;
import dave.evo.sys.app.functional.GenomeBreeder;
import dave.evo.sys.app.functional.GenomeSeeder;
import dave.evo.sys.app.functional.mutate.LinkWeightChanger;
import dave.evo.sys.app.functional.mutate.MutatorAdapter;
import dave.evo.sys.app.functional.mutate.MutatorOptional;
import dave.evo.sys.app.functional.mutate.MutatorSelection;
import dave.evo.sys.app.functional.mutate.MutatorSplit;
import dave.evo.sys.app.functional.mutate.NewLinkMutator;
import dave.evo.sys.app.functional.mutate.NewWeightMutator;
import dave.evo.sys.app.functional.mutate.SplitLinkMutator;
import dave.evo.sys.app.functional.mutate.UniformGeneMutator;
import dave.util.RNG;
import dave.util.config.Default;

public interface GeneOptions
{
	@Default("0.05")
	public abstract double new_link_chance();

	@Default("0.03")
	public abstract double link_split_chance();
	
	@Default("1.5")
	public abstract double link_mutation_factor();
	
	@Default("1.0")
	public abstract double initial_weight();
	
	@Default("0.1")
	public abstract double new_weight_chance();
	
	@Default("0.2")
	public abstract double link_reactivation_chance();
	
	@Default("TANH")
	public abstract String base_activation();
	
	
	public default Function<NetworkInterface, GeneSequence> defaultCreator(RNG rng, GeneHistory history)
	{
		return new GenomeSeeder(base_activation(), history, io -> initial_weight() * (rng.nextDouble() * 2 - 1));
	}
	
	public default Function<GeneSequence, GeneSequence> defaultMutator(RNG rng, GeneHistory history, LongFunction<NeuronGene> f)
	{
		return new MutatorSelection.Builder<>(
				new UniformGeneMutator(rng, new MutatorAdapter<>(LinkGene.class,
					new MutatorSplit<>(
						lg -> lg.active(),
						new MutatorSplit<>(
							lg -> (rng.nextDouble() < new_weight_chance()),
							new NewWeightMutator(() -> initial_weight() * (rng.nextDouble() * 2 - 1)),
							new LinkWeightChanger(() -> link_mutation_factor() * (rng.nextDouble() * 2 - 1))),
						new MutatorOptional<>(
							() -> (rng.nextDouble() < link_reactivation_chance()),
							lg -> lg.activate())))))
				.add(new_link_chance(), new NewLinkMutator(history, rng))
				.add(link_split_chance(), new SplitLinkMutator(history, f, rng))
				.build(rng);
	}
	
	public default BiFunction<GeneSequence, GeneSequence, GeneSequence> defaultBreeder(RNG rng)
	{
		return new GenomeBreeder(rng);
	}
}
