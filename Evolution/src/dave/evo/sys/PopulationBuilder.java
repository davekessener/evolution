package dave.evo.sys;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import dave.evo.config.SpeciationOptions;
import dave.evo.dna.Genome;
import dave.evo.dna.Population;
import dave.util.Utils;
import dave.util.service.Promise;

public class PopulationBuilder
{
	private final SpeciationOptions mOptions;
	private final Function<Genome, Promise<Double>> mEval;
	private final List<Utils.Pair<Genome, Promise<Double>>> mBuffer;
	
	public PopulationBuilder(SpeciationOptions o, Function<Genome, Promise<Double>> f)
	{
		mOptions = o;
		mEval = f;
		mBuffer = new ArrayList<>();
	}
	
	public void add(Genome g)
	{
		mBuffer.add(Utils.pair(g, mEval.apply(g)));
	}
	
	public Population build()
	{
		Population p = new Population(mOptions);
		
		mBuffer.forEach(e -> p.add(e.first, e.second.get()));
		mBuffer.clear();
		
		return p;
	}
}
