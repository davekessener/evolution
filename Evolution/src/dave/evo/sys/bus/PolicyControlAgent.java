package dave.evo.sys.bus;

import dave.evo.common.Library;
import dave.evo.common.Message;
import dave.evo.common.MessageBus;
import dave.evo.common.Options;
import dave.evo.common.Policy;
import dave.evo.dna.Population;
import dave.json.JsonUtils;
import dave.util.Utils;
import dave.util.log.Stdout;
import dave.util.property.Property;
import dave.util.property.SimpleProperty;

public class PolicyControlAgent extends BaseAgent
{
	private final Options mOptions;
	private final Policy mPolicy;
	private final Property<Population> mPopulation;
	private final Property<Boolean> mRunning;
	private long mLastSave, mNextSave, mLastStep;
	private int mCounter, mNotImprovedCounter;
	private double mHighscore;
	
	public PolicyControlAgent(MessageBus bus, Policy p, Property<Boolean> r, Options o)
	{
		super(Library.Agents.Controller.ID, bus);
		
		mOptions = o;
		mPolicy = p;
		mPopulation = new SimpleProperty<>();
		mRunning = r;
		mLastSave = mLastStep = System.currentTimeMillis();
		mCounter = 0;
		mNotImprovedCounter = 0;
		mHighscore = 0;
		
		register(Library.Agents.Controller.GET, this::onGetBest);
		
		register(Library.Agents.PopulationCreator.CREATED, this::onCreated);
		register(Library.Agents.Storage.SAVED, this::onSaved);
		register(Library.Agents.Storage.LOADED, this::onLoaded);
	}
	
	@Override
	public void start()
	{
		super.start();
		
		send(Library.Agents.Storage.ID, Library.Agents.Storage.REGISTER, Utils.pair(
			Library.State.POPULATION,
			JsonUtils.wrap(
				() -> mPopulation.get().save(),
				json -> mPopulation.set(Population.load(mOptions, json)))));
		
		send(Library.Agents.Storage.ID, Library.Agents.Storage.LOAD);
	}
	
	private void onGetBest(Message msg)
	{
		send(msg, Library.Agents.Controller.GET, mPopulation.get(), null);
	}
	
	private void onSaved(Message msg)
	{
		long duration = msg.get();
		
		mNextSave = System.currentTimeMillis() + 10 * duration;
	}
	
	private void onLoaded(Message msg)
	{
		Population p = mPopulation.get();
		
		if(p == null)
		{
			send(Library.Agents.PopulationCreator.ID, Library.Agents.PopulationCreator.CREATE);
		}
		else
		{
			improve(p);
		}
	}
	
	private void onCreated(Message msg)
	{
		Population p = msg.get();
		long t = System.currentTimeMillis();
		
		++mCounter;
		
		Stdout.printf("population: counter=%d, time=%dms, highscore=%f\n", mCounter, (t - mLastStep), p.highscore());
		
		mLastStep = t;
		
		if(mPolicy.done(p))
		{
			mPopulation.set(p);
			
			send(Library.Agents.Storage.ID, Library.Agents.Storage.SAVE);
			
			LOG.info("DONE!");
			
			if(mOptions.quit_on_success())
			{
				mRunning.set(false);
			}
		}
		else
		{
			if(mOptions.save_progress())
			{
				if(t >= mNextSave && t - mLastSave > mOptions.save_frequency())
				{
					mLastSave = t;
					mPopulation.set(p);
					
					send(Library.Agents.Storage.ID, Library.Agents.Storage.SAVE);
				}
			}
			else
			{
				mPopulation.set(p);
			}
			
			if(p.highscore() > mHighscore)
			{
				mHighscore = p.highscore();
				mNotImprovedCounter = 0;
			}
			
			if(++mNotImprovedCounter > mOptions.improvement_deadline())
			{
				LOG.info("Did not improve in %d generations; resetting!", mOptions.improvement_deadline());

				mNotImprovedCounter = 0;
				
				send(Library.Agents.PopulationCreator.ID, Library.Agents.PopulationCreator.RESET, p);
			}
			else
			{
				improve(p);
			}
		}
	}
	
	private void improve(Population p)
	{
		send(Library.Agents.PopulationCreator.ID, Library.Agents.PopulationCreator.IMPROVE, p);
	}
}
