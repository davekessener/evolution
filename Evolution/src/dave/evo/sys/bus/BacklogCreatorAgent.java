package dave.evo.sys.bus;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import dave.evo.common.Message;
import dave.evo.common.MessageBus;
import dave.evo.util.Backlog;
import dave.util.Utils;
import dave.util.stream.StreamUtils;

public abstract class BacklogCreatorAgent<T> extends BaseAgent
{
	private final Backlog<List<T>> mBacklog;
	private final int mSize;
	
	public BacklogCreatorAgent(String id, MessageBus bus, int n)
	{
		super(id, bus);
		
		mBacklog = new Backlog<>();
		mSize = n;
	}
	
	protected abstract Object pack(List<T> collection);
	protected abstract Stream<T> unpack(Object o);
	protected Object payloadForCreation(Message msg, int i) { return null; }
	
	protected void registerCreator(String id)
	{
		register(getID() + ".create", msg -> onCreate(msg, id));
		register(id + ".created", this::onCreated);
	}
	
	protected void registerMutator(String id)
	{
		register(getID() + ".mutate", msg -> onMutate(msg, id));
		register(id + ".created", this::onCreated);
	}
	
	protected void registerBreeder(String id)
	{
		register(getID() + ".breed", msg -> onBreed(msg, id));
		register(id + ".created", this::onCreated);
	}
	
	protected long createEntry(Message msg)
	{
		return mBacklog.create(msg, new ArrayList<>()).id;
	}
	
	protected void onCreate(Message msg, String cb)
	{
		long id = createEntry(msg);
		
		for(int i = 0 ; i < mSize ; ++i)
		{
			send(cb, cb + ".create", payloadForCreation(msg, i), null, id);
		}
	}
	
	protected void onMutate(Message msg, String cb)
	{
		long id = createEntry(msg);
		
		unpack(msg.get()).forEach(e -> send(cb, cb + ".mutate", e, null, id));
	}
	
	protected void onBreed(Message msg, String cb)
	{
		long id = createEntry(msg);
		Utils.Pair<?, ?> e = msg.get();
		
		StreamUtils.zip(unpack(e.first), unpack(e.second))
			.forEach(p -> send(cb, cb + ".breed", p, null, id));
	}
	
	protected void onCreated(Message msg)
	{
		long id = (long) msg.reply.info;
		Backlog.Entry<List<T>> e = mBacklog.get(id);
		
		e.content.add(msg.get());
		
		if(e.content.size() == mSize)
		{
			mBacklog.remove(id);
			
			send(e.source, getID() + ".created", pack(e.content), null);
		}
	}
}
