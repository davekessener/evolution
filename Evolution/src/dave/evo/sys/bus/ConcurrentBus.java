package dave.evo.sys.bus;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import dave.evo.common.Agent;
import dave.evo.common.Message;
import dave.util.SevereException;
import dave.util.Utils;
import dave.util.property.Property;

public class ConcurrentBus extends BaseBus
{
	private final BlockingQueue<Message> mQueue;
	private final Property<Boolean> mRunning;
	private final Thread mThread;
	
	public ConcurrentBus(Property<Boolean> r)
	{
		mQueue = new LinkedBlockingQueue<>();
		mRunning = r;
		mThread = new Thread(Utils.wrap(this::run));
	}
	
	@Override
	public void start()
	{
		mRunning.set(true);
		
		super.start();
		
		mThread.start();
	}
	
	@Override
	public void stop()
	{
		mRunning.set(false);
		
		super.stop();
		
		try
		{
			mThread.join(100);
		}
		catch(InterruptedException e)
		{
			throw new SevereException(e);
		}
	}
	
	@Override
	public void accept(Message msg)
	{
		mQueue.add(msg);
	}
	
	private void run() throws InterruptedException
	{
		while(mRunning.get())
		{
			Message msg = mQueue.poll(10, TimeUnit.MILLISECONDS);
			
			if(msg == null)
				continue;
			
			LOG.log("%s", msg);
			
			Agent a = agents().get(msg.to);
			
			if(a == null)
			{
				LOG.error("Unknown recipient for %s", msg);
			}
			else
			{
				a.accept(msg);
			}
		}
	}
}
