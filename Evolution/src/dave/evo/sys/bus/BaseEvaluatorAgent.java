package dave.evo.sys.bus;

import dave.evo.common.Message;
import dave.evo.common.MessageBus;
import dave.evo.dna.GeneSequence;
import dave.evo.common.NeuralNet;
import dave.evo.common.Library;

public abstract class BaseEvaluatorAgent extends BaseAgent
{
	protected BaseEvaluatorAgent(MessageBus bus)
	{
		super(Library.Agents.Evaluator.ID, bus);
		
		register(Library.Agents.Evaluator.EVALUATE, this::onEvaluateGenome);
		
		register(Library.Agents.PhenotypeCreator.CREATED, this::onCreated);
	}
	
	protected abstract void onEvaluate(Message msg, NeuralNet nn);
	
	private void onEvaluateGenome(Message msg)
	{
		GeneSequence g = msg.get();
		
		send(Library.Agents.PhenotypeCreator.ID, Library.Agents.PhenotypeCreator.INSTANTIATE, g, null, msg);
	}

	private void onCreated(Message msg)
	{
		NeuralNet nn = msg.get();
		Message reply = (Message) msg.reply.info;
		
		onEvaluate(reply, nn);
	}
}
