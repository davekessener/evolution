package dave.evo.sys.bus;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import dave.evo.common.Message;
import dave.evo.common.MessageBus;
import dave.evo.dna.Chromosome;
import dave.evo.dna.Genome;
import dave.util.Cache;
import dave.util.SimpleCache;
import dave.evo.common.NeuralNet;
import dave.evo.common.Library;

public class InstantiatorAgent extends BaseAgent
{
	private final Function<Chromosome, NeuralNet> mCreate;
	private final Function<List<NeuralNet>, NeuralNet> mAssemble;
	private final Cache<Genome, NeuralNet> mCache;
	
	public InstantiatorAgent(MessageBus bus, Function<Chromosome, NeuralNet> f1, Function<List<NeuralNet>, NeuralNet> f2)
	{
		super(Library.Agents.PhenotypeCreator.ID, bus);
		
		mCreate = f1;
		mAssemble = f2;

		mCache = new SimpleCache<>(100,
			g -> mAssemble.apply(g.chromosomes().map(mCreate::apply).collect(Collectors.toList())),
			nn -> nn.reset());
		
		register(Library.Agents.PhenotypeCreator.INSTANTIATE, this::onInstantiate);
	}
	
	private void onInstantiate(Message msg)
	{
		send(msg, Library.Agents.PhenotypeCreator.CREATED, mCache.load(msg.get()), null);
	}
}
