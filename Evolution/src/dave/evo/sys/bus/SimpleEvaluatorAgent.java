package dave.evo.sys.bus;

import java.util.function.ToDoubleFunction;

import dave.evo.common.Library;
import dave.evo.common.Message;
import dave.evo.common.MessageBus;
import dave.evo.dna.Genome;
import dave.evo.common.NeuralNet;
import dave.util.relay.Overloaded;
import dave.util.relay.Relay;

public class SimpleEvaluatorAgent extends BaseAgent
{
	private final Relay mRelay;
	private final ToDoubleFunction<NeuralNet> mCallback;
	
	public SimpleEvaluatorAgent(MessageBus bus, ToDoubleFunction<NeuralNet> f)
	{
		super(Library.Agents.Evaluator.ID, bus);
		
		if(f == null)
			throw new NullPointerException();
		
		mRelay = new Relay(this);
		
		mCallback = f;

		register(Library.Agents.Evaluator.EVALUATE, this::onEvaluate);
		
		register(Library.Agents.PhenotypeCreator.CREATED, this::onCreated);
	}
	
	private void onEvaluate(Message msg)
	{
		mRelay.call(msg, msg.payload);
	}
	
	private void onCreated(Message msg)
	{
		onEvaluateNeuralNet(msg.reply.info(), msg.get());
	}
	
	@Overloaded
	private void onEvaluateGenome(Message msg, Genome g)
	{
		send(Library.Agents.PhenotypeCreator.ID, Library.Agents.PhenotypeCreator.INSTANTIATE, g, null, msg);
	}
	
	@Overloaded
	private void onEvaluateNeuralNet(Message msg, NeuralNet nn)
	{
		double v = mCallback.applyAsDouble(nn);
		
		if(v < 0 || v > 1)
			throw new IllegalStateException(String.format("Invalid fitness %f!", v));
		
		send(msg, Library.Agents.Evaluator.EVALUATED, v, null);
	}
}
