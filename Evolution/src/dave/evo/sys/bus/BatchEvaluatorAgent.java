package dave.evo.sys.bus;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

import dave.evo.common.Library;
import dave.evo.common.Message;
import dave.evo.common.MessageBus;
import dave.evo.common.Options;
import dave.evo.eval.MatchResult;
import dave.evo.common.NeuralNet;
import dave.util.MathUtils;

public class BatchEvaluatorAgent extends BaseEvaluatorAgent
{
	private final BiFunction<NeuralNet, NeuralNet, MatchResult> mCallback;
	private final Options mOptions;
	private final List<Entry> mBacklog;
	
	public BatchEvaluatorAgent(MessageBus bus, BiFunction<NeuralNet, NeuralNet, MatchResult> f, Options o)
	{
		super(bus);
		
		mCallback = f;
		mOptions = o;
		mBacklog = new ArrayList<>(mOptions.population_size());
		
		register(Library.Agents.Evaluator.EVALUATE, this::onEvaluateInstance);
	}
	
	private void onEvaluateInstance(Message msg)
	{
		onEvaluate(msg, msg.get());
	}
	
	@Override
	protected void onEvaluate(Message msg, NeuralNet nn)
	{
		mBacklog.add(new Entry(msg, nn));
		
		if(mBacklog.size() == mOptions.population_size())
		{
			for(int i = 0 ; i < mBacklog.size() ; ++i)
			{
				for(int j = i + 1 ; j < mBacklog.size() ; ++j)
				{
					Entry e0 = mBacklog.get(i);
					Entry e1 = mBacklog.get(j);
					MatchResult r = mCallback.apply(e0.nn, e1.nn);
					
					e0.fitness += r.first;
					e1.fitness += r.second;
				}
			}
			
			final int n = mBacklog.size() - 1;
			
			mBacklog.forEach(e -> e.fitness = MathUtils.clamp(0.5 * (1 + e.fitness / n)));
			mBacklog.forEach(e -> send(e.message, Library.Agents.Evaluator.EVALUATED, e.fitness, null));
			
			mBacklog.clear();
		}
	}
	
	private static class Entry
	{
		public final Message message;
		public final NeuralNet nn;
		public double fitness;
		
		public Entry(Message message, NeuralNet nn)
		{
			this.message = message;
			this.nn = nn;
			this.fitness = 0;
		}
	}
}
