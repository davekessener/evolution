package dave.evo.sys.bus;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import dave.evo.common.Message;
import dave.evo.common.MessageBus;
import dave.evo.common.Options;
import dave.evo.dna.Genome;
import dave.evo.dna.Population;
import dave.evo.dna.Species;
import dave.evo.util.Backlog;
import dave.evo.common.Library;
import dave.util.Counter;
import dave.util.RNG;
import dave.util.Utils;

public class PopulationCreatorAgent extends BaseAgent
{
	private final Options mOptions;
	private final Backlog<Population> mBacklog;
	private final RNG mRandom;
	
	public PopulationCreatorAgent(MessageBus bus, RNG rng, Options o)
	{
		super(Library.Agents.PopulationCreator.ID, bus);
	
		mOptions = o;
		mBacklog = new Backlog<>();
		mRandom = rng;
		
		register(Library.Agents.PopulationCreator.CREATE, this::onPopulationCreate);
		register(Library.Agents.PopulationCreator.IMPROVE, this::onPopulationImprove);
		register(Library.Agents.PopulationCreator.RESET, this::onPopulationReset);
		
		register(Library.Agents.GenotypeCreator.CREATED, this::onGenomeCreated);
		register(Library.Agents.Evaluator.EVALUATED, this::onGenomeEvaluated);
	}
	
	private long createEntry(Message msg)
	{
		return mBacklog.create(msg, new Population(mOptions)).id;
	}
	
	private void onPopulationCreate(Message msg)
	{
		long id = createEntry(msg);
		
		for(int c = mOptions.population_size() ; c > 0 ; --c)
		{
			send(Library.Agents.GenotypeCreator.ID, Library.Agents.GenotypeCreator.CREATE, null, null, id);
		}
	}
	
	private void onPopulationImprove(Message msg)
	{
		Population oldpop = msg.get();
		int c = oldpop.genomeCount();
		
		if(c != mOptions.population_size())
		{
			LOG.warn("Population size difference: expected %d, got %d!", mOptions.population_size(), c);
		}
		
		doImprove(msg, oldpop);
	}
	
	private void onPopulationReset(Message msg)
	{
		Population base = msg.get();
		Population tmp = new Population(mOptions);
		
		List<Species.Entry> l = new ArrayList<>();
		
		base.species()
			.map(s -> s.entries().findFirst().get())
			.sorted((e0, e1) -> Double.compare(e1.fitness, e0.fitness))
			.limit((int) Math.ceil(mOptions.population_size() * mOptions.stale_reduction_size()))
			.forEach(l::add);
		
		if(l.size() < mOptions.stale_reduction_size())
		{
			List<Species.Entry> all = base.species()
				.flatMap(s -> s.entries())
				.sorted((e0, e1) -> Double.compare(e1.fitness, e0.fitness))
				.collect(Collectors.toList());
			
			for(Species.Entry e : all)
			{
				if(l.size() == mOptions.stale_reduction_size())
					break;
				
				if(!l.contains(e))
				{
					l.add(e);
				}
			}
		}
		
		l.forEach(e -> tmp.add(e.genome, e.fitness));
		
		doImprove(msg, tmp);
	}
	
	private void doImprove(Message msg, Population oldpop)
	{
		long newpop = createEntry(msg);
		
		final int c = oldpop.genomeCount();
		final int large_size = (int) (mOptions.large_species_threshold() * c);
		
		Counter total = new Counter();
		int asex = (int) (mOptions.asexual_reproduction() * c);
		
		oldpop.species()
			.limit(mOptions.large_species_limit())
			.filter(s -> s.size() > large_size)
			.forEach(s -> {
				Species.Entry best = s.entries().findFirst().get();
				
				send(Library.Agents.Evaluator.ID, Library.Agents.Evaluator.EVALUATE, best.genome, null, newpop);
				
				total.inc();
			});
		
		List<Species.Entry> all = oldpop.species()
			.flatMap(s -> s.entries())
			.sorted((e0, e1) -> Double.compare(e1.adjustedFitness(), e0.adjustedFitness()))
			.collect(Collectors.toList());
		
		for(int i = 0, e = (int) Math.floor(mOptions.population_size() * mOptions.elites()) ; i < e ; ++i)
		{
			if(i >= all.size())
				break;
			
			send(Library.Agents.Evaluator.ID, Library.Agents.Evaluator.EVALUATE, all.get(i).genome, null, newpop);
			
			total.inc();
		}
		
		if(c == oldpop.speciesCount())
		{
			asex = mOptions.population_size() - total.get();
		}
		
		for(int i = 0 ; i < asex ; ++i)
		{
			int j = getWeightedIndex(c);
			Genome g = all.get(j).genome;
			
			send(Library.Agents.GenotypeCreator.ID, Library.Agents.GenotypeCreator.MUTATE, g, null, newpop);
			
			total.inc();
		}
		
		while(total.get() < mOptions.population_size())
		{
			int i = getWeightedIndex(c);
			Species.Entry e0 = all.get(i);
			int l = e0.species().size();
			
			if(l <= 1)
			{
				send(Library.Agents.GenotypeCreator.ID, Library.Agents.GenotypeCreator.MUTATE, e0.genome, null, newpop);
				
				total.inc();
			}
			else
			{
				Species.Entry e1 = e0;
				
				for(int k = 0 ; k < mOptions.find_breed_parter_max_tries() && e0 == e1 ; ++k)
				{
					int j = getWeightedIndex(l);
					
					e1 = e0.species().entries().skip(j).findFirst().get();
				}
				
				if(e0 != e1)
				{
					Utils.Pair<Genome, Genome> p = null;
					
					if(e0.fitness >= e1.fitness)
					{
						p = Utils.pair(e0.genome, e1.genome);
					}
					else
					{
						p = Utils.pair(e1.genome, e0.genome);
					}
					
					send(Library.Agents.GenotypeCreator.ID, Library.Agents.GenotypeCreator.BREED, p, null, newpop);
					
					total.inc();
				}
			}
		}
	}
	
	private int getWeightedIndex(int c) { double v = mRandom.nextDouble(); return (int) (v * v * c); }
	
	private void onGenomeCreated(Message msg)
	{
		long id = (long) msg.reply.info;
		Genome g = msg.get();
		
		send(Library.Agents.Evaluator.ID, Library.Agents.Evaluator.EVALUATE, g, null, id);
	}
	
	private void onGenomeEvaluated(Message msg)
	{
		long id = (long) msg.reply.info;
		Genome g = msg.reply.get();
		double f = msg.get();
		Backlog.Entry<Population> e = mBacklog.get(id);
		
		e.content.add(g, f);
		
		if(e.content.genomeCount() == mOptions.population_size())
		{
			mBacklog.remove(id);
			
			e.content.rebalance();
			
			send(e.source, Library.Agents.PopulationCreator.CREATED, e.content, null);
		}
	}
}
