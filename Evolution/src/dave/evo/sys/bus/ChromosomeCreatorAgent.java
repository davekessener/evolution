package dave.evo.sys.bus;

import dave.evo.common.Library;
import dave.evo.common.MessageBus;
import dave.evo.common.NetworkInterface;
import dave.evo.dna.Chromosome;
import dave.evo.util.CreationWrapper;

public class ChromosomeCreatorAgent extends CreatorAgent<NetworkInterface, Chromosome>
{
	public ChromosomeCreatorAgent(MessageBus bus, CreationWrapper<NetworkInterface, Chromosome> f)
	{
		super(Library.Agents.ChromosomeCreator.ID, bus, f);
	}
}
