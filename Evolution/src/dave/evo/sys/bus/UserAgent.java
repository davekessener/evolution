package dave.evo.sys.bus;

import dave.evo.common.Message;
import dave.evo.common.MessageBus;
import dave.evo.dna.Genome;
import dave.evo.dna.Population;
import dave.evo.common.NeuralNet;

import java.io.File;

import dave.evo.common.Library;
import dave.json.JSON;
import dave.json.JsonObject;
import dave.json.JsonUtils;
import dave.json.PrettyPrinter;
import dave.util.SevereException;
import dave.util.Utils;
import dave.util.command.CmdEngine;
import dave.util.command.SimpleCommand;
import dave.util.log.Logger;
import dave.util.property.Property;

//@SuppressWarnings({"unused"})
public class UserAgent extends BaseAgent
{
	private final Thread mThread;
	private final CmdEngine mCmds;
	private final Property<Boolean> mRunning;
	
	public UserAgent(MessageBus bus, Property<Boolean> cb)
	{
		super("self", bus);
		
		mThread = new Thread(this::run);
		mCmds = new CmdEngine(System.out);
		mRunning = cb;

		mCmds.add(new SimpleCommand("save", "", this::executeSave));
		mCmds.add(new SimpleCommand("convert", "", this::executeConvert));
		
		register(Library.Agents.Controller.GET, this::onBest);
		register(Library.Agents.PhenotypeCreator.CREATED, this::onInstantiated);
	}
	
	@Override
	public void start()
	{
		mThread.start();
		
		LOG.info("User interface online");
	}
	
	@Override
	public void stop()
	{
		mThread.interrupt();
		
		try
		{
			mThread.join(100);
		}
		catch(InterruptedException e)
		{
			throw new SevereException(e);
		}
	}
	
	private void run()
	{
		mCmds.run("", System.in, () -> mRunning.get());
	}
	
	private void executeSave()
	{
		send(Library.Agents.Controller.ID, Library.Agents.Controller.GET);
	}
	
	private void executeConvert()
	{
		Genome g = Utils.run(() -> Genome.load(JsonUtils.fromFile(new File("best.json"))));
		
		send(Library.Agents.PhenotypeCreator.ID, Library.Agents.PhenotypeCreator.INSTANTIATE, g);
	}
	
	private void onBest(Message msg)
	{
		Population p = msg.get();
		
		send(Library.Agents.PhenotypeCreator.ID, Library.Agents.PhenotypeCreator.INSTANTIATE, p.best().get());
	}
	
	private void onInstantiated(Message msg)
	{
		NeuralNet nn = msg.get();
		Genome g = msg.reply.get();
		
		LOG.info("Writing snapshot ...");
		
		JsonObject json = new JsonObject();
		
		json.put("genome", g.save());
		json.put("neural-net", JSON.serialize(nn));
		
		Utils.writeToFile("snapshot.json", json.toString(new PrettyPrinter()));
		
		LOG.info("DONE");
	}
	
	private static final Logger LOG = Logger.get("user");
}
