package dave.evo.sys.bus;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import dave.evo.common.Agent;
import dave.evo.common.Library;
import dave.evo.common.Message;
import dave.evo.common.MessageBus;

public class ConcurrentEvaluatorAgent extends BaseAgent
{
	private final ExecutorService mAsync;
	private final Agent mSuper;
	
	public ConcurrentEvaluatorAgent(MessageBus bus, Agent s)
	{
		super(Library.Agents.Evaluator.ID, bus);
		
		mAsync = Executors.newCachedThreadPool();
		mSuper = s;
	}
	
	@Override
	public void stop()
	{
		super.stop();
		
		mAsync.shutdown();
	}
	
	@Override
	public void accept(Message msg)
	{
		mAsync.submit(() -> mSuper.accept(msg));
	}
}
