package dave.evo.sys.bus;

import java.util.HashMap;
import java.util.Map;

import dave.evo.common.Agent;
import dave.evo.common.MessageBus;
import dave.util.log.Logger;

public abstract class BaseBus implements MessageBus
{
	private final Map<String, Agent> mAgents;
	
	protected BaseBus()
	{
		mAgents = new HashMap<>();
	}
	
	protected Map<String, Agent> agents() { return mAgents; }
	
	@Override
	public void start()
	{
		mAgents.values().forEach(Agent::start);
	}
	
	@Override
	public void stop()
	{
		mAgents.values().forEach(Agent::stop);
	}
	
	@Override
	public void register(Agent a)
	{
		if(mAgents.put(a.getID(), a) != null)
			throw new IllegalStateException("Duplicate agent: " + a.getID() + "!");
	}
	
	@Override
	public void unregister(Agent a)
	{
		if(mAgents.remove(a.getID()) != a)
			throw new IllegalStateException("Failed to remove agent: " + a.getID() + "!");
	}
	
	protected static final Logger LOG = Logger.get("bus");
}
