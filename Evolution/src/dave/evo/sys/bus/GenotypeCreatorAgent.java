package dave.evo.sys.bus;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import dave.evo.common.Library;
import dave.evo.common.Message;
import dave.evo.common.MessageBus;
import dave.evo.common.NetworkInterface;
import dave.evo.dna.Chromosome;
import dave.evo.dna.Genome;

public class GenotypeCreatorAgent extends BacklogCreatorAgent<Chromosome>
{
	private final List<NetworkInterface> mChromosomes;
	
	public GenotypeCreatorAgent(MessageBus bus, List<NetworkInterface> chromosomes)
	{
		super(Library.Agents.GenotypeCreator.ID, bus, chromosomes.size());
		
		mChromosomes = new ArrayList<>(chromosomes);

		registerCreator(Library.Agents.ChromosomeCreator.ID);
		registerMutator(Library.Agents.ChromosomeCreator.ID);
		registerBreeder(Library.Agents.ChromosomeCreator.ID);
	}
	
	@Override
	protected Object payloadForCreation(Message msg, int i)
	{
		return mChromosomes.get(i);
	}

	@Override
	protected Object pack(List<Chromosome> collection)
	{
		return new Genome(collection);
	}

	@Override
	protected Stream<Chromosome> unpack(Object o)
	{
		return ((Genome) o).chromosomes();
	}
}
