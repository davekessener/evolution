package dave.evo.sys.bus;

import dave.evo.common.Message;
import dave.evo.common.MessageBus;
import dave.evo.util.CreationWrapper;
import dave.util.Utils;

public class CreatorAgent<K, T> extends BaseAgent
{
	private final CreationWrapper<K, T> mService;
	
	public CreatorAgent(String id, MessageBus bus, CreationWrapper<K, T> f)
	{
		super(id, bus);
		
		mService = f;

		register(id + ".create", this::onCreate);
		register(id + ".mutate", this::onMutate);
		register(id + ".breed", this::onBreed);
	}
	
	private void onCreate(Message msg)
	{
		K k = msg.get();
		
		send(msg, getID() + ".created", mService.creator.apply(k), null);
	}
	
	private void onMutate(Message msg)
	{
		T g = msg.get();
		
		send(msg, getID() + ".created", mService.mutator.apply(g), null);
	}
	
	private void onBreed(Message msg)
	{
		Utils.Pair<T, T> g = msg.get();
		
		send(msg, getID() + ".created", mService.breeder.apply(g.first, g.second), null);
	}
}
