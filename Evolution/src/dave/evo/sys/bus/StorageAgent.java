package dave.evo.sys.bus;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import dave.evo.common.Library;
import dave.evo.common.Message;
import dave.evo.common.MessageBus;
import dave.json.JsonCollectors;
import dave.json.JsonObject;
import dave.json.JsonUtils;
import dave.json.JsonValue;
import dave.json.PrettyPrinter;
import dave.json.Restorable;
import dave.json.SevereIOException;
import dave.util.Utils;

public class StorageAgent extends BaseAgent 
{
	private final String mFile;
	private final Map<String, Restorable> mState;
	
	public StorageAgent(MessageBus bus, String fn)
	{
		super(Library.Agents.Storage.ID, bus);
		
		mFile = fn;
		mState = new HashMap<>();
		
		register(Library.Agents.Storage.REGISTER, this::onRegister);
		register(Library.Agents.Storage.SAVE, this::onSave);
		register(Library.Agents.Storage.LOAD, this::onLoad);
	}
	
	public void addState(String id, Restorable o)
	{
		mState.put(id, o);
	}
	
	private JsonValue save()
	{
		return mState.entrySet().stream()
			.collect(JsonCollectors.ofObject((json, e) -> json.put(e.getKey(), e.getValue().save())));
	}
	
	private void onRegister(Message msg)
	{
		Utils.Pair<String, Restorable> e = msg.get();
		
		addState(e.first, e.second);
	}
	
	private void onSave(Message msg)
	{
		long t = System.currentTimeMillis();
		
		Utils.writeToFile(mFile, save().toString(new PrettyPrinter()));
		
		t = System.currentTimeMillis() - t;
		
		LOG.info("Saved in %.1fs to %s", (t / 1000.0), mFile);
		
		send(msg, Library.Agents.Storage.SAVED, t, null);
	}
	
	private void onLoad(Message msg)
	{
		File f = new File(mFile);
		
		if(f.exists())
		{
			long t = System.currentTimeMillis();
			
			try
			{
				((JsonObject) JsonUtils.fromFile(f)).stream()
					.forEach(e -> {
						Restorable c = mState.get(e.getKey());
						
						if(c != null)
						{
							c.load(e.getValue());
						}
						else
						{
							LOG.warn("Cannot restore %s! (No such entry)", e.getKey());
						}
					});
				
				t = System.currentTimeMillis() - t;
				
				LOG.info("Loaded in %.1fs from %s", (t / 1000.0), mFile);
			}
			catch(IOException e)
			{
				throw new SevereIOException(e);
			}
		}
		
		send(msg, Library.Agents.Storage.LOADED, null, null);
	}
}
