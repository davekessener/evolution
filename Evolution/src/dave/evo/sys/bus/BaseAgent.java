package dave.evo.sys.bus;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import dave.evo.common.Agent;
import dave.evo.common.Message;
import dave.evo.common.MessageBus;
import dave.util.log.Logger;

public abstract class BaseAgent implements Agent
{
	private final String mID;
	private final MessageBus mBus;
	private final Map<String, Consumer<Message>> mCallbacks;
	
	protected BaseAgent(String id, MessageBus bus)
	{
		mID = id;
		mBus = bus;
		mCallbacks = new HashMap<>();
	}
	
	protected void send(Message msg, String subject, Object payload, Object info) { send(msg.from, subject, payload, msg, info); }
	protected void send(String to, String subject) { send(to, subject, null, null, null); }
	protected void send(String to, String subject, Object payload) { send(to, subject, payload, null, null); }
	protected void send(String to, String subject, Object payload, Message reply, Object info)
	{
		mBus.accept(new Message(getID(), to, subject, reply, payload, info));
	}
	
	protected void register(String t, Consumer<Message> f)
	{
		mCallbacks.put(t, f);
	}
	
	@Override
	public String getID()
	{
		return mID;
	}
	
	@Override
	public void start()
	{
	}
	
	@Override
	public void stop()
	{
	}
	
	@Override
	public void accept(Message msg)
	{
		Consumer<Message> cb = mCallbacks.get(msg.subject);
		
		if(cb == null)
		{
			LOG.error("Unhandled message: %s", msg);
		}
		else
		{
			cb.accept(msg);
		}
	}
	
	protected static final Logger LOG = Logger.get("agent");
}
