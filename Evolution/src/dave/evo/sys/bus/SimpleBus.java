package dave.evo.sys.bus;

import java.util.LinkedList;
import java.util.Queue;

import dave.evo.common.Agent;
import dave.evo.common.Message;
import dave.util.Utils;
import dave.util.property.Property;

public class SimpleBus extends BaseBus
{
	private final Property<Boolean> mRunning;
	private final Queue<Message> mQueue;
	
	public SimpleBus(Property<Boolean> running)
	{
		mRunning = running;
		mQueue = new LinkedList<>();
	}
	
	@Override
	public void start()
	{
		mRunning.set(true);
		
		super.start();
		
		while(mRunning.get())
		{
			step();
		}
	}
	
	@Override
	public void stop()
	{
		super.stop();
		
		mRunning.set(false);
	}

	@Override
	public void accept(Message msg)
	{
		mQueue.add(msg);
	}
	
	private void step()
	{
		Message msg = mQueue.poll();
		
		if(msg == null)
		{
			Utils.sleep(10);
			
			return;
		}
		
		if(mRunning.get())
		{
			Agent a = agents().get(msg.to);
			
			if(a != null)
			{
				LOG.log("%s", msg);
				
				a.accept(msg);
				
				if(!mRunning.get())
				{
					stop();
				}
			}
			else
			{
				LOG.error("Can't deliver %s", msg);
			}
		}
		else
		{
			LOG.warn("Trying to send message after shutdown: %s", msg);
		}
	}
}
