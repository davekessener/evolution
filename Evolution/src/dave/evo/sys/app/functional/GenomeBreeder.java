package dave.evo.sys.app.functional;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

import dave.evo.dna.Gene;
import dave.evo.dna.GeneSequence;
import dave.evo.dna.GeneAlignment;
import dave.evo.dna.HiddenGene;
import dave.evo.dna.LinkGene;
import dave.util.RNG;
import dave.util.log.Logger;
import dave.util.stream.StreamUtils;

public class GenomeBreeder implements BiFunction<GeneSequence, GeneSequence, GeneSequence>
{
	private final RNG mRandom;
	
	public GenomeBreeder(RNG rng)
	{
		mRandom = rng;
	}
	
	@Override
	public GeneSequence apply(GeneSequence g0, GeneSequence g1)
	{
		final boolean left = g0.size() >= g1.size();
		
		if(!left)
		{
			GeneSequence g = g0; g0 = g1; g1 = g;
		}
		
		GeneAlignment ga = new GeneAlignment(g0, g1);
		List<Gene> genes = new ArrayList<>();
		
		ga.genes().forEach(p -> {
			if(p.left != null && p.right != null && p.left.marker() != p.right.marker())
				throw new IllegalStateException("Mismatch: " + p.left.marker() + " != " + p.right.marker());
			
			if(p.left == null && p.right == null)
				throw new IllegalStateException("Both empty!");
			
			if((left && p.right == null) || (!left && p.left == null))
			{
				genes.add(left ? p.left : p.right);
			}
			else if(p.left != null && p.right != null)
			{
				genes.add(mRandom.nextDouble() < 0.5 ? p.left : p.right);
			}
		});
		
		if(genes.stream().filter(gg -> (gg instanceof HiddenGene)).anyMatch(gg -> !genes.stream().filter(lg -> (lg instanceof LinkGene)).map(lg -> (LinkGene) lg).anyMatch(lg -> lg.from() == gg.marker())))
		{
			Logger.DEFAULT.error("%s:", g0.toString());
			g0.genes().map(StreamUtils.stream_with_index()).forEach(e -> Logger.DEFAULT.error("[%02d] %s", e.index, e.value));

			Logger.DEFAULT.error("%s:", g1.toString());
			g1.genes().map(StreamUtils.stream_with_index()).forEach(e -> Logger.DEFAULT.error("[%02d] %s", e.index, e.value));
			
			ga.genes().map(StreamUtils.stream_with_index()).forEach(p -> {
				String l = p.value.left == null ? "---" : String.format("%d", p.value.left.marker());
				String r = p.value.right == null ? "---" : String.format("%d", p.value.right.marker());
				
				Logger.DEFAULT.error("[%02d] %3s %3s", p.index, l, r);
			});
			
			throw new IllegalStateException();
		}
		
		return new GeneSequence(genes);
	}
}
