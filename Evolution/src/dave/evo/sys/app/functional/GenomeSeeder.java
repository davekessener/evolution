package dave.evo.sys.app.functional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import dave.evo.common.GeneHistory;
import dave.evo.common.NetworkInterface;
import dave.evo.dna.BiasGene;
import dave.evo.dna.Gene;
import dave.evo.dna.GeneSequence;
import dave.evo.dna.InputGene;
import dave.evo.dna.LinkGene;
import dave.evo.dna.OutputGene;

public class GenomeSeeder implements Function<NetworkInterface, GeneSequence>
{
	private final String mActivation;
	private final GeneHistory mHistory;
	private final ToDoubleFunction<NetworkInterface> mWeightGenerator;
	private final Map<NetworkInterface, List<Gene>> mCache;
	
	public GenomeSeeder(String a, GeneHistory h, ToDoubleFunction<NetworkInterface> f)
	{
		mActivation = a;
		mHistory = h;
		mWeightGenerator = f;
		
		mCache = new HashMap<>();
	}

	@Override
	public GeneSequence apply(NetworkInterface io)
	{
		List<Gene> genes = mCache.get(io);
		
		if(genes == null)
		{
			genes = new ArrayList<>();
			
			long bias = 0;
			long[] inputs = IntStream.range(0, io.inputs.size()).mapToLong(i -> mHistory.nextID()).toArray();
			long[] outputs = IntStream.range(0, io.outputs.size()).mapToLong(i -> mHistory.nextID()).toArray();
			
			genes.add(new BiasGene(bias));
			
			for(int i = 0 ; i < inputs.length ; ++i)
			{
				genes.add(new InputGene(inputs[i]));
			}
			
			for(int i = 0 ; i < outputs.length ; ++i)
			{
				genes.add(new OutputGene(mActivation, outputs[i]));
			}
			
			for(int i = 0 ; i < outputs.length ; ++i)
			{
				long to = outputs[i];
				
				genes.add(new LinkGene(mHistory.link(bias, to), bias, to, 0, true));
				
				for(int j = 0 ; j < inputs.length ; ++j)
				{
					long from = inputs[j];
					
					genes.add(new LinkGene(mHistory.link(from, to), from, to, 0, true));
				}
			}
			
			mCache.put(io, genes);
		}
		
		return new GeneSequence(genes.stream()
			.map(g -> {
				if(g instanceof LinkGene)
				{
					LinkGene lg = (LinkGene) g;
					
					return new LinkGene(lg.marker(), lg.from(), lg.to(), mWeightGenerator.applyAsDouble(io), true);
				}
				else
				{
					return g;
				}
			}).collect(Collectors.toList()));
	}
}
