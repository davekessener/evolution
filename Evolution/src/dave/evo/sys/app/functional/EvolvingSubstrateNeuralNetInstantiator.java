package dave.evo.sys.app.functional;

import java.util.function.Function;

import dave.evo.common.Options;
import dave.evo.common.NetworkInterface;
import dave.evo.config.HypercubeOptions;
import dave.evo.dna.GeneSequence;
import dave.evo.common.NeuralNet;
import dave.evo.nn.BasicNeuralNet;
import dave.evo.nn.es.SubstrateGenerator;
import dave.evo.nn.hyper.CPPNN;
import dave.evo.nn.hyper.HypercubePainter;
import dave.evo.nn.hyper.Substrate;

public class EvolvingSubstrateNeuralNetInstantiator implements Function<GeneSequence, NeuralNet>
{
	private final Function<HypercubePainter, Substrate> mCallback;
	private final HypercubeOptions mOptions;
	
	public EvolvingSubstrateNeuralNetInstantiator(NetworkInterface i, Options o) { this(new SubstrateGenerator(i, o), o); }
	public EvolvingSubstrateNeuralNetInstantiator(Function<HypercubePainter, Substrate> f, HypercubeOptions o)
	{
		mCallback = f;
		mOptions = o;
	}

	@Override
	public NeuralNet apply(GeneSequence g)
	{
		CPPNN cppnn = new CPPNN(BasicNeuralNet.build(g));
		
		return BasicNeuralNet.build(mCallback.apply(cppnn).paint(HypercubeNeuralNetInstantiator.scalePainter(cppnn, mOptions)));
	}
}
