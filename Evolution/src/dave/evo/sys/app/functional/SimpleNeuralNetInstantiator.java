package dave.evo.sys.app.functional;

import java.util.function.Function;

import dave.evo.dna.GeneSequence;
import dave.evo.nn.BasicNeuralNet;
import dave.evo.common.NeuralNet;

public class SimpleNeuralNetInstantiator implements Function<GeneSequence, NeuralNet>
{
	@Override
	public NeuralNet apply(GeneSequence g)
	{
		return BasicNeuralNet.build(g);
	}
}
