package dave.evo.sys.app.functional.mutate;

import java.util.function.BooleanSupplier;
import java.util.function.Function;

public class MutatorOptional<T> extends MutatorSplit<T>
{
	public MutatorOptional(BooleanSupplier f, Function<T, T> cb)
	{
		super(lg -> f.getAsBoolean(), cb, g -> g);
	}
}
