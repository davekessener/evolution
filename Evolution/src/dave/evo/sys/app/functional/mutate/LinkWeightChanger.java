package dave.evo.sys.app.functional.mutate;

import java.util.function.DoubleSupplier;
import java.util.function.Function;

import dave.evo.dna.LinkGene;

public class LinkWeightChanger implements Function<LinkGene, LinkGene>
{
	private final DoubleSupplier mCallback;
	
	public LinkWeightChanger(DoubleSupplier f)
	{
		mCallback = f;
	}
	
	@Override
	public LinkGene apply(LinkGene t)
	{
		return new LinkGene(t.marker(), t.from(), t.to(), t.weight() + mCallback.getAsDouble(), t.active());
	}
}
