package dave.evo.sys.app.functional.mutate;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.LongFunction;

import dave.evo.common.GeneHistory;
import dave.evo.dna.Gene;
import dave.evo.dna.GeneSequence;
import dave.evo.dna.LinkGene;
import dave.evo.dna.NeuronGene;
import dave.util.RNG;
import dave.util.stream.StreamUtils;

public class SplitLinkMutator implements Function<GeneSequence, GeneSequence>
{
	private final LongFunction<NeuronGene> mCallback;
	private final GeneHistory mHistory;
	private final RNG mRandom;
	
	public SplitLinkMutator(GeneHistory h, LongFunction<NeuronGene> c, RNG rng)
	{
		mCallback = c;
		mHistory = h;
		mRandom = rng;
	}
	
	@Override
	public GeneSequence apply(final GeneSequence t)
	{
		return t.genes()
			.filter(g -> (g instanceof LinkGene))
			.map(g -> (LinkGene) g)
			.filter(g -> g.active())
			.filter(lg -> lg.from() != 0)
			.filter(g -> {
				long id = mHistory.split(g.marker());
				
				return !t.genes().anyMatch(gg -> gg.marker() == id);
			})
			.collect(StreamUtils.shuffle(mRandom))
			.findFirst().map(lg -> {
				List<Gene> genes = new ArrayList<>(t.size() + 4);
				
				t.genes()
					.map(g -> (g.marker() == lg.marker() ? lg.deactivate() : g))
					.forEach(genes::add);
				
				long id = mHistory.split(lg.marker());
				
				genes.add(mCallback.apply(id));
				genes.add(new LinkGene(mHistory.link(lg.from(), id), lg.from(), id, lg.weight(), true));
				genes.add(new LinkGene(mHistory.link(0, id), 0, id, 0, true));
				genes.add(new LinkGene(mHistory.link(id, lg.to()), id, lg.to(), 1, true));
				
				return new GeneSequence(genes);
			}).orElse(t);
	}
}
