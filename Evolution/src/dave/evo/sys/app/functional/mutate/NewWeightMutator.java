package dave.evo.sys.app.functional.mutate;

import java.util.function.DoubleSupplier;
import java.util.function.Function;

import dave.evo.dna.LinkGene;

public class NewWeightMutator implements Function<LinkGene, LinkGene>
{
	private final DoubleSupplier mCallback;
	
	public NewWeightMutator(DoubleSupplier f)
	{
		mCallback = f;
	}
	
	@Override
	public LinkGene apply(LinkGene g)
	{
		return new LinkGene(g.marker(), g.from(), g.to(), mCallback.getAsDouble(), g.active());
	}
}
