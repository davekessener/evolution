package dave.evo.sys.app.functional.mutate;

import java.util.function.Function;

public class MutatorAdapter<T, TT extends T> implements Function<T, T>
{
	private final Function<TT, TT> mCallback;
	private final Class<? extends TT> mClass;
	
	public MutatorAdapter(Class<? extends TT> c, Function<TT, TT> f)
	{
		mCallback = f;
		mClass = c;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T apply(T t)
	{
		if(mClass.isAssignableFrom(t.getClass()))
		{
			t = mCallback.apply((TT) t);
		}
		
		return t;
	}
}
