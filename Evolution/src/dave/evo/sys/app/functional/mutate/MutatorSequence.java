package dave.evo.sys.app.functional.mutate;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class MutatorSequence<T> implements Function<T, T>
{
	private final List<Function<T, T>> mMutators;
	
	@SafeVarargs
	public MutatorSequence(Function<T, T> ... f)
	{
		mMutators = Arrays.asList(f);
	}

	@Override
	public T apply(T t)
	{
		for(Function<T, T> f : mMutators)
		{
			t = f.apply(t);
		}
		
		return t;
	}
}
