package dave.evo.sys.app.functional.mutate;

import java.util.function.Function;

public class MutatorSplit<T> implements Function<T, T>
{
	private final Function<T, Boolean> mCondition;
	private final Function<T, T> mPrimary, mSecondary;
	
	public MutatorSplit(Function<T, Boolean> f, Function<T, T> f1, Function<T, T> f2)
	{
		mCondition = f;
		mPrimary = f1;
		mSecondary = f2;
	}

	@Override
	public T apply(T t)
	{
		return mCondition.apply(t) ? mPrimary.apply(t) : mSecondary.apply(t);
	}
}
