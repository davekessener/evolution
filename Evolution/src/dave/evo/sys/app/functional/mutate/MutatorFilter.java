package dave.evo.sys.app.functional.mutate;

import java.util.function.Function;
import java.util.function.Predicate;

public class MutatorFilter<T> implements Function<T, T>
{
	private final Function<T, T> mCallback;
	private final Predicate<T> mFilter;
	
	public MutatorFilter(Function<T, T> cb, Predicate<T> f)
	{
		mCallback = cb;
		mFilter = f;
	}
	
	@Override
	public T apply(T t)
	{
		if(mFilter.test(t))
		{
			t = mCallback.apply(t);
		}
		
		return t;
	}
}
