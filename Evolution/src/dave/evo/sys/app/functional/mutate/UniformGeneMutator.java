package dave.evo.sys.app.functional.mutate;

import java.util.function.Function;
import java.util.stream.Collectors;

import dave.evo.dna.Gene;
import dave.evo.dna.GeneSequence;
import dave.util.RNG;
import dave.util.stream.StreamUtils;

public class UniformGeneMutator implements Function<GeneSequence, GeneSequence>
{
	private final RNG mRandom;
	private final Function<Gene, Gene> mMutator;
	
	public UniformGeneMutator(RNG rng, Function<Gene, Gene> f)
	{
		mRandom = rng;
		mMutator = f;
	}
	
	@Override
	public GeneSequence apply(GeneSequence g)
	{
		final int idx = ((int) (g.size() * mRandom.nextDouble())) % g.size();
		
		return new GeneSequence(g.genes()
			.map(StreamUtils.stream_with_index())
			.map(e -> (e.index == idx ? mMutator.apply(e.value) : e.value))
			.collect(Collectors.toList()));
	}
}
