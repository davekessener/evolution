package dave.evo.sys.app.functional.mutate;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import dave.evo.common.GeneHistory;
import dave.evo.dna.BiasGene;
import dave.evo.dna.Gene;
import dave.evo.dna.GeneSequence;
import dave.evo.dna.InputGene;
import dave.evo.dna.LinkGene;
import dave.evo.dna.NeuronGene;
import dave.evo.dna.OutputGene;
import dave.util.RNG;
import dave.util.Utils;

public class NewLinkMutator implements Function<GeneSequence, GeneSequence>
{
	private final GeneHistory mHistory;
	private final RNG mRandom;
	
	public NewLinkMutator(GeneHistory h, RNG rng)
	{
		mHistory = h;
		mRandom = rng;
	}
	
	@Override
	public GeneSequence apply(GeneSequence genome)
	{
		List<NeuronGene> neurons = genome.genes()
			.filter(g -> (g instanceof NeuronGene))
			.map(g -> (NeuronGene) g)
			.collect(Collectors.toList());
		List<LinkGene> links = genome.genes()
			.filter(g -> (g instanceof LinkGene))
			.map(g -> (LinkGene) g)
			.collect(Collectors.toList());
		
		int nn = neurons.size(), nl = links.size();
		
		if(nl == nn * (nn + 1) / 2)
			return genome;
		
		Utils.shuffle(neurons, mRandom);

		for(NeuronGene from : neurons)
		{
			for(NeuronGene to : neurons)
			{
				long f = from.marker();
				long t = to.marker();
				
				if(from == to)
					continue;
				
				if(to instanceof InputGene)
					continue;
				
				if(to instanceof BiasGene)
					continue;
				
				if(from instanceof OutputGene)
					continue;
				
				if(links.stream().anyMatch(l -> l.from() == f && l.to() == t))
					continue;
				
//				if(reachable(genome, t, f))
//					continue;
				
				List<Gene> genes = new ArrayList<>(nn + nl + 1);
				
				neurons.forEach(genes::add);
				links.forEach(genes::add);
				genes.add(new LinkGene(mHistory.link(f, t), f, t, 0.2 * (mRandom.nextDouble() * 2 - 1), true));
				
				return new GeneSequence(genes);
			}
		}
		
		return genome;
	}
	
	public static boolean reachable(GeneSequence genome, long from, long to)
	{
		return (from == to) || genome.genes()
			.filter(g -> (g instanceof LinkGene))
			.map(g -> (LinkGene) g)
			.filter(g -> g.from() == from)
			.anyMatch(g -> reachable(genome, g.to(), to));
	}
}
