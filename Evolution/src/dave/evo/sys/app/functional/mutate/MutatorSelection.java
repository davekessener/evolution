package dave.evo.sys.app.functional.mutate;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import dave.util.RNG;

public class MutatorSelection<T> implements Function<T, T>
{
	private final List<Option<T>> mOptions;
	private final Function<T, T> mDefault;
	private final RNG mRandom;
	
	private MutatorSelection(List<Option<T>> o, Function<T, T> d, RNG rng)
	{
		mOptions = o;
		mDefault = d;
		mRandom = rng;
	}

	@Override
	public T apply(T t)
	{
		double v = mRandom.nextDouble();
		
		for(Option<T> o : mOptions)
		{
			if(v < o.chance)
			{
				return o.callback.apply(t);
			}
			else
			{
				v -= o.chance;
			}
		}
		
		return mDefault.apply(t);
	}
	
	private static class Option<T>
	{
		public final double chance;
		public final Function<T, T> callback;
		
		public Option(double chance, Function<T, T> callback)
		{
			this.chance = chance;
			this.callback = callback;
		}
	}
	
	public static class Builder<T>
	{
		private final Function<T, T> mDefault;
		private final List<Option<T>> mOptions;
		
		public Builder( ) { this(g -> g); }
		public Builder(Function<T, T> d)
		{
			mDefault = d;
			mOptions = new ArrayList<>();
		}
		
		public Builder<T> add(double c, Function<T, T> f) { mOptions.add(new Option<>(c, f)); return this; }
		
		public MutatorSelection<T> build(RNG rng)
		{
			return new MutatorSelection<>(mOptions, mDefault, rng);
		}
	}
}
