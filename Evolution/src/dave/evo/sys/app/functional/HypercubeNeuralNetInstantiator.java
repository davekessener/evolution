package dave.evo.sys.app.functional;

import java.util.function.Function;

import dave.evo.config.HypercubeOptions;
import dave.evo.dna.GeneSequence;
import dave.evo.common.NeuralNet;
import dave.evo.nn.BasicNeuralNet;
import dave.evo.nn.hyper.CPPNN;
import dave.evo.nn.hyper.HypercubePainter;
import dave.evo.nn.hyper.ScaledHypercubePainter;
import dave.evo.nn.hyper.Substrate;

public class HypercubeNeuralNetInstantiator implements Function<GeneSequence, NeuralNet>
{
	private final Substrate mSubstrate;
	private final HypercubeOptions mOptions;
	
	public HypercubeNeuralNetInstantiator(Substrate s, HypercubeOptions o)
	{
		mSubstrate = s;
		mOptions = o;
	}

	@Override
	public NeuralNet apply(GeneSequence g)
	{
		return BasicNeuralNet.build(mSubstrate.paint(scalePainter(new CPPNN(BasicNeuralNet.build(g)), mOptions)));
	}

	public static HypercubePainter scalePainter(HypercubePainter f, HypercubeOptions o)
	{
		return new ScaledHypercubePainter(f, v -> v * o.cppn_output_scale());
	}
}
