package dave.evo.sys.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.LongFunction;

import dave.evo.common.Agent;
import dave.evo.common.GeneHistory;
import dave.evo.common.Library;
import dave.evo.common.MessageBus;
import dave.evo.common.NetworkInterface;
import dave.evo.common.Options;
import dave.evo.dna.Chromosome;
import dave.evo.dna.GeneSequence;
import dave.evo.dna.HiddenGene;
import dave.evo.nn.BasicNeuralNet;
import dave.evo.common.NeuralNet;
import dave.evo.sys.app.modular.Module;
import dave.evo.sys.bus.ChromosomeCreatorAgent;
import dave.evo.sys.bus.GenotypeCreatorAgent;
import dave.evo.sys.bus.InstantiatorAgent;
import dave.evo.sys.bus.PolicyControlAgent;
import dave.evo.sys.bus.PopulationCreatorAgent;
import dave.evo.sys.bus.SimpleBus;
import dave.evo.sys.bus.StorageAgent;
import dave.evo.util.CreationWrapper;
import dave.json.JsonString;
import dave.json.JsonUtils;
import dave.util.SimpleBazaar;
import dave.util.TypeMultiplexedFunction;
import dave.util.Utils;
import dave.util.config.Configuration;
import dave.util.log.Logger;
import dave.util.Bazaar;
import dave.util.RNG;
import dave.util.Seed;
import dave.util.property.BindableProperty;
import dave.util.property.ObservableProperty;

public class App
{
	private final MessageBus mBus;
	private final ObservableProperty<Boolean> mRunning;
	
	private App(MessageBus bus, ObservableProperty<Boolean> r)
	{
		mBus = bus;
		mRunning = r;
	}
	
	public ObservableProperty<Boolean> runningProperty() { return mRunning; }
	
	public void run()
	{
		mRunning.set(true);
		
		mBus.start();
		
		while(mRunning.get())
		{
			Utils.sleep(100);
		}
	}
	
	public static class Builder
	{
		private final Configuration<Options> mConfig;
		private final RNG mRandom;
		private final GeneHistory mHistory;
		private final ObservableProperty<Boolean> mRunning;
		private final MessageBus mBus;
		private final List<Function<MessageBus, Agent>> mUser;
		private final Bazaar<String> mBazaar;
		private final TypeMultiplexedFunction.Builder<Chromosome, NeuralNet> mBuilder;
		
		public Builder(Configuration<Options> config, RNG rand)
		{
			mConfig = config;
			mRandom = rand;
			mHistory = new GeneHistory();
			
			mRunning = new BindableProperty<>();
			mBus = new SimpleBus(mRunning);
			mUser = new ArrayList<>();
			mBazaar = new SimpleBazaar<>(100);
			mBuilder = defaultChromosomeInstantiator();
			
			mBazaar.offer(Library.App.HIDDEN_CREATE, a -> (LongFunction<HiddenGene>) (id -> new HiddenGene(options().base_activation(), id)));
			mBazaar.offer(Library.App.CHROMOSOME_IO, a -> Arrays.asList((NetworkInterface) mBazaar.acquire(Library.App.EVALUATION_IO)));
			mBazaar.offer(Library.App.CHROMOSOME_CREATOR, a -> mBuilder);
			mBazaar.offer(Library.App.NN_CREATE, this::acquireNeuralNetInstantiator);
			mBazaar.offer(Library.App.PHENOTYPE_CREATE, this::acquirePhenotypeCreator);
			
			mBazaar.offer(Library.App.OPTIONS, a -> mConfig.proxy());
			mBazaar.offer(Library.App.CONFIG, a -> mConfig);
			mBazaar.offer(Library.App.RNG, a -> mRandom);
			mBazaar.offer(Library.App.HISTORY, a -> mHistory);
			mBazaar.offer(Library.App.BUS, a -> mBus);
			mBazaar.offer(Library.App.RUNNING, a -> mRunning);
			mBazaar.offer(Library.App.STORAGE_FILE, a -> options().save_filename());
			mBazaar.offer(Library.App.GENES_CREATE, a -> options().defaultCreator(random(), history()));
			mBazaar.offer(Library.App.GENES_MUTATE, a -> options().defaultMutator(random(), history(), mBazaar.acquire(Library.App.HIDDEN_CREATE)));
			mBazaar.offer(Library.App.GENES_BREED, a -> options().defaultBreeder(random()));
			mBazaar.offer(Library.App.AGENT_STORAGE, this::acquireStorageAgent);
			mBazaar.offer(Library.App.AGENT_CHROMOSOME_CREATE, this::acquireChromosomeAgent);
			mBazaar.offer(Library.App.AGENT_GENOTYPE_CREATE, this::acquireGenotypeAgent);
			mBazaar.offer(Library.App.AGENT_PHENOTYPE_CREATE, this::acquirePhenotypeAgent);
			mBazaar.offer(Library.App.AGENT_POPULATION_CREATE, this::acquirePopulationAgent);
			mBazaar.offer(Library.App.AGENT_CONTROL, this::acquireControlAgent);
			mBazaar.offer(Library.App.APP, this::acquireApp);
		}
		
		public Bazaar<String> supplier() { return mBazaar; }
		
		public RNG random() { return mBazaar.acquire(Library.App.RNG); }
		public Configuration<Options> config() { return mBazaar.acquire(Library.App.CONFIG); }
		public Options options() { return mBazaar.acquire(Library.App.OPTIONS); }
		public MessageBus bus() { return mBazaar.acquire(Library.App.BUS); }
		public GeneHistory history() { return mBazaar.acquire(Library.App.HISTORY); }
		public ObservableProperty<Boolean> running() { return mBazaar.acquire(Library.App.RUNNING); }
		
		private static TypeMultiplexedFunction.Builder<Chromosome, NeuralNet> defaultChromosomeInstantiator()
		{
			return (new TypeMultiplexedFunction.Builder<Chromosome, NeuralNet>())
				.register(GeneSequence.class, BasicNeuralNet::build);
		}
		
		private Function<Chromosome, NeuralNet> acquireNeuralNetInstantiator(Bazaar.Arguments<String> args)
		{
			TypeMultiplexedFunction.Builder<Chromosome, NeuralNet> builder = mBazaar.acquire(Library.App.CHROMOSOME_CREATOR);
			
			return builder.build();
		}
		
		private Function<List<NeuralNet>, NeuralNet> acquirePhenotypeCreator(Bazaar.Arguments<String> args)
		{
			return g -> {
				if(g.size() != 1)
					throw new UnsupportedOperationException("Cannot instantiate NN sized " + g.size());
				
				return g.get(0);
			};
		}
		
		private Agent acquireStorageAgent(Bazaar.Arguments<String> args)
		{
			RNG rng = random();
			GeneHistory history = history();
			StorageAgent a = new StorageAgent(bus(), mBazaar.acquire(Library.App.STORAGE_FILE));
			
			a.addState(Library.State.SEED, JsonUtils.wrap(
				() -> new JsonString(rng.getSeed().toString()),
				json -> rng.setSeed(new Seed(((JsonString) json).get()))));
			a.addState(Library.State.HISTORY, history);
			
			return a;
		}
		
		private Agent acquireChromosomeAgent(Bazaar.Arguments<String> args)
		{
			Function<NetworkInterface, Chromosome> creator = mBazaar.acquire(Library.App.GENES_CREATE);
			Function<Chromosome, Chromosome> mutator = mBazaar.acquire(Library.App.GENES_MUTATE);
			BiFunction<Chromosome, Chromosome, Chromosome> breeder = mBazaar.acquire(Library.App.GENES_BREED);
			
			return new ChromosomeCreatorAgent(bus(), new CreationWrapper<>(creator, mutator, breeder));
		}
		
		private Agent acquireGenotypeAgent(Bazaar.Arguments<String> args)
		{
			return new GenotypeCreatorAgent(bus(), mBazaar.acquire(Library.App.CHROMOSOME_IO));
		}
		
		private Agent acquirePhenotypeAgent(Bazaar.Arguments<String> args)
		{
			return new InstantiatorAgent(bus(), mBazaar.acquire(Library.App.NN_CREATE), mBazaar.acquire(Library.App.PHENOTYPE_CREATE));
		}
		
		private Agent acquirePopulationAgent(Bazaar.Arguments<String> args)
		{
			return new PopulationCreatorAgent(bus(), random(), options());
		}
		
		private Agent acquireControlAgent(Bazaar.Arguments<String> args)
		{
			return new PolicyControlAgent(bus(), mBazaar.acquire(Library.App.POLICY), running(), options());
		}
		
		private App acquireApp(Bazaar.Arguments<String> args)
		{
			bus().register(mBazaar.acquire(Library.App.AGENT_CHROMOSOME_CREATE));
			bus().register(mBazaar.acquire(Library.App.AGENT_GENOTYPE_CREATE));
			bus().register(mBazaar.acquire(Library.App.AGENT_PHENOTYPE_CREATE));
			bus().register(mBazaar.acquire(Library.App.AGENT_POPULATION_CREATE));
			bus().register(mBazaar.acquire(Library.App.AGENT_EVALUATE));
			bus().register(mBazaar.acquire(Library.App.AGENT_STORAGE));
			bus().register(mBazaar.acquire(Library.App.AGENT_CONTROL));
			
			mUser.forEach(f -> bus().register(f.apply(bus())));
			
			return new App(bus(), running());
		}
		
		public Builder installAll(Collection<? extends Module> l)
		{
			l.forEach(this::install);
			
			return this;
		}
		
		public Builder install(Module m)
		{
			LOG.info("Installing " + m);
			
			m.install(this);
			
			return this;
		}
		
		public Builder addAgent(Function<MessageBus, Agent> f) { mUser.add(f); return this; }
		
		public App build()
		{
			return mBazaar.acquire(Library.App.APP);
		}
		
		private static final Logger LOG = Logger.get("app");
	}
}
