package dave.evo.sys.app.script;

import java.util.function.BiFunction;

import dave.evo.common.NetworkInterface;
import dave.evo.common.NeuralNet;
import dave.evo.common.Options;
import dave.evo.eval.MatchResult;
import dave.evo.eval.ttt.TicTacToeEvaluator;
import dave.evo.nn.hyper.Substrate;
import dave.util.RNG;
import dave.util.Utils;

public class TicTacToeGoal extends AdversarialGoal
{
	public TicTacToeGoal()
	{
		super(TICTACTOE_SUBSTRATE_INTERFACE);
	}
	
	@Override
	public String toString()
	{
		return "TicTacToeGoal";
	}
	
	@Override
	protected BiFunction<NeuralNet, NeuralNet, MatchResult> evaluator(RNG rng, Options o)
	{
		return new TicTacToeEvaluator(rng);
	}

	public static final NetworkInterface TICTACTOE_SUBSTRATE_INTERFACE = Utils.tap(new NetworkInterface.Builder(0, 0), b -> {
		for(int y = 0 ; y < 3 ; ++y)
		{
			for(int x = 0 ; x < 3 ; ++x)
			{
				double px = (x + 1) / 3.0;
				double py = (y + 1) / 3.0;

				b.addInput(-px, -py);
				b.addOutput(px, py);
			}
		}
	}).build();
	
	public static final Substrate TICTACTOE_SUBSTRATE = Utils.tap(new Substrate.Builder(TICTACTOE_SUBSTRATE_INTERFACE.bias), b -> {
		NetworkInterface s = TICTACTOE_SUBSTRATE_INTERFACE;
		
		Substrate.Node bias = b.bias();

		Substrate.Node[] in = new Substrate.Node[9];
		Substrate.Node[] out = new Substrate.Node[9];
		
		for(int i = 0 ; i < 9 ; ++i)
		{
			in[i] = b.addInput(s.inputs.get(i));
			out[i] = b.addOutput(s.outputs.get(i), "TANH");
		}

		Substrate.Node h1 = b.addHidden(-0.5, 0.5, "TANH");
		Substrate.Node h2 = b.addHidden(0.5, -0.5, "TANH");
		
		for(int i = 0 ; i < 9 ; ++i)
		{
			b.link(bias, out[i]);
			
			for(int j = 0 ; j < 9 ; ++j)
			{
				b.link(in[j], out[i]);
			}
			
			b.link(in[i], h1);
			b.link(in[i], h2);

			b.link(h1, out[i]);
			b.link(h2, out[i]);
		}

		b.link(bias, h1);
		b.link(bias, h2);
	}).build();
}
