package dave.evo.sys.app.script;

import java.util.function.ToDoubleFunction;

import dave.evo.common.NetworkInterface;
import dave.evo.common.NeuralNet;
import dave.evo.common.Options;
import dave.evo.eval.maze.MazeEvaluator;
import dave.evo.eval.maze.MazeOptions;
import dave.util.RNG;

public class SimpleMazeGoal extends IndependentGoal
{
	private final MazeOptions mOptions;
	
	public SimpleMazeGoal(MazeOptions o)
	{
		super(SIMPLE_MAZE_IO);
		
		mOptions = o;
	}

	@Override
	public String toString()
	{
		return "Maze[Simple]Goal";
	}
	
	@Override
	protected ToDoubleFunction<NeuralNet> evaluator(RNG rng, Options o)
	{
		return new MazeEvaluator(mOptions, rng);
	}
	
	public static final NetworkInterface SIMPLE_MAZE_IO = (new NetworkInterface.Builder(0, 0))
		.addInput(-0.5, -1)
		.addInput( 0.0, -1)
		.addInput( 0.5, -1)
		.addOutput(-0.5, 1)
		.addOutput( 0.0, 1)
		.addOutput( 0.5, 1)
		.build();
}
