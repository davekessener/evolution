package dave.evo.sys.app.script;

import java.util.function.ToDoubleFunction;

import dave.evo.common.NetworkInterface;
import dave.evo.common.NeuralNet;
import dave.evo.common.Options;
import dave.evo.eval.life.LifeEvaluator;
import dave.evo.eval.life.LifeOptions;
import dave.util.RNG;

public class LifeGoal extends IndependentGoal
{
	private final LifeOptions mOptions;
	
	public LifeGoal(LifeOptions o)
	{
		super(LIFE_IO);
		
		mOptions = o;
	}

	@Override
	public String toString()
	{
		return "Maze[Full]Goal";
	}
	
	@Override
	protected ToDoubleFunction<NeuralNet> evaluator(RNG rng, Options o)
	{
		return new LifeEvaluator(mOptions, rng);
	}
	
	public static final NetworkInterface LIFE_IO = (new NetworkInterface.Builder(0, 0))
		.addInput(-0.25, 0)
		.addInput(0, 0.25)
		.addInput(0.25, 0)
		.addInput(-1, -1)
		.addInput(0, -1)
		.addInput(1, -1)
		.addOutput(-0.75, 0)
		.addOutput(0, 0.75)
		.addOutput(0.75, 0)
		.build();
}
