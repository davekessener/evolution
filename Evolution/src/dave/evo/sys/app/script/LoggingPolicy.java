package dave.evo.sys.app.script;



import java.util.stream.Collectors;

import dave.evo.common.Policy;
import dave.evo.dna.GeneSequence;
import dave.evo.dna.Genome;
import dave.evo.dna.Population;
import dave.json.PrettyPrinter;
import dave.util.Averager;
import dave.util.Utils;
import dave.util.log.Logger;

public class LoggingPolicy implements Policy
{
	private final Policy mSuper;
	private final Averager mTimePerGen;
	private double mHighscore;
	private long mLastTime;
	private int mCount, mLastCount;
	
	public LoggingPolicy(Policy s)
	{
		mSuper = s;
		mTimePerGen = new Averager();
		
		mHighscore = 0;
		mLastTime = System.currentTimeMillis();
		mLastCount = 0;
		mCount = 0;
	}
	
	@Override
	public boolean done(Population pop)
	{
		++mCount;
		
		if(pop.highscore() > mHighscore)
		{
			mHighscore = pop.highscore();
			
			Utils.writeToFile("best.json", pop.best().get().save().toString(new PrettyPrinter()));
			
			log(pop);
		}
		else if(System.currentTimeMillis() - mLastTime > 10000)
		{
			log(pop);
		}
		
		return mSuper.done(pop);
	}
	
	private void log(Population p)
	{
		long t = System.currentTimeMillis();
		Genome g = p.best().orElse(null);
		int s = p.speciesCount();
		double passed = ((t - mLastTime) / 1000.0) / (mCount - mLastCount);
		
		mTimePerGen.add(passed);
		
		LOG.info("Highscore %d (%.1fs - %.1fs avg): %.2f (%s genes, %d species)",
			mCount, passed, mTimePerGen.get(), mHighscore, (g == null ? "-/-" : g.chromosomes()
					.map(c -> ((c instanceof GeneSequence) ? "" + ((GeneSequence) c).size() : "-"))
					.collect(Collectors.joining(", "))), s);
		
		mLastTime = t;
		mLastCount = mCount;
	}
	
	@Override
	public String toString()
	{
		return mSuper.toString() + "[Logged]";
	}
	
	private static final Logger LOG = Logger.get("exec");
}
