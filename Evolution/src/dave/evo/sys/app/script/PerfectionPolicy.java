package dave.evo.sys.app.script;

import dave.evo.common.Policy;
import dave.evo.dna.Population;

public class PerfectionPolicy implements Policy
{
	@Override
	public boolean done(Population pop)
	{
		return pop.highscore() > 0.999;
	}

	@Override
	public String toString()
	{
		return "PerfectionPolicy";
	}
}
