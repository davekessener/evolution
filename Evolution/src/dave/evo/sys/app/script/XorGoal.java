package dave.evo.sys.app.script;

import dave.evo.common.NetworkInterface;
import dave.evo.eval.XorEvaluator;
import dave.evo.nn.hyper.Substrate;
import dave.util.Utils;

public class XorGoal extends IndependentGoal
{
	public XorGoal()
	{
		super(XOR_SUBSTRATE_INTERFACE, new XorEvaluator());
	}
	
	@Override
	public String toString()
	{
		return "XorGoal";
	}
	
	public static final NetworkInterface XOR_SUBSTRATE_INTERFACE = (new NetworkInterface.Builder(0, 0))
			.addInput(-1, -1)
			.addInput( 1, -1)
			.addOutput(0, 1)
			.build();

	public static final Substrate XOR_SUBSTRATE = Utils.tap(new Substrate.Builder(XOR_SUBSTRATE_INTERFACE.bias), b -> {
		NetworkInterface i = XOR_SUBSTRATE_INTERFACE;

		Substrate.Node i0 = b.addInput(i.inputs.get(0));
		Substrate.Node i1 = b.addInput(i.inputs.get(1));
		Substrate.Node h0 = b.addHidden(-0.5, 0, "TANH");
		Substrate.Node h1 = b.addHidden( 0.5, 0, "TANH");
		Substrate.Node out = b.addOutput(i.outputs.get(0), "TANH");

		b.link(b.bias(), h0);
		b.link(i0, h0);
		b.link(i1, h0);
		
		b.link(b.bias(), h1);
		b.link(i0, h1);
		b.link(i1, h1);

		b.link(b.bias(), out);
		b.link(i0, out);
		b.link(i1, out);
		b.link(h0, out);
		b.link(h1, out);
	}).build();
	
}
