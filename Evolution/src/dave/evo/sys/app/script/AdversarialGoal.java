package dave.evo.sys.app.script;

import java.util.function.BiFunction;

import dave.evo.common.Agent;
import dave.evo.common.MessageBus;
import dave.evo.common.NetworkInterface;
import dave.evo.common.Options;
import dave.evo.eval.MatchResult;
import dave.evo.common.NeuralNet;
import dave.evo.sys.bus.BatchEvaluatorAgent;
import dave.util.RNG;

public abstract class AdversarialGoal extends BaseGoal
{
	private final BiFunction<NeuralNet, NeuralNet, MatchResult> mEvaluator;
	
	protected AdversarialGoal(NetworkInterface i) { this(i, null); }
	protected AdversarialGoal(NetworkInterface i, BiFunction<NeuralNet, NeuralNet, MatchResult> f)
	{
		super(i);
		
		mEvaluator = f;
	}
	
	protected BiFunction<NeuralNet, NeuralNet, MatchResult> evaluator(RNG rng, Options o)
	{
		return mEvaluator;
	}

	@Override
	public Agent getEvaluator(MessageBus bus, RNG rng, Options o)
	{
		return new BatchEvaluatorAgent(bus, evaluator(rng, o), o);
	}
}
