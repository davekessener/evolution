package dave.evo.sys.app.script;

import java.util.function.ToDoubleFunction;

import dave.evo.common.NetworkInterface;
import dave.evo.common.NeuralNet;
import dave.evo.common.Options;
import dave.evo.eval.seq.SequenceEvaluator;
import dave.evo.eval.seq.SequenceOptions;
import dave.util.RNG;

public class SequencerGoal extends IndependentGoal
{
	private final SequenceOptions mOptions;
	
	public SequencerGoal(SequenceOptions o)
	{
		super(SEQUENCER_IO);
		
		mOptions = o;
	}
	
	@Override
	protected ToDoubleFunction<NeuralNet> evaluator(RNG rng, Options o)
	{
		return new SequenceEvaluator(mOptions, rng, o);
	}
	
	@Override
	public String toString()
	{
		return "SequencerGoal";
	}
	
	public static final NetworkInterface SEQUENCER_IO = (new NetworkInterface.Builder(0, 0))
		.addInput(-1, -1)
		.addInput(1, -1)
		.addOutput(0, 1)
		.build();
}
