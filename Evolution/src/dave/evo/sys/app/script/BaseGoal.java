package dave.evo.sys.app.script;

import dave.evo.common.Goal;
import dave.evo.common.NetworkInterface;

public abstract class BaseGoal implements Goal
{
	private final NetworkInterface mInterface;
	
	protected BaseGoal(NetworkInterface i)
	{
		mInterface = i;
	}
	
	@Override
	public NetworkInterface getInterface()
	{
		return mInterface;
	}
}
