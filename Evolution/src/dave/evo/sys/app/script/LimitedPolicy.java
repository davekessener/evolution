package dave.evo.sys.app.script;

import dave.evo.common.Policy;
import dave.evo.dna.Population;
import dave.util.Counter;

public class LimitedPolicy implements Policy
{
	private final int mMaxTries;
	private final double mMaxFitness;
	private final Counter mCounter;
	
	public LimitedPolicy(int tries, double f)
	{
		mMaxTries = tries;
		mMaxFitness = f;
		mCounter = new Counter();
	}

	@Override
	public boolean done(Population pop)
	{
		mCounter.inc();
		
		if(mCounter.get() >= mMaxTries)
			return true;
		
		if(pop.highscore() >= mMaxFitness)
			return true;
		
		return false;
	}
	
	@Override
	public String toString()
	{
		return String.format("Limited[tries=%d,fitness=%.2f]Policy", mMaxTries, mMaxFitness);
	}
}
