package dave.evo.sys.app.script;

import java.util.function.ToDoubleFunction;

import dave.evo.common.NetworkInterface;
import dave.evo.common.NeuralNet;
import dave.evo.common.Options;
import dave.evo.eval.counter.CountingEvaluator;
import dave.evo.eval.counter.CountingOptions;
import dave.util.RNG;

public class CountingGoal extends IndependentGoal
{
	private final CountingOptions mOptions;
	
	public CountingGoal(CountingOptions o)
	{
		super(COUNTING_SUBSTRATE_INTERFACE);
		
		mOptions = o;
	}
	
	@Override
	public String toString()
	{
		return "CountingGoal";
	}
	
	@Override
	protected ToDoubleFunction<NeuralNet> evaluator(RNG rng, Options o)
	{
		return new CountingEvaluator(mOptions, rng, o);
	}
	
	public static NetworkInterface COUNTING_SUBSTRATE_INTERFACE = (new NetworkInterface.Builder(0, 0))
		.addInput(-1, -1)
		.addOutput(1, 1)
		.build();
}
