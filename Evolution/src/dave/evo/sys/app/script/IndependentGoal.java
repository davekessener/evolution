package dave.evo.sys.app.script;

import java.util.function.ToDoubleFunction;

import dave.evo.common.Agent;
import dave.evo.common.MessageBus;
import dave.evo.common.Options;
import dave.evo.common.NetworkInterface;
import dave.evo.common.NeuralNet;
import dave.evo.sys.bus.SimpleEvaluatorAgent;
import dave.util.RNG;

public abstract class IndependentGoal extends BaseGoal
{
	private final ToDoubleFunction<NeuralNet> mEvaluator;
	
	protected IndependentGoal(NetworkInterface i) { this(i, null); }
	protected IndependentGoal(NetworkInterface i, ToDoubleFunction<NeuralNet> f)
	{
		super(i);
		
		mEvaluator = f;
	}
	
	protected ToDoubleFunction<NeuralNet> evaluator(RNG rng, Options o)
	{
		return mEvaluator;
	}

	@Override
	public Agent getEvaluator(MessageBus bus, RNG rng, Options o)
	{
		return new SimpleEvaluatorAgent(bus, evaluator(rng, o));
	}
}
