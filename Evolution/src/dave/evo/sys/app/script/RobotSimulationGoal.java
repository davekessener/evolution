package dave.evo.sys.app.script;

import java.util.function.ToDoubleFunction;

import dave.evo.common.NetworkInterface;
import dave.evo.common.NeuralNet;
import dave.evo.common.Options;
import dave.evo.eval.robot.FoodWorldGenerator;
import dave.evo.eval.robot.MazeWorldGenerator;
import dave.evo.eval.robot.RobotEvaluator;
import dave.evo.eval.robot.SimulationOptions;
import dave.util.RNG;

public class RobotSimulationGoal extends IndependentGoal
{
	private final SimulationOptions mOptions;
	
	public RobotSimulationGoal(SimulationOptions o)
	{
		super(ROBOT_IO);
		
		mOptions = o;
	}
	
	@Override
	protected ToDoubleFunction<NeuralNet> evaluator(RNG rng, Options o)
	{
		return new RobotEvaluator(mOptions,
			new MazeWorldGenerator(rng, mOptions),
			new FoodWorldGenerator(rng, mOptions));
	}
	
	@Override
	public String toString()
	{
		return "RobotSimulationGoal";
	}

	
	public static final NetworkInterface ROBOT_IO = (new NetworkInterface.Builder(0, 0))
		.addInput( 0.0,  0.5)
		.addInput( 0.5,  0.0)
		.addInput( 0.0, -0.5)
		.addInput(-0.5,  0.0)
		.addInput(-0.25,  0.0 )
		.addInput(-0.25,  0.25)
		.addInput( 0.0 ,  0.25)
		.addInput( 0.25,  0.25)
		.addInput( 0.25,  0.0 )
		.addOutput(-0.75, 0)
		.addOutput(0, 0.75)
		.addOutput(0.75, 0)
		.build();
}
