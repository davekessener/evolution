package dave.evo.sys.app.modular;

import java.util.List;
import java.util.function.Function;
import java.util.function.LongFunction;
import java.util.stream.Collectors;

import dave.evo.common.Library;
import dave.evo.common.NetworkInterface;
import dave.evo.common.Options;
import dave.evo.dna.HiddenGene;
import dave.evo.nn.Activation;
import dave.evo.nn.BasicNeuralNet;
import dave.evo.nn.es.SubstrateGenerator;
import dave.evo.nn.hyper.CPPNN;
import dave.evo.common.NeuralNet;
import dave.evo.sys.app.App.Builder;
import dave.evo.sys.app.functional.HypercubeNeuralNetInstantiator;
import dave.util.Bazaar;
import dave.util.Utils;
import dave.util.stream.StreamUtils;

public class ESHyperNEATBuildModule implements Module
{
	@Override
	public void install(Builder build)
	{
		build.supplier().offer(Library.App.HIDDEN_CREATE, a -> (LongFunction<HiddenGene>) (id -> new HiddenGene(Activation.any(build.random()).toString(), id)));
		build.supplier().offer(Library.App.CHROMOSOME_IO, this::interfaceProxy, Bazaar.options().setPriority(1));
		build.supplier().offer(Library.App.PHENOTYPE_CREATE, this::build, Bazaar.options().setPriority(1));
	}
	
	private List<NetworkInterface> interfaceProxy(Bazaar.Arguments<String> args)
	{
		if(args.size() == 1 && (boolean) args.get(0))
			throw new Bazaar.Refusal("requested original");
		
		return StreamUtils.map(args.bazaar().acquire(Library.App.CHROMOSOME_IO, true), io -> CPPNN_IO);
	}
	
	private Function<List<NeuralNet>, NeuralNet> build(Bazaar.Arguments<String> args)
	{
		if(args.size() == 1 && (boolean) args.get(0))
			throw new Bazaar.Refusal("requested original");
		
		Options options = args.bazaar().acquire(Library.App.OPTIONS);
		List<NetworkInterface> interfaces = args.bazaar().acquire(Library.App.CHROMOSOME_IO, true);
		Function<List<NeuralNet>, NeuralNet> original = args.bazaar().acquire(Library.App.PHENOTYPE_CREATE, true);
		
		return l -> {
			if(l.size() != interfaces.size())
				throw new IllegalArgumentException(String.format("Expected list sized %d, received %d instead!", interfaces.size(), l.size()));
			
			return original.apply(StreamUtils.zip(l.stream(), interfaces.stream())
				.map(p -> Utils.pair(new CPPNN(p.first), new SubstrateGenerator(p.second, options)))
				.map(p -> p.second.apply(p.first).paint(HypercubeNeuralNetInstantiator.scalePainter(p.first, options)))
				.map(BasicNeuralNet::build)
				.collect(Collectors.toList()));
		};
	}
	
	@Override
	public String toString()
	{
		return "ESHyperNEATModule";
	}
	
	private static final NetworkInterface CPPNN_IO = NetworkInterface.buildSimple(5, 1);
}
