package dave.evo.sys.app.modular;

import dave.evo.common.Goal;
import dave.evo.common.Library;
import dave.evo.sys.app.App.Builder;

public class GoalModule implements Module
{
	private final Goal mGoal;
	
	public GoalModule(Goal goal)
	{
		mGoal = goal;
	}
	
	@Override
	public void install(Builder build)
	{
		build.supplier().offer(Library.App.EVALUATION_IO, a -> mGoal.getInterface());
		build.supplier().offer(Library.App.AGENT_EVALUATE, a -> mGoal.getEvaluator(build.bus(), build.random(), build.options()));
	}
	
	@Override
	public String toString()
	{
		return "Goal[" + mGoal + "]Module";
	}
}
