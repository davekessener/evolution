package dave.evo.sys.app.modular;

import java.util.function.BiFunction;
import java.util.function.Function;

import dave.evo.common.Library;
import dave.evo.common.NetworkInterface;
import dave.evo.common.NeuralNet;
import dave.evo.common.Options;
import dave.evo.dna.Chromosome;
import dave.evo.dna.WeightMatrix;
import dave.evo.nn.Activation;
import dave.evo.nn.FlatNeuralNet;
import dave.evo.sys.app.App.Builder;
import dave.util.RNG;
import dave.util.TypeMultiplexedFunction;
import dave.util.math.Matrix;

public class FlatModule implements Module
{
	@Override
	public void install(Builder build)
	{
		build.supplier().offer(Library.App.GENES_CREATE, a -> genes_create(build.options(), build.random()));
		build.supplier().offer(Library.App.GENES_MUTATE, a -> genes_mutate(build.options(), build.random()));
		build.supplier().offer(Library.App.GENES_BREED, a -> genes_breed(build.options(), build.random()));
		
		TypeMultiplexedFunction.Builder<Chromosome, NeuralNet> f = build.supplier().acquire(Library.App.CHROMOSOME_CREATOR);
		
		f.register(WeightMatrix.class, wm -> new FlatNeuralNet(wm.matrix(), Activation.get(Activation.Type.valueOf(wm.activation()))));
	}
	
	private Function<NetworkInterface, WeightMatrix> genes_create(Options o, RNG rng)
	{
		return io -> {
			double[] weights = new double[io.inputs.size() * io.outputs.size()];
			
			for(int i = 0 ; i < weights.length ; ++i)
			{
				weights[i] = o.initial_weight() * (rng.nextDouble() * 2 - 1);
			}
			
			return new WeightMatrix(new Matrix(io.inputs.size(), io.outputs.size(), weights), o.base_activation());
		};
	}
	
	private Function<WeightMatrix, WeightMatrix> genes_mutate(Options o, RNG rng)
	{
		return wm -> {
			Matrix m = wm.matrix();
			int w = m.width(), h = m.height();
			double[] n = new double[w * h];
			
			for(int y = 0 ; y < h ; ++y)
			{
				for(int x = 0 ; x < w ; ++x)
				{
					n[x + y * w] = m.get(x, y);
					
					if(rng.nextDouble() < o.flat_mutation_chance())
					{
						n[x + y * w] += o.flat_mutation_weight() * rng.nextGauss();
					}
				}
			}
			
			return new WeightMatrix(new Matrix(w, h, n), wm.activation());
		};
	}
	
	private BiFunction<WeightMatrix, WeightMatrix, WeightMatrix> genes_breed(Options o, RNG rng)
	{
		return (w0, w1) -> {
			return (rng.nextDouble() < 0.5 ? w0 : w1);
		};
	}

	@Override
	public String toString()
	{
		return "FlatNeuralNetModule";
	}
}
