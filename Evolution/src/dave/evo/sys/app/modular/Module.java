package dave.evo.sys.app.modular;

import dave.evo.sys.app.App;

public interface Module
{
	public abstract void install(App.Builder build);
}
