package dave.evo.sys.app.modular;

import dave.evo.common.Library;
import dave.evo.common.Policy;
import dave.evo.sys.app.App.Builder;

public class ControlModule implements Module
{
	private final Policy mPolicy;
	
	public ControlModule(Policy p)
	{
		mPolicy = p;
	}

	@Override
	public void install(Builder build)
	{
		build.supplier().offer(Library.App.POLICY, a -> mPolicy);
	}
	
	@Override
	public String toString()
	{
		return "Policy[" + mPolicy + "]ControlModule";
	}
}
