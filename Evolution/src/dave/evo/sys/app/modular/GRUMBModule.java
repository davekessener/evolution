package dave.evo.sys.app.modular;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import dave.evo.common.NetworkInterface;
import dave.evo.common.NeuralNet;
import dave.evo.nn.GRUMB;
import dave.util.math.IVec2;

public class GRUMBModule extends MemoryModule
{
	public GRUMBModule(int m)
	{
		super("GRUMB", m);
	}

	@Override
	protected List<IVec2> getInterfaceList(NetworkInterface io)
	{
		List<IVec2> r = new ArrayList<>();
		int c = capacity();
		int in = io.inputs.size(), out = io.outputs.size();
		int total = in + out + c;
		
		for(int i = 0 ; i < 3 ; ++i)
		{
			r.add(new IVec2(total, c));
		}

		r.add(new IVec2(in + c, c));
		r.add(new IVec2(c, out));
		
		return r;
	}

	@Override
	protected Function<List<NeuralNet>, NeuralNet> createInstantiator()
	{
		return l -> new GRUMB(capacity(), l.get(0), l.get(1), l.get(2), l.get(3), l.get(4));
	}
}
