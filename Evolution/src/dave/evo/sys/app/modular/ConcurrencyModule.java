package dave.evo.sys.app.modular;

import dave.evo.common.Agent;
import dave.evo.common.Library;
import dave.evo.common.MessageBus;
import dave.evo.sys.app.App.Builder;
import dave.evo.sys.bus.ConcurrentBus;
import dave.evo.sys.bus.ConcurrentEvaluatorAgent;
import dave.util.Bazaar;
import dave.util.ConcurrentRNG;

public class ConcurrencyModule implements Module
{
	@Override
	public void install(Builder build)
	{
		build.supplier().offer(Library.App.RNG, new Bazaar.StickyWrapper<>(
			a -> new ConcurrentRNG(a.bazaar().acquire(Library.App.RNG))));
		build.supplier().offer(Library.App.BUS, new Bazaar.StickyWrapper<>(
			a -> new ConcurrentBus(a.bazaar().acquire(Library.App.RUNNING))));
		build.supplier().offer(Library.App.AGENT_EVALUATE, this::acquireEvaluator);
	}
	
	private Agent acquireEvaluator(Bazaar.Arguments<String> args)
	{
		if(args.size() == 1 && (boolean) args.get(0))
			throw new Bazaar.Refusal("requested original");
		
		MessageBus bus = args.bazaar().acquire(Library.App.BUS);
		Agent a = args.bazaar().acquire(Library.App.AGENT_EVALUATE, true);
		
		return new ConcurrentEvaluatorAgent(bus, a);
	}
	
	@Override
	public String toString()
	{
		return "ConcurrencyModule";
	}
}
