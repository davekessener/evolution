package dave.evo.sys.app.modular;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import dave.evo.common.Library;
import dave.evo.common.NetworkInterface;
import dave.evo.common.NeuralNet;
import dave.evo.sys.app.App.Builder;
import dave.util.Bazaar;
import dave.util.math.IVec2;

public abstract class MemoryModule implements Module
{
	private final String mName;
	private final int mMemoryCapacity;
	
	protected MemoryModule(String name, int m)
	{
		mName = name;
		mMemoryCapacity = m;
	}
	
	protected int capacity() { return mMemoryCapacity; }
	
	protected abstract List<IVec2> getInterfaceList(NetworkInterface io);
	protected abstract Function<List<NeuralNet>, NeuralNet> createInstantiator( );
	
	@Override
	public void install(Builder build)
	{
		build.supplier().offer(Library.App.CHROMOSOME_IO, this::acquireInterfaces);
		build.supplier().offer(Library.App.PHENOTYPE_CREATE, this::acquireInstantiator);
	}
	
	protected List<NetworkInterface> acquireInterfaces(Bazaar.Arguments<String> args)
	{
		NetworkInterface io = args.bazaar().acquire(Library.App.EVALUATION_IO);
		
		return getInterfaceList(io).stream()
			.map(v -> NetworkInterface.buildSimple(v.x, v.y))
			.collect(Collectors.toList());
	}
	
	protected Function<List<NeuralNet>, NeuralNet> acquireInstantiator(Bazaar.Arguments<String> args)
	{
		return createInstantiator();
	}
	
	@Override
	public String toString()
	{
		return "Memory[" + mName + ":" + mMemoryCapacity + "]Module";
	}
}
