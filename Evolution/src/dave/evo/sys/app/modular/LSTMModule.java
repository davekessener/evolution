package dave.evo.sys.app.modular;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import dave.evo.common.NetworkInterface;
import dave.evo.common.NeuralNet;
import dave.evo.nn.LSTM;
import dave.util.math.IVec2;

public class LSTMModule extends MemoryModule
{
	public LSTMModule(int m)
	{
		super("LSTM", m);
	}
	
	@Override
	protected List<IVec2> getInterfaceList(NetworkInterface io)
	{
		List<IVec2> r = new ArrayList<>();
		int c = capacity();
		
		for(int i = 0 ; i < 4 ; ++i)
		{
			r.add(new IVec2(io.inputs.size() + c, c));
		}
		
		r.add(new IVec2(c, io.outputs.size()));
		
		return r;
	}

	@Override
	protected Function<List<NeuralNet>, NeuralNet> createInstantiator()
	{
		return l -> new LSTM(capacity(), l.get(0), l.get(1), l.get(2), l.get(3), l.get(4));
	}
}
