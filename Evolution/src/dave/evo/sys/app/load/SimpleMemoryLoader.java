package dave.evo.sys.app.load;

import java.util.function.Function;

import dave.evo.sys.app.modular.MemoryModule;
import dave.json.JsonValue;

public class SimpleMemoryLoader implements MemoryLoader
{
	private final String mID;
	private final Function<JsonValue, MemoryModule> mCallback;
	
	public SimpleMemoryLoader(String id, Function<JsonValue, MemoryModule> f)
	{
		mID = id;
		mCallback = f;
	}

	@Override
	public String getID()
	{
		return mID;
	}

	@Override
	public MemoryModule load(JsonValue json)
	{
		return mCallback.apply(json);
	}
}
