package dave.evo.sys.app.load;

import dave.evo.common.Policy;
import dave.json.JsonValue;
import dave.util.Identifiable;

public interface PolicyLoader extends Identifiable
{
	public abstract Policy load(Policy s, JsonValue json);
}
