package dave.evo.sys.app.load;

import dave.evo.common.Library;
import dave.evo.common.Policy;
import dave.evo.sys.app.modular.ControlModule;
import dave.evo.sys.app.modular.Module;
import dave.json.JsonObject;
import dave.json.JsonValue;

public class ControlModuleLoader implements ModuleLoader
{
	@Override
	public String getID()
	{
		return Library.Modules.CONTROL;
	}

	@Override
	public Module load(JsonValue json)
	{
		Policy policy = ((JsonObject) json).stream().reduce(
			(Policy) null, 
			(p, e) -> LoadService.INSTANCE.loadPolicy(e.getKey(), p, e.getValue()),
			(p1, p2) -> { throw new UnsupportedOperationException(); });
		
		return new ControlModule(policy);
	}
}
