package dave.evo.sys.app.load;

import java.util.function.Function;

import dave.evo.common.Goal;
import dave.json.JsonValue;

public class SimpleGoalLoader implements GoalLoader
{
	private final String mID;
	private final Function<JsonValue, Goal> mCallback;
	
	public SimpleGoalLoader(String id, Function<JsonValue, Goal> f)
	{
		mID = id;
		mCallback = f;
	}

	@Override
	public String getID()
	{
		return mID;
	}

	@Override
	public Goal load(JsonValue json)
	{
		return mCallback.apply(json);
	}
}
