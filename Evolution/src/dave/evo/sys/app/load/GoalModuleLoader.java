package dave.evo.sys.app.load;

import dave.evo.common.Library;
import dave.evo.sys.app.modular.GoalModule;
import dave.evo.sys.app.modular.Module;
import dave.json.JsonObject;
import dave.json.JsonValue;

public class GoalModuleLoader implements ModuleLoader
{
	@Override
	public String getID()
	{
		return Library.Modules.GOAL;
	}

	@Override
	public Module load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		String id = o.getString("goal");
		JsonValue options = (o.contains("options") ? o.get("options") : null);
		
		return new GoalModule(LoadService.INSTANCE.loadGoal(id, options));
	}
}
