package dave.evo.sys.app.load;

import java.util.function.Function;

import dave.evo.common.Goal;

public class SimpleParametrizedGoalLoader<T> extends ParametrizedGoalLoader<T>
{
	private final Function<T, Goal> mCallback;
	
	public SimpleParametrizedGoalLoader(String id, Class<T> c, Function<T, Goal> f)
	{
		super(id, c);
		
		mCallback = f;
	}

	@Override
	protected Goal create(T options)
	{
		return mCallback.apply(options);
	}
}
