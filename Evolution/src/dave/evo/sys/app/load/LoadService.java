package dave.evo.sys.app.load;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import dave.evo.common.Goal;
import dave.evo.common.Library;
import dave.evo.common.Policy;
import dave.evo.eval.counter.CountingOptions;
import dave.evo.eval.life.LifeOptions;
import dave.evo.eval.maze.MazeOptions;
import dave.evo.eval.robot.SimulationOptions;
import dave.evo.eval.seq.SequenceOptions;
import dave.evo.sys.app.modular.ConcurrencyModule;
import dave.evo.sys.app.modular.ESHyperNEATBuildModule;
import dave.evo.sys.app.modular.FlatModule;
import dave.evo.sys.app.modular.GRUMBModule;
import dave.evo.sys.app.modular.LSTMModule;
import dave.evo.sys.app.modular.MemoryModule;
import dave.evo.sys.app.modular.Module;
import dave.evo.sys.app.script.CountingGoal;
import dave.evo.sys.app.script.LifeGoal;
import dave.evo.sys.app.script.LimitedPolicy;
import dave.evo.sys.app.script.LoggingPolicy;
import dave.evo.sys.app.script.PerfectionPolicy;
import dave.evo.sys.app.script.RobotSimulationGoal;
import dave.evo.sys.app.script.SequencerGoal;
import dave.evo.sys.app.script.SimpleMazeGoal;
import dave.evo.sys.app.script.TicTacToeGoal;
import dave.evo.sys.app.script.XorGoal;
import dave.json.JsonNumber;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.util.Identifiable;

public final class LoadService
{
	public static LoadService INSTANCE = new LoadService();
	
	private final Map<String, ModuleLoader> mModules;
	private final Map<String, PolicyLoader> mPolicies;
	private final Map<String, MemoryLoader> mMemories;
	private final Map<String, GoalLoader> mGoals;
	
	private LoadService()
	{
		mModules = new HashMap<>();
		mPolicies = new HashMap<>();
		mMemories = new HashMap<>();
		mGoals = new HashMap<>();
	}
	
	private <T extends Identifiable> void register(Map<String, T> m, T v)
	{
		if(m.put(v.getID(), v) != null)
			throw new IllegalArgumentException("Duplicate loader " + v.getID());
	}
	
	public void register(ModuleLoader l)
	{
		register(mModules, l);
	}
	
	public void register(PolicyLoader l)
	{
		register(mPolicies, l);
	}
	
	public void register(MemoryLoader l)
	{
		register(mMemories, l);
	}
	
	public void register(GoalLoader l)
	{
		register(mGoals, l);
	}
	
	public Module loadModule(String id, JsonValue json)
	{
		ModuleLoader l = mModules.get(id);
		
		if(l == null)
			throw new IllegalArgumentException("No such loader: " + id);
		
		return l.load(json);
	}

	public Policy loadPolicy(String id, JsonValue json) { return loadPolicy(id, null, json); }
	public Policy loadPolicy(String id, Policy s, JsonValue json)
	{
		PolicyLoader l = mPolicies.get(id);
		
		if(l == null)
			throw new IllegalArgumentException("No such loader: " + id);
		
		return l.load(s, json);
	}
	
	public MemoryModule loadMemory(String id, JsonValue json)
	{
		MemoryLoader l = mMemories.get(id);
		
		if(l == null)
			throw new IllegalArgumentException("No such loader: " + id);
		
		return l.load(json);
	}
	
	public Goal loadGoal(String id, JsonValue json)
	{
		GoalLoader l = mGoals.get(id);
		
		if(l == null)
			throw new IllegalArgumentException("No such loader: " + id);
		
		return l.load(json);
	}
	
	public List<Module> loadModules(JsonValue json)
	{
		return ((JsonObject) json).stream()
			.map(e -> loadModule(e.getKey(), e.getValue()))
			.collect(Collectors.toList());
	}
	
	static
	{
		INSTANCE.register(new SimpleModuleLoader(Library.Modules.CONCURRENCY, json -> new ConcurrencyModule()));
		INSTANCE.register(new SimpleModuleLoader(Library.Modules.ESHYPERNEAT, json -> new ESHyperNEATBuildModule()));
		INSTANCE.register(new SimpleModuleLoader(Library.Modules.FLAT, json -> new FlatModule()));
		INSTANCE.register(new MemoryModuleLoader());
		INSTANCE.register(new ControlModuleLoader());
		INSTANCE.register(new GoalModuleLoader());
		
		INSTANCE.register(new SimplePolicyLoader(Library.Policies.PERFECTION, true, (p, json) -> new PerfectionPolicy()));
		INSTANCE.register(new SimplePolicyLoader(Library.Policies.LOGGING, false, (p, json) -> new LoggingPolicy(p)));
		INSTANCE.register(new SimplePolicyLoader(Library.Policies.LIMITED, true, (p, json) -> {
			JsonObject o = (JsonObject) json;
			int tries = o.getInt("max-tries");
			double f = o.getDouble("max-fitness");
			
			return new LimitedPolicy(tries, f);
		}));
		
		INSTANCE.register(new SimpleMemoryLoader(Library.Memories.LSTM, json -> new LSTMModule(((JsonNumber) json).getInt())));
		INSTANCE.register(new SimpleMemoryLoader(Library.Memories.GRUMB, json -> new GRUMBModule(((JsonNumber) json).getInt())));
		
		INSTANCE.register(new SimpleGoalLoader(Library.Goals.XOR, json -> new XorGoal()));
		INSTANCE.register(new SimpleGoalLoader(Library.Goals.TICTACTOE, json -> new TicTacToeGoal()));
		INSTANCE.register(new SimpleParametrizedGoalLoader<>(Library.Goals.COUNTING, CountingOptions.class, o -> new CountingGoal(o)));
		INSTANCE.register(new SimpleParametrizedGoalLoader<>(Library.Goals.ROBOT, SimulationOptions.class, o -> new RobotSimulationGoal(o)));
		INSTANCE.register(new SimpleParametrizedGoalLoader<>(Library.Goals.MAZE, MazeOptions.class, o -> new SimpleMazeGoal(o)));
		INSTANCE.register(new SimpleParametrizedGoalLoader<>(Library.Goals.LIFE, LifeOptions.class, o -> new LifeGoal(o)));
		INSTANCE.register(new SimpleParametrizedGoalLoader<>(Library.Goals.SEQUENCE, SequenceOptions.class, o -> new SequencerGoal(o)));
	}
}
