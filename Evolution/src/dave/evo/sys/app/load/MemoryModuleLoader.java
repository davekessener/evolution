package dave.evo.sys.app.load;

import dave.evo.common.Library;
import dave.evo.sys.app.modular.Module;
import dave.json.JsonObject;
import dave.json.JsonValue;

public class MemoryModuleLoader implements ModuleLoader
{
	@Override
	public String getID()
	{
		return Library.Modules.MEMORY;
	}

	@Override
	public Module load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		String type = o.getString("type");
		JsonValue capacity = o.get("capacity");
		
		return LoadService.INSTANCE.loadMemory(type, capacity);
	}
}
