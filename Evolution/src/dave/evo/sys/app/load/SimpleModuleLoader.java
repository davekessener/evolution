package dave.evo.sys.app.load;

import java.util.function.Function;

import dave.evo.sys.app.modular.Module;
import dave.json.JsonValue;

public class SimpleModuleLoader implements ModuleLoader
{
	private final String mID;
	private final Function<JsonValue, Module> mCallback;
	
	public SimpleModuleLoader(String id, Function<JsonValue, Module> f)
	{
		mID = id;
		mCallback = f;
	}

	@Override
	public String getID()
	{
		return mID;
	}

	@Override
	public Module load(JsonValue json)
	{
		return mCallback.apply(json);
	}
}
