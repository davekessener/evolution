package dave.evo.sys.app.load;

import dave.evo.common.Goal;
import dave.json.JsonValue;
import dave.util.Identifiable;

public interface GoalLoader extends Identifiable
{
	public abstract Goal load(JsonValue json);
}
