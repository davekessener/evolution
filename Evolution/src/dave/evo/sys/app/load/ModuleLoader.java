package dave.evo.sys.app.load;

import dave.evo.sys.app.modular.Module;
import dave.json.JsonValue;
import dave.util.Identifiable;

public interface ModuleLoader extends Identifiable
{
	public abstract Module load(JsonValue json);
}
