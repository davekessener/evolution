package dave.evo.sys.app.load;

import dave.evo.common.Goal;
import dave.json.JsonValue;
import dave.util.config.Configuration;

public abstract class ParametrizedGoalLoader<T> implements GoalLoader
{
	private final String mID;
	private final Class<T> mBase;
	
	protected ParametrizedGoalLoader(String id, Class<T> c)
	{
		mID = id;
		mBase = c;
	}
	
	protected abstract Goal create(T options);
	
	@Override
	public String getID()
	{
		return mID;
	}

	@Override
	public Goal load(JsonValue json)
	{
		Configuration<T> config = new Configuration<>(mBase);
		
		if(json != null)
		{
			config.load(json);
		}
		
		return create(config.proxy());
	}
}
