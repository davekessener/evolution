package dave.evo.sys.app.load;

import dave.evo.sys.app.modular.MemoryModule;
import dave.json.JsonValue;
import dave.util.Identifiable;

public interface MemoryLoader extends Identifiable
{
	public abstract MemoryModule load(JsonValue json);
}
