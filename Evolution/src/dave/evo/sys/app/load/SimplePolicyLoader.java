package dave.evo.sys.app.load;

import java.util.function.BiFunction;

import dave.evo.common.Policy;
import dave.json.JsonValue;

public class SimplePolicyLoader implements PolicyLoader
{
	private final String mID;
	private final BiFunction<Policy, JsonValue, Policy> mCallback;
	private final boolean mFirst;
	
	public SimplePolicyLoader(String id, boolean first, BiFunction<Policy, JsonValue, Policy> f)
	{
		mID = id;
		mCallback = f;
		mFirst = first;
	}

	@Override
	public String getID()
	{
		return mID;
	}

	@Override
	public Policy load(Policy s, JsonValue json)
	{
		if(mFirst && s != null)
			throw new IllegalArgumentException(mID + " must be first!");
		
		return mCallback.apply(s, json);
	}
}
