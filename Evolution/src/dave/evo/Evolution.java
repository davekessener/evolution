package dave.evo;

import java.io.File;
import java.io.IOException;
import java.util.List;

import dave.evo.common.Library;
import dave.evo.common.Options;
import dave.arguments.Arguments;
import dave.arguments.Option;
import dave.arguments.ParseException;
import dave.arguments.Parser;
import dave.evo.sys.app.App;
import dave.evo.sys.app.load.LoadService;
import dave.evo.sys.app.modular.Module;
import dave.evo.sys.bus.UserAgent;
import dave.json.JsonUtils;
import dave.util.RNG;
import dave.util.Seed;
import dave.util.ShutdownService;
import dave.util.ShutdownService.Priority;
import dave.util.XoRoRNG;
import dave.util.config.Configuration;
import dave.util.log.LogBase;
import dave.util.log.LogLevelFilter;
import dave.util.log.LogSink;
import dave.util.log.Logger;
import dave.util.log.Severity;
import dave.util.log.Stdout;
import dave.util.log.service.LogService;
import dave.util.property.BindableProperty;
import dave.util.property.SimpleProperty;

public final class Evolution
{
	private static final LogLevelFilter sLogFilter = new LogLevelFilter();
	
	public static void main(String[] args)
	{
		LogBase.INSTANCE.registerSink(sLogFilter, LogSink.build());
		LogBase.INSTANCE.start();
		
		ShutdownService.INSTANCE.register(Priority.LAST, LogBase.INSTANCE::stop);
		
		try
		{
			Logger.DEFAULT.log(Severity.INFO, "%s v%d - [%s]", Library.TITLE, Library.VERSION, Library.DEBUG ? "DEBUG" : "RELEASE");
			
			LogService.INSTANCE.initialize(Library.DEBUG);
			
			run(parseArguments(args));
		}
		catch(Throwable e)
		{
			e.printStackTrace();
			
			Logger.DEFAULT.fatal("%s", e.getMessage());
		}
		finally
		{
			ShutdownService.INSTANCE.shutdown();
		}
		
		Stdout.println("Goodbye.");
		
		System.exit(0);
	}
	
	private static void run(Arguments args) throws IOException
	{
		if(args.hasArgument(OPTION_VERBOSE))
		{
			sLogFilter.setThreshold(Severity.NONE);
		}
		
		Seed seed = (args.hasArgument(OPTION_SEED) ? new Seed(args.getArgument(OPTION_SEED)) : new Seed());
		
		Logger.DEFAULT.info("Seed: %s", seed);
		
		run(args.getArgument(OPTION_CONFIG), args.getArgument(OPTION_SCRIPT), seed.toString());
	}
	
	public static Configuration<Options> loadConfig(String cf) throws IOException
	{
		File config_file = new File(cf);
		Configuration<Options> config = new Configuration<>(Options.class);
		
		if(config_file.exists())
		{
			config.load(JsonUtils.fromFile(config_file));
		}
		
		return config;
	}
	
	public static List<Module> loadScript(String sf) throws IOException
	{
		File script_file = new File(sf);
		
		return LoadService.INSTANCE.loadModules(JsonUtils.fromFile(script_file));
	}
	
	public static void run(String cf, String sf, String s) throws IOException
	{
		Seed seed = new Seed(s);
		
		run(loadConfig(cf), loadScript(sf), seed);
	}
	
	public static void run(Configuration<Options> config, List<Module> script, Seed seed)
	{
		RNG rng = new XoRoRNG(seed);
		BindableProperty<Boolean> running = new BindableProperty<>(new SimpleProperty<>(true));
		
		App app = (new App.Builder(config, rng))
				.addAgent(bus -> new UserAgent(bus, running))
				.installAll(script)
				.build();

		running.bind(app.runningProperty());

		app.run();
	}
	
	private static Arguments parseArguments(String[] args)
	{
		
		Parser p = new Parser(OPTION_CONFIG, OPTION_SCRIPT, OPTION_VERBOSE, OPTION_SEED);
		
		try
		{
			return p.parse(args);
		}
		catch (ParseException e)
		{
			Logger.DEFAULT.error("usage: %s", p.toString());
			
			throw new RuntimeException("Invalid arguments! (" + e.getMessage() + ")");
		}
	}
	
	private static final Option OPTION_CONFIG = (new Option.OptionBuilder("config")).setShortcut("c").setDefault(Library.CONFIG).build();
	private static final Option OPTION_SCRIPT = (new Option.OptionBuilder("script")).setShortcut("s").setDefault(Library.SCRIPT).build();
	private static final Option OPTION_VERBOSE = (new Option.OptionBuilder("verbose")).setShortcut("v").build();
	private static final Option OPTION_SEED = (new Option.OptionBuilder("seed")).hasValue(true).build();
	
	private Evolution( ) { }
}
