package dave.evo.util;

import java.util.stream.IntStream;
import java.util.stream.Stream;

import dave.evo.nn.hyper.HypercubePainter;
import dave.util.image.Canvas;
import dave.util.image.Color;
import dave.util.image.SimpleGraphics;

public class HypercubeVisualizer
{
	private final double mX0, mY0, mZ0, mW0;
	private final double mX1, mY1, mZ1, mW1;
	private final int mRX, mRY, mRZ, mRW;
	
	private HypercubeVisualizer(
		double x0, double x1, int rx,
		double y0, double y1, int ry,
		double z0, double z1, int rz,
		double w0, double w1, int rw)
	{
		mX0 = x0;
		mX1 = x1;
		mRX = rx;
		
		mY0 = y0;
		mY1 = y1;
		mRY = ry;
		
		mZ0 = z0;
		mZ1 = z1;
		mRZ = rz;
		
		mW0 = w0;
		mW1 = w1;
		mRW = rw;
	}
	
	public Stream<Canvas> process(HypercubePainter f)
	{
		return IntStream.range(0, mRY)
			.mapToObj(iy -> IntStream.range(0, mRX)
				.mapToObj(ix -> generateImage(f, x(ix), y(iy))))
			.flatMap(e -> e);
	}
	
	public Canvas generateImage(HypercubePainter f, double x, double y)
	{
		Canvas canvas = Canvas.build(mRZ, mRW);
		SimpleGraphics gc = new SimpleGraphics(canvas);
		
		for(int iw = 0 ; iw < mRW ; ++iw)
		{
			for(int iz = 0 ; iz < mRZ ; ++iz)
			{
				double v = 0.5 * (1 + f.at(x, y, z(iz), w(iw)));
				
				if(v < 0 || v > 1)
					throw new IllegalStateException("" + v);
				
				gc.drawPixel(iz, iw, Color.monochrome(v));
			}
		}
		
		return canvas;
	}
	
	private double x(int ix) { return convert(ix, mRX, mX0, mX1); }
	private double y(int iy) { return convert(iy, mRY, mY0, mY1); }
	private double z(int iz) { return convert(iz, mRZ, mZ0, mZ1); }
	private double w(int iw) { return convert(iw, mRW, mW0, mW1); }
	
	private static double convert(int i, int r, double x0, double x1) { return x0 + (i / (r - 1.0)) * (x1 - x0); }
	
	public static HypercubeVisualizer build()
	{
		return (new Builder()).build();
	}
	
	public static class Builder
	{
		private double x0, x1;
		private double y0, y1;
		private double z0, z1;
		private double w0, w1;
		private int rx, ry, rz, rw;
		
		public Builder()
		{
			x0 = y0 = z0 = w0 = -1;
			x1 = y1 = z1 = w1 =  1;
			rx = ry = rz = rw = 100;
		}
		
		public Builder setStartX(double x0) { this.x0 = x0; return this; }
		public Builder setEndX(double x1) { this.x1 = x1; return this; }
		public Builder setResolutionX(int rx) { this.rx = rx; return this; }
		
		public Builder setStartY(double y0) { this.y0 = y0; return this; }
		public Builder setEndY(double y1) { this.y1 = y1; return this; }
		public Builder setResolutionY(int ry) { this.ry = ry; return this; }
		
		public Builder setStartZ(double z0) { this.z0 = z0; return this; }
		public Builder setEndZ(double z1) { this.z1 = z1; return this; }
		public Builder setResolutionZ(int rz) { this.rz = rz; return this; }
		
		public Builder setStartW(double w0) { this.w0 = w0; return this; }
		public Builder setEndW(double w1) { this.w1 = w1; return this; }
		public Builder setResolutionW(int rw) { this.rw = rw; return this; }
		
		public HypercubeVisualizer build()
		{
			return new HypercubeVisualizer(x0, x1, rx, y0, y1, ry, z0, z1, rz, w0, w1, rw);
		}
	}
}
