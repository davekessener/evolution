package dave.evo.util;

import java.util.HashMap;
import java.util.Map;

import dave.evo.common.Message;

public class Backlog<T>
{
	private final Map<Long, Entry<T>> mBacklog;
	private long mNextID;
	
	public Backlog()
	{
		mBacklog = new HashMap<>();
		mNextID = 1;
	}
	
	public Entry<T> create(Message source, T content)
	{
		Entry<T> e = new Entry<>(mNextID++, source, content);
		
		mBacklog.put(e.id, e);
		
		return e;
	}
	
	public Entry<T> get(long id)
	{
		Entry<T> e = mBacklog.get(id);
		
		if(e == null)
			throw new IllegalArgumentException("No such entry: " + id);
		
		return e;
	}
	
	public void remove(long id)
	{
		if(mBacklog.remove(id) == null)
			throw new IllegalArgumentException("No such entry: " + id);
	}
	
	public static class Entry<T>
	{
		public final long id;
		public final Message source;
		public final T content;
		
		public Entry(long id, Message source, T content)
		{
			this.id = id;
			this.source = source;
			this.content = content;
		}
	}
}
