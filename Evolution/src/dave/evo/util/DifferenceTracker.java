package dave.evo.util;

import java.util.HashMap;
import java.util.Map;

import dave.util.Averager;

public class DifferenceTracker
{
	private final Map<String, Averager> mContent;
	
	public DifferenceTracker()
	{
		mContent = new HashMap<>();
	}
	
	public Map<String, Averager> raw()
	{
		return mContent;
	}
	
	public <T> void add(T k, double v)
	{
		String id = k.toString().toLowerCase();
		Averager a = mContent.get(id);
		
		if(a == null)
		{
			mContent.put(id, a = new Averager());
		}
		
		a.add(v);
	}
	
	public <T> double get(T k)
	{
		String id = k.toString().toLowerCase();
		Averager a = mContent.get(id);
		
		return a == null ? 0 : a.get();
	}
}
