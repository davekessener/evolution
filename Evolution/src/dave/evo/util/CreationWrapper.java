package dave.evo.util;

import java.util.function.BiFunction;
import java.util.function.Function;

public class CreationWrapper<K, T>
{
	public final Function<K, T> creator;
	public final Function<T, T> mutator;
	public final BiFunction<T, T, T> breeder;
	
	public CreationWrapper(Function<K, T> f1, Function<T, T> f2, BiFunction<T, T, T> f3)
	{
		this.creator = f1;
		this.mutator = f2;
		this.breeder = f3;
	}
}
