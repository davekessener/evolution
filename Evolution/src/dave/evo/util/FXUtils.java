package dave.evo.util;

import dave.util.image.Canvas;
import dave.util.image.ConvertR8G8B8;
import javafx.scene.image.Image;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.WritableImage;

public final class FXUtils
{
	public static Image toImage(Canvas canvas)
	{
		int w = canvas.width(), h = canvas.height();
		WritableImage img = new WritableImage(w, h);
		byte[] data = (new ConvertR8G8B8()).convert(canvas);

		img.getPixelWriter().setPixels(0, 0, w, h, PixelFormat.getByteRgbInstance(), data, 0, w * 3);
		
		return img;
	}
	
	private FXUtils( ) { }
}
