package dave.evo.util;

public final class EvoUtils
{
	public static double[] join(double[] ... a)
	{
		int l = 0;
		
		for(int i = 0 ; i < a.length ; ++i)
		{
			l += a[i].length;
		}
		
		double[] r = new double[l];
		int o = 0;
		
		for(int i = 0 ; i < a.length ; ++i)
		{
			for(int j = 0 ; j < a[i].length ; ++j)
			{
				r[o + j] = a[i][j];
			}
			
			o += a[i].length;
		}
		
		return r;
	}
	
	public static void move(double[] to, double[] from)
	{
		if(to.length != from.length)
			throw new IllegalArgumentException();
		
		for(int i = 0 ; i < to.length ; ++i)
		{
			to[i] = from[i];
		}
	}
	
	private EvoUtils( ) { }
}
