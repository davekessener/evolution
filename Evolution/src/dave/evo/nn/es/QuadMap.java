package dave.evo.nn.es;

import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Stream;

import dave.evo.config.SubstrateOptions;
import dave.evo.nn.hyper.HypercubePainter;
import dave.util.Variancer;
import dave.util.math.Vec2;

public class QuadMap
{
	private final double mX, mY;
	private final boolean mOut;
	private final HypercubePainter mCPPNN;
	private final SubstrateOptions mOptions;
	private final Point mRoot;
	
	public QuadMap(double x, double y, boolean out, HypercubePainter f, SubstrateOptions o)
	{
		mX = x;
		mY = y;
		mOut = out;
		mCPPNN = f;
		mOptions = o;
		
		mRoot = new Point();
		
		buildTree();
	}
	
	public Point root() { return mRoot; }
	
	private void buildTree()
	{
		Queue<Point> open = new ArrayDeque<>();
		int min = mOptions.min_division();
		int max = mOptions.max_division();
		double variance = mOptions.division_variance();
		
		open.add(mRoot);
		
		while(!open.isEmpty())
		{
			Point p = open.poll();
			
			p.split();
			
			if(p.level() < min || (p.level() <= max && p.simpleVariance() >= variance))
			{
				p.children().forEach(open::add);
			}
		}
	}
	
	public void cull()
	{
		cull(mRoot);
	}
	
	private void cull(Point p)
	{
		if(p.leaf())
			return;
		
		if(p.variance() < mOptions.prune_variance())
		{
			p.clear();
		}
		else
		{
			p.children().forEach(this::cull);
		}
	}
	
	public Set<Link> prune()
	{
		Set<Link> r = new HashSet<>();
		
		prune(mRoot, r);
		
		return r;
	}
	
	private void prune(Point p, Set<Link> links)
	{
		if(p.leaf())
			return;
		
		p.children().forEach(c -> {
			if(c.variance() >= mOptions.prune_variance())
			{
				prune(c, links);
			}
			else
			{
				double d = p.radius();

				double dl = Math.abs(c.value() - valueAt(c.x() - d, c.y()));
				double dr = Math.abs(c.value() - valueAt(c.x() + d, c.y()));
				double dd = Math.abs(c.value() - valueAt(c.x(), c.y() - d));
				double du = Math.abs(c.value() - valueAt(c.x(), c.y() + d));
				
				double band = Math.max(Math.min(dl, dr), Math.min(du, dd));
				
				if(band > mOptions.banding_threshold())
				{
					links.add(linkWith(c.x(), c.y()));
				}
			}
		});
	}
	
	private double valueAt(double x, double y)
	{
		return (mOut ? mCPPNN.at(mX, mY, x, y) : mCPPNN.at(x, y, mX, mY));
	}
	
	private Link linkWith(double x, double y)
	{
		return (mOut ? new Link(mX, mY, x, y) : new Link(x, y, mX, mY));
	}
	
	public static Set<Link> connectionsOf(Vec2 p, boolean out, HypercubePainter f, SubstrateOptions o) { return connectionsOf(p.x, p.y, out, f, o); }
	public static Set<Link> connectionsOf(double x, double y, boolean out, HypercubePainter f, SubstrateOptions o)
	{
		QuadMap self = new QuadMap(x, y, out, f, o);
		Set<Link> r = self.prune();
		
		r.removeIf(l -> l.length() <= o.minimum_distance());
		
		return r;
	}
	
	public class Point
	{
		private final double mX, mY;
		private final double mRadius;
		private final int mLevel;
		private final double mValue;
		private final Point[] mChildren;
		private double mVariance;
		
		private Point() { this(0, 0, 1, 1); }
		private Point(double x, double y, double r, int l)
		{
			mX = x;
			mY = y;
			mRadius = r;
			mLevel = l;
			
			mValue = valueAt(x, y);
			mChildren = new Point[4];
			mVariance = -1;
			
			if(mValue < -1 || mValue > 1)
				throw new IllegalStateException("" + mValue);
		}
		
		public double x() { return mX; }
		public double y() { return mY; }
		public double radius() { return mRadius; }
		public double value() { return mValue; }
		public int level() { return mLevel; }
		public boolean leaf() { return mChildren[0] == null; }
		
		public Stream<Point> children()
		{
			if(leaf())
				throw new IllegalStateException();
			
			return Stream.of(mChildren);
		}
		
		public void split()
		{
			if(!leaf())
				throw new IllegalStateException();
			
			if(mVariance != -1)
				throw new IllegalStateException();
			
			double r = mRadius / 2;
			int l = mLevel + 1;
			
			mChildren[0] = new Point(mX - r, mY - r, r, l);
			mChildren[1] = new Point(mX - r, mY + r, r, l);
			mChildren[2] = new Point(mX + r, mY - r, r, l);
			mChildren[3] = new Point(mX + r, mY + r, r, l);
		}
		
		public double simpleVariance()
		{
			Variancer v = new Variancer();

			v.add(mChildren[0].value());
			v.add(mChildren[1].value());
			v.add(mChildren[2].value());
			v.add(mChildren[3].value());
			
			return v.get();
		}
		
		public double variance()
		{
			if(leaf())
				mVariance = 0;
			
			if(mVariance == -1)
			{
				Variancer v = new Variancer();
				
				calculateVariance(v);
				
				mVariance = v.get();
			}
			
			return mVariance;
		}
		
		private void calculateVariance(Variancer v)
		{
			if(leaf())
			{
				v.add(mValue);
			}
			else
			{
				children().forEach(c -> c.calculateVariance(v));
			}
		}
		
		public void clear()
		{
			if(leaf())
				throw new IllegalStateException();
			
			for(int i = 0 ; i < 4 ; ++i)
			{
				mChildren[i] = null;
			}
			
			mVariance = 0;
		}
	}
}
