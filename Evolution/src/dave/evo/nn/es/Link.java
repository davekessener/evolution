package dave.evo.nn.es;

import dave.util.math.Vec2;

public class Link
{
	public final double x0, y0;
	public final double x1, y1;
	
	public Link(double x0, double y0, double x1, double y1)
	{
		this.x0 = x0;
		this.y0 = y0;
		this.x1 = x1;
		this.y1 = y1;
	}
	
	public Vec2 from() { return new Vec2(x0, y0); }
	public Vec2 to() { return new Vec2(x1, y1); }
	
	public double length()
	{
		return from().sub(to()).length();
	}
	
	@Override
	public int hashCode()
	{
		return (3 * Double.hashCode(x0)) ^ (7 * Double.hashCode(y0)) ^ (13 * Double.hashCode(x1)) ^ (17 * Double.hashCode(y1));
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Link)
		{
			Link l = (Link) o;
			
			return x0 == l.x0 && y0 == l.y0 && x1 == l.x1 && y1 == l.y1;
		}
		
		return false;
	}
}
