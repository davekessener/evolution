package dave.evo.nn.es;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

import dave.evo.common.Options;
import dave.evo.common.NetworkInterface;
import dave.evo.nn.hyper.HypercubePainter;
import dave.evo.nn.hyper.Substrate;
import dave.util.math.Vec2;

public class SubstrateGenerator implements Function<HypercubePainter, Substrate>
{
	private final NetworkInterface mInterface;
	private final Options mOptions;
	
	public SubstrateGenerator(NetworkInterface i, Options o)
	{
		mInterface = i;
		mOptions = o;
	}

	@Override
	public Substrate apply(HypercubePainter f)
	{
		Substrate.Builder b = new Substrate.Builder(mInterface.bias.x, mInterface.bias.y);
		
		Map<Vec2, Substrate.Node> nodes = new HashMap<>();
		
		mInterface.inputs.forEach(v -> nodes.put(v, b.addInput(v.x, v.y)));
		mInterface.outputs.forEach(v -> nodes.put(v, b.addOutput(v.x, v.y, mOptions.base_activation())));
		
		Set<Vec2> ineligable = new HashSet<>();
		Set<Vec2> hidden = new HashSet<>();
		Queue<Vec2> unvisited = new ArrayDeque<>();
		Set<Con> connections = new HashSet<>();
		
		ineligable.add(mInterface.bias);
		mInterface.inputs.forEach(ineligable::add);
		
		Consumer<Link> add_hidden = l -> {
			if(!ineligable.contains(l.to()))
			{
				if(hidden.add(l.to()))
				{
					unvisited.add(l.to());
				}
				
				connections.add(new Con(l));
			}
		};
		
		mInterface.inputs.forEach(v -> QuadMap.connectionsOf(v, true, f, mOptions)
			.forEach(add_hidden));
		
		for(int i = 0 ; i < mOptions.recursion_depth() ; ++i)
		{
			if(unvisited.isEmpty())
				break;
			
			for(int j = unvisited.size() ; j > 0 ; --j)
			{
				QuadMap.connectionsOf(unvisited.poll(), true, f, mOptions).forEach(add_hidden);
			}
		}
		
		mInterface.outputs.forEach(v -> QuadMap.connectionsOf(v, false, f, mOptions)
			.forEach(l -> connections.add(new Con(l))));

		mInterface.inputs.forEach(v -> markFrom(v, connections));
		mInterface.outputs.forEach(v -> markTo(v, connections));
		
		Function<Vec2, Substrate.Node> lookup = v -> {
			Substrate.Node n = nodes.get(v);
			
			if(n == null)
			{
				nodes.put(v, n = b.addHidden(v.x, v.y, mOptions.base_activation()));
			}
			
			return n;
		};
		
		mInterface.outputs.stream()
			.forEach(v -> {
				Substrate.Node o = nodes.get(v);
				
				b.link(b.bias(), o, 0);
			});
		
		connections.stream()
			.filter(c -> c.from && c.to)
			.map(c -> c.link)
			.forEach(l -> b.link(lookup.apply(l.from()), lookup.apply(l.to())));
		
		return b.build();
	}
	
	private static void markFrom(Vec2 p, Set<Con> cons)
	{
		Queue<Vec2> open = new ArrayDeque<>();
		
		open.add(p);

		while(!open.isEmpty())
		{
			Vec2 v = open.poll();
			
			cons.stream()
				.filter(c -> !c.from)
				.filter(c -> c.link.from().equals(v))
				.forEach(c -> {
					c.from = true;
					
					open.add(c.link.to());
				});
		}
	}
	
	private static void markTo(Vec2 p, Set<Con> cons)
	{
		Queue<Vec2> open = new ArrayDeque<>();
		
		open.add(p);

		while(!open.isEmpty())
		{
			Vec2 v = open.poll();
			
			cons.stream()
				.filter(c -> !c.to)
				.filter(c -> c.link.to().equals(v))
				.forEach(c -> {
					c.to = true;
					
					open.add(c.link.from());
				});
		}
	}
	
	private static class Con
	{
		public final Link link;
		public boolean from, to;
		
		public Con(Link l)
		{
			this.link = l;
			this.from = this.to = false;
		}
		
		@Override
		public int hashCode()
		{
			return link.hashCode();
		}
		
		@Override
		public boolean equals(Object o)
		{
			if(o instanceof Con)
			{
				return link.equals(((Con) o).link);
			}
			
			return false;
		}
	}
}
