package dave.evo.nn;

public class InputNeuron implements Neuron
{
	private double mValue;
	
	public void set(double v)
	{
		mValue = v;
	}
	
	@Override
	public double get()
	{
		return mValue;
	}
}
