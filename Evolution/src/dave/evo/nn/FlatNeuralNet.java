package dave.evo.nn;

import dave.evo.common.NeuralNet;
import dave.evo.dna.WeightMatrix;
import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;
import dave.util.math.Matrix;

@Container
public class FlatNeuralNet implements NeuralNet, Saveable
{
	private final Matrix mWeights;
	private final Activation mActivation;
	
	public FlatNeuralNet(Matrix m, Activation a)
	{
		mWeights = m;
		mActivation = a;
	}
	
	@Override
	public int inputs()
	{
		return mWeights.width();
	}

	@Override
	public int outputs()
	{
		return mWeights.height();
	}

	@Override
	public double[] apply(double[] in)
	{
		if(in.length != inputs())
			throw new IllegalArgumentException();
		
		Matrix m = mWeights.mul(new Matrix(1, in.length, in));
		double[] r = new double[outputs()];
		
		for(int i = 0 ; i < r.length ; ++i)
		{
			r[i] = mActivation.applyAsOutput(m.get(0, i));
		}
		
		return r;
	}
	
	@Override
	@Saver
	public JsonValue save()
	{
		JsonObject json = new JsonObject();
		
		json.putString("activation", mActivation.toString());
		json.put("weights", mWeights.save());
		
		return json;
	}
	
	@Loader
	public static FlatNeuralNet load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		Activation a = Activation.get(Activation.Type.valueOf(o.getString("activation")));
		Matrix w = Matrix.load(o.get("weights"));
		
		return new FlatNeuralNet(w, a);
	}
	
	public static FlatNeuralNet build(WeightMatrix wm)
	{
		return new FlatNeuralNet(wm.matrix(), Activation.get(Activation.Type.valueOf(wm.activation())));
	}
}
