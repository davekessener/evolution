package dave.evo.nn;

import java.util.ArrayList;
import java.util.List;

import dave.evo.common.Library;
import dave.evo.common.NeuralNet;
import dave.evo.dna.GeneSequence;
import dave.evo.dna.LinkGene;
import dave.evo.dna.NeuronGene;
import dave.evo.nn.detail.BasicNeuralNetBuilder;
import dave.evo.nn.detail.BasicNeuralNetSerializer;
import dave.json.Container;
import dave.json.JSON;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.PrettyPrinter;
import dave.json.Saveable;
import dave.json.Saver;
import dave.util.log.Logger;
import dave.util.stream.StreamUtils;

@Container
public class BasicNeuralNet implements NeuralNet, Saveable
{
	private final List<InputNeuron> mInputs;
	private final List<OutputNeuron> mOutputs;
	
	public BasicNeuralNet(List<InputNeuron> in, List<OutputNeuron> out)
	{
		mInputs = in;
		mOutputs = out;
	}
	
	@Override
	public int inputs()
	{
		return mInputs.size();
	}
	
	@Override
	public int outputs()
	{
		return mOutputs.size();
	}
	
	@Override
	public double[] apply(double[] in)
	{
		if(in.length != mInputs.size())
			throw new IllegalArgumentException("Expected " + mInputs.size() + ", got " + in.length);
		
		for(int i = 0 ; i < in.length ; ++i)
		{
			mInputs.get(i).set(in[i]);
		}
		
		mOutputs.forEach(OutputNeuron::reset);
		
		double[] out = new double[mOutputs.size()];
		
		for(int i = 0 ; i < mOutputs.size() ; ++i)
		{
			out[i] = mOutputs.get(i).get();
		}
		
		return out;
		
//		return mOutputs.stream().mapToDouble(OutputNeuron::get).toArray();
	}
	
	@Override
	@Saver
	public JsonValue save()
	{
		return BasicNeuralNetSerializer.save(mInputs, mOutputs);
	}
	
	@Loader
	public static BasicNeuralNet load(JsonValue json)
	{
		return BasicNeuralNetSerializer.load(json);
	}
	
	public static BasicNeuralNet build(GeneSequence genome)
	{
		BasicNeuralNetBuilder b = new BasicNeuralNetBuilder();
		List<NeuronGene> neurons = new ArrayList<>();
		List<LinkGene> links = new ArrayList<>();
		
		genome.genes()
			.forEach(g -> {
				if(g instanceof NeuronGene)
				{
					neurons.add((NeuronGene) g);
				}
				else if(g instanceof LinkGene)
				{
					links.add((LinkGene) g);
				}
			});
		
		neurons.forEach(b::add);
		links.forEach(b::add);
		
		if(Library.DEBUG)
		{
			NeuralNet nn = b.build();
			
			try
			{
				nn.apply(new double[nn.inputs()]);
			}
			catch(Throwable e)
			{
				Logger.DEFAULT.error("%s failed! (%d + %d = %d)", genome, neurons.size(), links.size(), genome.size());
				genome.genes().map(StreamUtils.stream_with_index()).forEach(p -> Logger.DEFAULT.error("[%03d] %s", p.index, p.value));
				Logger.DEFAULT.error("%s", JSON.serialize(nn).toString(new PrettyPrinter()));
				throw e;
			}
		}
		
		return b.build();
	}
}
