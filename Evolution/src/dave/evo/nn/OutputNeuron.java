package dave.evo.nn;

public class OutputNeuron extends ConnectedNeuron
{
	public OutputNeuron(Activation a)
	{
		super(a);
	}
	
	@Override
	public double get()
	{
		double min = activation().min();
		double max = activation().max();
		double v = super.get();
		
		return (v - min) / (max - min);
	}
}
