package dave.evo.nn;

import dave.evo.common.NeuralNet;
import dave.evo.util.EvoUtils;
import dave.json.Container;
import dave.json.JSON;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class LSTM implements NeuralNet, Saveable
{
	private final NeuralNet mInputGate, mOutputGate, mForgetGate;
	private final NeuralNet mCell, mOutput;
	private final double[] mHidden, mMemory;
	private final int mInputs, mOutputs;
	
	public LSTM(int size, NeuralNet g_in, NeuralNet g_out, NeuralNet forget, NeuralNet cell, NeuralNet out)
	{
		if(g_in.inputs() != g_out.inputs() || g_in.inputs() != forget.inputs() || g_in.inputs() != cell.inputs())
			throw new IllegalArgumentException();

		if(g_in.outputs() != g_out.outputs() || g_in.outputs() != forget.outputs() || g_in.outputs() != cell.outputs())
			throw new IllegalArgumentException();
		
		mInputs = g_in.inputs() - size;
		mOutputs = out.outputs();
		
		if(mInputs <= 0)
			throw new IllegalArgumentException();
		
		if(out.inputs() != cell.outputs())
			throw new IllegalArgumentException();
		
		if(cell.outputs() != size)
			throw new IllegalArgumentException();
		
		mInputGate = g_in;
		mOutputGate = g_out;
		mForgetGate = forget;
		mCell = cell;
		mOutput = out;
		
		mHidden = new double[size];
		mMemory = new double[size];
	}
	
	@Override
	public int inputs()
	{
		return mInputs;
	}

	@Override
	public int outputs()
	{
		return mOutputs;
	}

	private static double to_tanh(double v) { return 2 * v - 1; }
	
	@Override
	public double[] apply(double[] in)
	{
		if(in.length != mInputs)
			throw new IllegalArgumentException(String.format("Expected input sized %d, not %d!", mInputs, in.length));
		
		double[] base = EvoUtils.join(in, mHidden);
		double[] g_in = mInputGate.apply(base);
		double[] g_forget = mForgetGate.apply(base);
		double[] g_out = mOutputGate.apply(base);
		double[] cell = mCell.apply(base);
		
		for(int i = 0 ; i < mMemory.length ; ++i)
		{
			mMemory[i] = g_forget[i] * mMemory[i] + g_in[i] * to_tanh(cell[i]);
		}
		
		for(int i = 0 ; i < mHidden.length ; ++i)
		{
			mHidden[i] = g_out[i] * Activation.TANH.apply(mMemory[i]);
		}
		
		return mOutput.apply(mHidden);
	}
	
	@Override
	public void reset()
	{
		for(int i = 0 ; i < mMemory.length ; ++i)
		{
			mMemory[i] = 0;
			mHidden[i] = 0;
		}
	}
	
	@Override
	@Saver
	public JsonValue save()
	{
		JsonObject json = new JsonObject();
		
		json.put("input_gate", JSON.serialize(mInputGate));
		json.put("output_gate", JSON.serialize(mOutputGate));
		json.put("forget_gate", JSON.serialize(mForgetGate));
		json.put("cell", JSON.serialize(mCell));
		json.put("output", JSON.serialize(mOutput));
		json.putInt("size", mCell.outputs());
		
		return json;
	}
	
	@Loader
	public static LSTM load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;

		NeuralNet input_gate = (NeuralNet) JSON.deserialize(o.get("input_gate"));
		NeuralNet output_gate = (NeuralNet) JSON.deserialize(o.get("output_gate"));
		NeuralNet forget_gate = (NeuralNet) JSON.deserialize(o.get("forget_gate"));
		NeuralNet cell = (NeuralNet) JSON.deserialize(o.get("cell"));
		NeuralNet output = (NeuralNet) JSON.deserialize(o.get("output"));
		int size = o.getInt("size");
		
		return new LSTM(size, input_gate, output_gate, forget_gate, cell, output);
	}
}
