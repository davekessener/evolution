package dave.evo.nn;

import dave.evo.common.NeuralNet;
import dave.evo.util.EvoUtils;
import dave.json.Container;
import dave.json.JSON;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class GRUMB implements NeuralNet, Saveable
{
	private final NeuralNet mReadGate, mWriteGate, mInputGate;
	private final NeuralNet mInput, mOutput;
	private final double[] mMemory, mLastOutput;
	private final int mInputs, mOutputs;
	
	public GRUMB(int size, NeuralNet g_read, NeuralNet g_write, NeuralNet g_in, NeuralNet input, NeuralNet output)
	{
		if(input.inputs() <= size)
			throw new IllegalArgumentException();
		
		mInputs = input.inputs() - size;
		mOutputs = output.outputs();
		
		int total = size + mOutputs + mInputs;
		
		if(g_read.inputs() != total)
			throw new IllegalArgumentException();
		
		if(g_write.inputs() != total)
			throw new IllegalArgumentException();
		
		if(g_in.inputs() != total)
			throw new IllegalArgumentException();
		
		if(g_read.outputs() != size)
			throw new IllegalArgumentException();
		
		if(g_write.outputs() != size)
			throw new IllegalArgumentException();
		
		if(g_in.outputs() != size)
			throw new IllegalArgumentException();
		
		if(input.outputs() != size)
			throw new IllegalArgumentException();
		
		mMemory = new double[size];
		mLastOutput = new double[mOutputs];
		
		mReadGate = g_read;
		mWriteGate = g_write;
		mInputGate = g_in;
		mInput = input;
		mOutput = output;
	}
	
	@Override
	public int inputs()
	{
		return mInputs;
	}

	@Override
	public int outputs()
	{
		return mOutputs;
	}

	@Override
	public double[] apply(double[] in)
	{
		if(in.length != mInputs)
			throw new IllegalArgumentException();
		
		double[] base = EvoUtils.join(in, mMemory, mLastOutput);
		double[] g_in = mInputGate.apply(base);
		double[] g_read = mReadGate.apply(base);
		double[] g_write = mWriteGate.apply(base);
		double[] tmp = mInput.apply(EvoUtils.join(in, mMemory));
		
		for(int i = 0 ; i < mMemory.length ; ++i)
		{
			tmp[i] = to_tanh(tmp[i]) * g_in[i] + mMemory[i] * g_read[i];
		}
		
		for(int i = 0 ; i < mMemory.length ; ++i)
		{
			mMemory[i] += Activation.TANH.apply(tmp[i]) * g_write[i];
		}
		
		double[] r = mOutput.apply(tmp);
		
		EvoUtils.move(mLastOutput, r);
		
		return r;
	}
	
	private static double to_tanh(double v) { return v * 2 - 1; }

	@Override
	@Saver
	public JsonValue save()
	{
		JsonObject json = new JsonObject();

		json.put("read_gate", JSON.serialize(mReadGate));
		json.put("write_gate", JSON.serialize(mWriteGate));
		json.put("input_gate", JSON.serialize(mInputGate));
		json.put("input", JSON.serialize(mInput));
		json.put("output", JSON.serialize(mOutput));
		json.putInt("size", mMemory.length);
		
		return json;
	}
	
	@Loader
	public static GRUMB load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		NeuralNet g_read = (NeuralNet) JSON.deserialize(o.get("read_gate"));
		NeuralNet g_write = (NeuralNet) JSON.deserialize(o.get("write_gate"));
		NeuralNet g_in = (NeuralNet) JSON.deserialize(o.get("input_gate"));
		NeuralNet input = (NeuralNet) JSON.deserialize(o.get("input"));
		NeuralNet output = (NeuralNet) JSON.deserialize(o.get("output"));
		int size = o.getInt("size");
		
		return new GRUMB(size, g_read, g_write, g_in, input, output);
	}
}
