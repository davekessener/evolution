package dave.evo.nn.hyper;

import java.util.function.DoubleUnaryOperator;

public class ScaledHypercubePainter implements HypercubePainter
{
	private final HypercubePainter mSuper;
	private final DoubleUnaryOperator mF;
	
	public ScaledHypercubePainter(HypercubePainter s, DoubleUnaryOperator f)
	{
		mSuper = s;
		mF = f;
	}

	@Override
	public double at(double x0, double y0, double x1, double y1)
	{
		return mF.applyAsDouble(mSuper.at(x0, y0, x1, y1));
	}
}
