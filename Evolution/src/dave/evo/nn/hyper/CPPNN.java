package dave.evo.nn.hyper;

import dave.evo.common.NeuralNet;

public class CPPNN implements HypercubePainter
{
	private final NeuralNet mNN;
	
	public CPPNN(NeuralNet nn)
	{
		if(nn.inputs() != 5 || nn.outputs() != 1)
			throw new IllegalArgumentException();
		
		mNN = nn;
	}

	@Override
	public double at(double x0, double y0, double x1, double y1)
	{
		double output = mNN.evaluate(x0, y0, x1, y1, distance(x1 - x0, y1 - y0))[0];
		
		return (2 * output - 1);
	}
	
	private static double distance(double dx, double dy)
	{
		return Math.sqrt(dx * dx + dy * dy);
	}
}
