package dave.evo.nn.hyper;

public interface HypercubePainter
{
	public abstract double at(double x0, double y0, double x1, double y1);
}
