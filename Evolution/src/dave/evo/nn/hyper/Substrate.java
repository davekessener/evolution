package dave.evo.nn.hyper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import dave.evo.dna.GeneSequence;
import dave.json.Container;
import dave.json.JsonCollectors;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;
import dave.util.Utils;
import dave.util.math.Vec2;

@Container
public class Substrate implements Saveable
{
	private final Node mBias;
	private final List<Node> mInputs, mOutputs, mHidden;
	private final List<Link> mLinks;
	private final Map<Link, Double> mWeights;
	
	public Substrate(Node bias, List<Node> in, List<Node> out, List<Node> h, List<Link> l, Map<Link, Double> w)
	{
		mBias = bias;
		mInputs = new ArrayList<>(in);
		mOutputs = new ArrayList<>(out);
		mHidden = new ArrayList<>(h);
		mLinks = new ArrayList<>(l);
		mWeights = new HashMap<>(w);
	}
	
	public Node bias() { return mBias; }
	
	public boolean done() { return mWeights.size() == mLinks.size(); }
	
	public Stream<Link> links() { return mLinks.stream(); }
	public Stream<Link> open() { return mLinks.stream().filter(l -> !mWeights.containsKey(l)); }
	public Stream<Node> inputs() { return mInputs.stream(); }
	public Stream<Node> outputs() { return mOutputs.stream(); }
	public Stream<Node> hidden() { return mHidden.stream(); }
	
	public GeneSequence paint(HypercubePainter f)
	{
		mLinks.forEach(l -> mWeights.put(l, f.at(l.from.x, l.from.y, l.to.x, l.to.y)));
		
		return build();
	}
	
	public GeneSequence build()
	{
		if(!done())
			throw new IllegalStateException();
		
		GeneSequence.Builder genes = new GeneSequence.Builder();
		Map<Integer, Long> m = new HashMap<>();
		
		m.put(0, 0L);
		
		mInputs.forEach(e -> m.put(e.id, genes.addInput()));
		mOutputs.forEach(e -> m.put(e.id, genes.addOutput(e.activation)));
		mHidden.forEach(e -> m.put(e.id, genes.addHidden(e.activation)));
		
		mLinks.forEach(l -> genes.link(m.get(l.from.id), m.get(l.to.id), mWeights.get(l)));
		
		return genes.build();
	}
	
	@Override
	@Saver
	public JsonValue save()
	{
		JsonObject json = new JsonObject();
		
		json.put("bias", saveNode(mBias));
		Stream.of(Utils.pair("inputs", mInputs), Utils.pair("outputs", mOutputs), Utils.pair("hidden", mHidden))
			.forEach(e -> json.put(e.first, e.second.stream().map(Substrate::saveNode).collect(JsonCollectors.ofArray())));
		json.put("links", mLinks.stream().map(Substrate::saveLink).collect(JsonCollectors.ofArray()));
		
		return json;
	}
	
	@Loader
	public static Substrate load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		Map<Integer, Node> nodes = new HashMap<>();
		
		Function<JsonValue, Node> loadNode = v -> {
			JsonObject no = (JsonObject) v;
			
			int id = no.getInt("id");
			double x = no.getDouble("x");
			double y = no.getDouble("y");
			String a = no.getString("activation");
			
			Node n = new Node(id, x, y, a);
			
			nodes.put(id, n);
			
			return n;
		};
		
		Function<JsonValue, Link> loadLink = v -> {
			JsonObject lo = (JsonObject) v;
			
			int from = lo.getInt("from");
			int to = lo.getInt("to");
			
			return new Link(nodes.get(from), nodes.get(to));
		};
		
		Node bias = loadNode.apply(o.get("bias"));
		List<Node> in = o.getArray("inputs").stream().map(loadNode).collect(Collectors.toList());
		List<Node> out = o.getArray("outputs").stream().map(loadNode).collect(Collectors.toList());
		List<Node> hidden = o.getArray("hidden").stream().map(loadNode).collect(Collectors.toList());
		List<Link> links = o.getArray("links").stream().map(loadLink).collect(Collectors.toList());
		
		return new Substrate(bias, in, out, hidden, links, new HashMap<>());
	}
	
	private static JsonValue saveNode(Node v)
	{
		JsonObject json = new JsonObject();
		
		json.putInt("id", v.id);
		json.putNumber("x", v.x);
		json.putNumber("y", v.y);
		json.putString("activation", v.activation);
		
		return json;
	}
	
	private static JsonValue saveLink(Link l)
	{
		JsonObject json = new JsonObject();
		
		json.putInt("from", l.from.id);
		json.putInt("to", l.to.id);
		
		return json;
	}
	
	public static class Node
	{
		public final int id;
		public final double x, y;
		public final String activation;
		
		public Node(int id, double x, double y) { this(id, x, y, null); }
		public Node(int id, double x, double y, String activation)
		{
			this.id = id;
			this.x = x;
			this.y = y;
			this.activation = activation;
		}
	}
	
	public static class Link
	{
		public final Node from, to;
		
		public Link(Node from, Node to)
		{
			this.from = from;
			this.to = to;
		}
	}
	
	public static class Builder
	{
		private final Node mBias;
		private final List<Node> mInputs, mOutputs, mHidden;
		private final List<Link> mLinks;
		private final Map<Link, Double> mWeights;
		private int mNextID;
		
		public Builder(Vec2 b) { this(b.x, b.y); }
		public Builder(double x, double y)
		{
			mBias = new Node(0, x, y);
			mInputs = new ArrayList<>();
			mOutputs = new ArrayList<>();
			mHidden = new ArrayList<>();
			mLinks = new ArrayList<>();
			mWeights = new HashMap<>();
			
			mNextID = 1;
		}
		
		public Node bias() { return mBias; }

		public Node addInput(Vec2 v) { return addInput(v.x, v.y); }
		public Node addInput(double x, double y)
		{
			Node n = new Node(mNextID++, x, y);
			
			mInputs.add(n);
			
			return n;
		}

		public Node addOutput(Vec2 v, String a) { return addOutput(v.x, v.y, a); }
		public Node addOutput(double x, double y, String a)
		{
			Node n = new Node(mNextID++, x, y, a);
			
			mOutputs.add(n);
			
			return n;
		}
		
		public Node addHidden(double x, double y, String a)
		{
			Node n = new Node(mNextID++, x, y, a);
			
			mHidden.add(n);
			
			return n;
		}
		
		public Link link(Node from, Node to, double w)
		{
			Link l = link(from, to);
			
			setWeight(l, w);
			
			return l;
		}
		
		public Link link(Node from, Node to)
		{
			if(from == null || to == null)
				throw new NullPointerException();
			
			Link l = new Link(from, to);
			
			mLinks.add(l);
			
			return l;
		}
		
		public void setWeight(Link l, double w)
		{
			mWeights.put(l, w);
		}
		
		public Substrate build()
		{
			return new Substrate(mBias, mInputs, mOutputs, mHidden, mLinks, mWeights);
		}
	}
}
