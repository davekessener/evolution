package dave.evo.nn;

public interface Neuron
{
	public abstract double get( );
}
