package dave.evo.nn;

import java.util.HashMap;
import java.util.Map;
import java.util.function.DoubleUnaryOperator;

import dave.util.RNG;
import dave.util.Utils;

public class Activation implements DoubleUnaryOperator
{
	private final Type mType;
	private final DoubleUnaryOperator mCallback;
	private final double mMin, mMax;
	
	private Activation(Type t, DoubleUnaryOperator f, double min, double max)
	{
		mType = t;
		mCallback = f;
		mMin = min;
		mMax = max;
		
		sActivations.put(t, this);
	}
	
	public Type type() { return mType; }
	public double min() { return mMin; }
	public double max() { return mMax; }
	
	public double applyAsOutput(double x)
	{
		return (apply(x) - mMin) / (mMax - mMin);
	}

	public double apply(double x)
	{
		return applyAsDouble(x);
	}
	
	@Override
	public double applyAsDouble(double x)
	{
		return mCallback.applyAsDouble(x);
	}
	
	@Override
	public String toString()
	{
		return mType.toString();
	}
	
	public static enum Type
	{
		GAUSS,
		ABS,
		SINE,
		SIGMOID,
		STEEP_SIG,
		BI_SIGMOID,
		TANH,
		FULL_STEP,
		HALF_STEP,
		LINEAR
	}
	
	public static Activation get(Type t)
	{
		Activation a = sActivations.get(t);
		
		if(a == null)
			throw new IllegalArgumentException("No activation " + t);
		
		return a;
	}
	
	public static Activation any(RNG rng)
	{
		return Utils.any(CPPNN_VALID, rng);
	}
	
	private static final Map<Type, Activation> sActivations = new HashMap<>();
	
	public static final Activation GAUSS = new Activation(Type.GAUSS, x -> Math.exp(-(x * x) * 4), 0, 1);
	public static final Activation ABS = new Activation(Type.ABS, x -> Math.abs(x), 0, 1);
	public static final Activation SINE = new Activation(Type.SINE, x -> Math.sin(x), -1, 1);
	public static final Activation SIGMOID = new Activation(Type.SIGMOID, x -> 1 / (1 + Math.exp(-x)), 0, 1);
	public static final Activation STEEP_SIG = new Activation(Type.STEEP_SIG, x -> SIGMOID.apply(4.9 * x), 0, 1);
	public static final Activation BI_SIGMOID = new Activation(Type.BI_SIGMOID, x -> SIGMOID.apply(x) * 2 - 1, -1, 1);
	public static final Activation TANH = new Activation(Type.TANH, x -> 2 * SIGMOID.apply(2 * x) - 1, -1, 1);
	public static final Activation FULL_STEP = new Activation(Type.FULL_STEP, x -> x < 0 ? -1 : 1, -1, 1);
	public static final Activation HALF_STEP = new Activation(Type.HALF_STEP, x -> x < 0 ? 0 : 1, 0, 1);
	public static final Activation LINEAR = new Activation(Type.LINEAR, x -> x, 0, 1);
	
	private static final Activation[] CPPNN_VALID = new Activation[] { GAUSS, ABS, SINE, SIGMOID, TANH, BI_SIGMOID, LINEAR };
}
