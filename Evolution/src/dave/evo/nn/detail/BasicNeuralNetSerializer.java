package dave.evo.nn.detail;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import dave.evo.nn.Activation;
import dave.evo.nn.BasicNeuralNet;
import dave.evo.nn.BiasNeuron;
import dave.evo.nn.ConnectedNeuron;
import dave.evo.nn.HiddenNeuron;
import dave.evo.nn.InputNeuron;
import dave.evo.nn.Neuron;
import dave.evo.nn.OutputNeuron;
import dave.json.JsonBuilder;
import dave.json.JsonCollectors;
import dave.json.JsonObject;
import dave.json.JsonString;
import dave.json.JsonValue;

public class BasicNeuralNetSerializer
{
	public static JsonValue save(List<InputNeuron> inputs, List<OutputNeuron> outputs)
	{
		JsonObject json = new JsonObject();
		
		json.put("inputs", inputs.stream().map(n -> new JsonString(neuronID(n))).collect(JsonCollectors.ofArray()));
		json.put("outputs", outputs.stream().map(n -> new JsonString(neuronID(n))).collect(JsonCollectors.ofArray()));
		
		Map<String, JsonValue> neurons = new HashMap<>();
		
		outputs.forEach(n -> saveNeuron(n, neurons));
		
		json.put("neurons", neurons.entrySet().stream().collect(JsonCollectors.ofObject()));
		
		return json;
	}
	
	public static BasicNeuralNet load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		Map<String, Neuron> neurons = new HashMap<>();
		
		o.getArray("inputs").stream()
			.map(v -> ((JsonString) v).get())
			.forEach(id -> neurons.put(id, new InputNeuron()));
		
		List<String> out = Arrays.asList(o.getArray("outputs").asStrings());
		
		o.getObject("neurons").stream()
			.forEach(e -> neurons.put(e.getKey(), createNeuron(out.contains(e.getKey()), e.getValue())));
		o.getObject("neurons").stream()
			.forEach(e -> loadNeuron(e.getKey(), e.getValue(), neurons));
		
		if(neurons.values().stream().filter(n -> (n instanceof BiasNeuron)).count() > 1)
			throw new IllegalStateException();
		
		List<InputNeuron> inputs = Stream.of(o.getArray("inputs").asStrings())
			.map(id -> (InputNeuron) neurons.get(id))
			.collect(Collectors.toList());
		List<OutputNeuron> outputs = out.stream()
			.map(id -> (OutputNeuron) neurons.get(id))
			.collect(Collectors.toList());
		
		return new BasicNeuralNet(inputs, outputs);
	}

	private static void loadNeuron(String id, JsonValue json, Map<String, Neuron> neurons)
	{
		ConnectedNeuron cn = (ConnectedNeuron) neurons.get(id);
		
		((JsonObject) json).getArray("connections").stream()
			.map(v -> (JsonObject) v)
			.forEach(o -> {
				double weight = o.getDouble("weight");
				Neuron other = neurons.get(o.getString("other"));
				
				if(other == null)
				{
					neurons.put(o.getString("other"), other = new BiasNeuron());
				}
				
				cn.connect(weight, other);
			});
	}
	
	private static Neuron createNeuron(boolean out, JsonValue v)
	{
		JsonObject o = (JsonObject) v;
		Activation a = Activation.get(Activation.Type.valueOf(o.getString("activation")));
		
		return out ? new OutputNeuron(a) : new HiddenNeuron(a);
	}
	
	private static void saveNeuron(Neuron n, Map<String, JsonValue> cache)
	{
		String key = neuronID(n);
		
		if(cache.containsKey(key))
			return;
		
		if(n instanceof ConnectedNeuron)
		{
			ConnectedNeuron cn = (ConnectedNeuron) n;
			
			cache.put(key, saveNeuron(cn));
			
			cn.connections().forEach(c -> saveNeuron(c.other, cache));
		}
	}
	
	private static JsonValue saveNeuron(ConnectedNeuron cn)
	{
		JsonObject json = new JsonObject();
		
		json.putString("activation", cn.activation().toString());
		json.put("connections", cn.connections().map(BasicNeuralNetSerializer::saveConnection).collect(JsonCollectors.ofArray()));
		
		return json;
	}
	
	private static JsonValue saveConnection(ConnectedNeuron.Connection c)
	{
		return (new JsonBuilder())
			.putNumber("weight", c.weight)
			.putString("other", neuronID(c.other))
			.toJSON();
	}
	
	private static String neuronID(Neuron n)
	{
		return String.format("%08x", n.hashCode());
	}
}
