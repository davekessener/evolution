package dave.evo.nn.detail;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import dave.evo.dna.BiasGene;
import dave.evo.dna.Gene;
import dave.evo.dna.HiddenGene;
import dave.evo.dna.InputGene;
import dave.evo.dna.LinkGene;
import dave.evo.dna.OutputGene;
import dave.evo.nn.Activation;
import dave.evo.nn.BasicNeuralNet;
import dave.evo.nn.BiasNeuron;
import dave.evo.nn.ConnectedNeuron;
import dave.evo.nn.HiddenNeuron;
import dave.evo.nn.InputNeuron;
import dave.evo.nn.Neuron;
import dave.evo.nn.OutputNeuron;
import dave.util.relay.Overloaded;
import dave.util.relay.Relay;

public class BasicNeuralNetBuilder
{
	private final Relay mRelay;
	private final Map<Long, Neuron> mNeurons;
	private final Set<Container<InputNeuron>> mInputs;
	private final Set<Container<OutputNeuron>> mOutputs;
	
	public BasicNeuralNetBuilder()
	{
		mRelay = new Relay(this);
		mNeurons = new HashMap<>();
		mInputs = new TreeSet<>(BasicNeuralNetBuilder.Container::compare);
		mOutputs = new TreeSet<>(BasicNeuralNetBuilder.Container::compare);
	}
	
	public BasicNeuralNet build()
	{
		return new BasicNeuralNet(Container.convert(mInputs), Container.convert(mOutputs));
	}
	
	public void add(Gene g)
	{
		mRelay.call(g);
	}
	
	@Overloaded
	private void addInput(InputGene g)
	{
		InputNeuron n = new InputNeuron();
		
		mNeurons.put(g.marker(), n);
		mInputs.add(new Container<>(g.marker(), n));
	}
	
	@Overloaded
	private void addBias(BiasGene g)
	{
		mNeurons.put(g.marker(), new BiasNeuron());
	}
	
	@Overloaded
	private void addHidden(HiddenGene g)
	{
		mNeurons.put(g.marker(), new HiddenNeuron(Activation.get(Activation.Type.valueOf(g.activation()))));
	}
	
	@Overloaded
	private void addOutput(OutputGene g)
	{
		OutputNeuron n = new OutputNeuron(Activation.get(Activation.Type.valueOf(g.activation())));
		
		mNeurons.put(g.marker(), n);
		mOutputs.add(new Container<>(g.marker(), n));
	}
	
	@Overloaded
	private void addLink(LinkGene g)
	{
		if(g.active())
		{
			Neuron from = mNeurons.get(g.from());
			ConnectedNeuron to = (ConnectedNeuron) mNeurons.get(g.to());
			
			if(from == null)
				throw new NullPointerException(String.format("No such neuron: from-%d", g.from()));
			
			if(to == null)
				throw new NullPointerException(String.format("No such neuron: to-%d", g.to()));
			
			to.connect(g.weight(), from);
		}
	}
	
	private static class Container<T>
	{
		public final long marker;
		public final T content;
		
		public Container(long marker, T content)
		{
			this.marker = marker;
			this.content = content;
		}
		
		public static <T> int compare(Container<T> c1, Container<T> c2)
		{
			return Long.compare(c1.marker, c2.marker);
		}
		
		public static <T> List<T> convert(Collection<Container<T>> c)
		{
			return c.stream().map(e -> e.content).collect(Collectors.toList());
		}
	}
}
