package dave.evo.nn;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public abstract class ConnectedNeuron implements Neuron
{
	private final Activation mActivation;
	private final List<Connection> mConnections;
	private double mValue;
	private boolean mDone;
	
	protected ConnectedNeuron(Activation a)
	{
		mActivation = a;
		mConnections = new ArrayList<>();
		
		mDone = false;
	}
	
	public Activation activation() { return mActivation; }
	public Stream<Connection> connections() { return mConnections.stream(); }
	
	public void connect(double w, Neuron o)
	{
		if(o == null)
			throw new NullPointerException();
		
		mConnections.add(new Connection(w, o));
	}
	
	@Override
	public double get()
	{
		if(mConnections.isEmpty())
			throw new IllegalStateException();
		
		if(!mDone)
		{
			mDone = true;
			
			mValue = compute();
		}
		
		return mValue;
	}
	
	public void reset()
	{
		if(mDone)
		{
			mDone = false;
			
			for(Connection c : mConnections)
			{
				if(c.other instanceof ConnectedNeuron)
				{
					((ConnectedNeuron) c.other).reset();
				}
			}
			
//			mConnections.stream()
//				.map(c -> c.other)
//				.filter(n -> (n instanceof ConnectedNeuron))
//				.map(n -> (ConnectedNeuron) n)
//				.forEach(ConnectedNeuron::reset);
		}
	}
	
	private double compute()
	{
		double v = 0;
		
		for(Connection c : mConnections)
		{
			v += c.weight * c.other.get();
		}
		
		return mActivation.apply(v);

//		return mActivation.apply(mConnections.stream().mapToDouble(c -> c.weight * c.other.get()).sum());
	}
	
	public static class Connection
	{
		public final double weight;
		public final Neuron other;
		
		private Connection(double weight, Neuron other)
		{
			this.weight = weight;
			this.other = other;
		}
	}
}
