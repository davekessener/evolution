package dave.evo.eval.counter;

import dave.util.RNG;

public class RandomAI implements CounterAI
{
	private final RNG mRandom;
	
	public RandomAI(RNG rng)
	{
		mRandom = rng;
	}

	@Override
	public double step(double in)
	{
		return (mRandom.nextInt() & 1) == 0 ? -1 : 1;
	}

	@Override
	public void reset()
	{
	}
}
