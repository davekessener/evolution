package dave.evo.eval.counter;

import dave.util.config.Default;

public interface CountingOptions
{
	@Default("1")
	public abstract int min_length( );
	
	@Default("9")
	public abstract int max_length( );
	
	@Default("0.25")
	public abstract double zero_chance( );
	
	@Default("25")
	public abstract int turns( );
}
