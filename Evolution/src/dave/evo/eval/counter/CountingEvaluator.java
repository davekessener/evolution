package dave.evo.eval.counter;

import dave.evo.common.NeuralNet;
import dave.evo.config.EvaluationOptions;
import dave.evo.eval.BiasedEvaluator;
import dave.util.Averager;
import dave.util.RNG;

public class CountingEvaluator extends BiasedEvaluator<CounterAI>
{
	private final CountingOptions mOptions;
	private final RNG mRandom;
	
	public CountingEvaluator(CountingOptions o, RNG rng, EvaluationOptions eo)
	{
		super(eo.fitness_bias(), o.turns());
		
		mOptions = o;
		mRandom = rng;
	}
	
	@Override
	public double evaluate(CounterAI ai)
	{
		Averager avg = new Averager();
		
		for(int l = mOptions.min_length() ; l <= mOptions.max_length() ; ++l)
		{
			int pos = 0, neg = 0;
			double result = Double.NaN;
			
			ai.reset();

			for(int j = 0 ; j < l ; ++j)
			{
				int v = (mRandom.nextInt() & 1);
				
				if(v == 0)
				{
					v = -1;
				}
				
				if(mRandom.nextDouble() < mOptions.zero_chance())
				{
					v = 0;
				}
				
				if(v == 1) ++pos;
				if(v == -1) ++neg;
				
				result = ai.step(v);
			}
			
			double expected = 0;
			
			if(pos > neg) expected =  1;
			if(pos < neg) expected = -1;
			
			avg.add(Math.max(0, 1 - 0.5 * Math.abs(expected - result)));
		}
		
		return avg.get();
	}

	@Override
	protected CounterAI wrap(NeuralNet nn)
	{
		return new NeuralNetAI(nn);
	}

	@Override
	protected double computeBias()
	{
		return evaluate(new RandomAI(mRandom));
	}
}
