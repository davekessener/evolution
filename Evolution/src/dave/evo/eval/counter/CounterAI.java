package dave.evo.eval.counter;

public interface CounterAI
{
	public abstract double step(double in);
	
	public abstract void reset( );
}
