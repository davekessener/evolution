package dave.evo.eval.counter;

import dave.evo.common.NeuralNet;

public class NeuralNetAI implements CounterAI
{
	private final NeuralNet mAI;
	
	public NeuralNetAI(NeuralNet nn)
	{
		mAI = nn;
		
		if(nn.inputs() != 1 || nn.outputs() != 1)
			throw new IllegalArgumentException();
		
		mAI.reset();
	}

	@Override
	public double step(double in)
	{
		return mAI.evaluate(in)[0] * 2 - 1;
	}

	@Override
	public void reset()
	{
		mAI.reset();
	}
}
