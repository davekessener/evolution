package dave.evo.eval;

import java.util.function.ToDoubleFunction;

import dave.evo.common.NeuralNet;
import dave.util.MathUtils;

public class XorEvaluator implements ToDoubleFunction<NeuralNet>
{
	private final double mThreshold;
	
	public XorEvaluator() { this(0.25); }
	public XorEvaluator(double th)
	{
		mThreshold = th;
	}
	
	@Override
	public double applyAsDouble(NeuralNet nn)
	{
		double r1 = nn.evaluate(0, 0)[0];
		double r2 = nn.evaluate(0, 1)[0];
		double r3 = nn.evaluate(1, 0)[0];
		double r4 = nn.evaluate(1, 1)[0];

		r1 = clamp(r1);
		r2 = clamp(r2);
		r3 = clamp(r3);
		r4 = clamp(r4);
		
		double p1 = Math.abs(0.0 - r1);
		double p2 = Math.abs(1.0 - r2);
		double p3 = Math.abs(1.0 - r3);
		double p4 = Math.abs(0.0 - r4);
		
		double r = 1 - (p1 + p2 + p3 + p4) / 4.0;
		
		r *= r;

		return r;
	}
	
	private double clamp(double v) { return MathUtils.clamp((v - mThreshold) / (1 - 2 * mThreshold)); }
}
