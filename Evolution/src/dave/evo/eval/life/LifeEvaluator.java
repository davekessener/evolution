package dave.evo.eval.life;

import java.util.function.ToDoubleFunction;

import dave.evo.common.NeuralNet;
import dave.util.Averager;
import dave.util.RNG;

public class LifeEvaluator implements ToDoubleFunction<NeuralNet>
{
	private final LifeOptions mOptions;
	private final RNG mRandom;
	
	public LifeEvaluator(LifeOptions o, RNG rng)
	{
		mOptions = o;
		mRandom = rng;
	}
	
	@Override
	public double applyAsDouble(NeuralNet nn)
	{
		Averager avg = new Averager();
		
		for(int i = 0 ; i < mOptions.turns() ; ++i)
		{
			nn.reset();
			
			World w = new World(new NeuralNetAI(nn), mRandom, mOptions);
			
			while(!w.done())
			{
				w.tick();
			}
			
			avg.add(w.fitness());
		}
		
		return avg.get();
	}
}
