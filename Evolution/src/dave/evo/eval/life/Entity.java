package dave.evo.eval.life;

import dave.evo.eval.maze.View;
import dave.util.math.IVec2;

public class Entity
{
	private final AI mAI;
	private final int mDistance;
	private final LifeOptions mOptions;
	private double mFood, mWater;
	private double mHunger;
	private World mWorld;
	private View mView;
	private boolean mAlive;
	private IVec2 mPosition;
	private int mCounter;
	
	public Entity(AI ai, int l, World w, LifeOptions o)
	{
		mAI = ai;
		mDistance = l;
		mOptions = o;
		
		mFood = mWater = 0.75;
		mHunger = 0.0;
		mWorld = w;
		mView = View.UP;
		mAlive = true;
		
		mPosition = new IVec2(mWorld.width() / 2, mWorld.height() / 2);
		
		mCounter = 0;
	}
	
	public boolean alive() { return mAlive; }
	public int lifetime() { return mCounter; }
	public IVec2 position() { return mPosition; }
	public View view() { return mView; }
	public double food() { return mFood; }
	public double water() { return mWater; }
	public AI ai() { return mAI; }
	
	public void eat() { mFood = 1; }
	public void drink() { mWater = 1; }
	
	public void tick()
	{
		if(!mAlive)
			return;
		
		++mCounter;
		
		mFood -= mHunger;
		mWater -= mHunger;
		
		if(mFood <= 0 || mWater <= 0)
		{
			mAlive = false;
			
			return;
		}
		
		if(mCounter > mOptions.hunger_offset_factor() * mDistance)
		{
			mHunger += mOptions.hunger_growth_rate() / mDistance;
		}
		
		boolean forward = mWorld.wall(mPosition.add(mView.v));
		boolean left = mWorld.wall(mPosition.add(mView.left().v));
		boolean right = mWorld.wall(mPosition.add(mView.right().v));
		
		mAI.setResource(mFood, mWater);
		mAI.setSmell(mWorld.smell(mPosition));
		mAI.setView(left, forward, right);
		
		mAI.update();
		
		if(mAI.forward() > 0.5)
		{
			if(!forward)
			{
				mPosition = mPosition.add(mView.v);
			}
		}
		else
		{
			double d = mAI.right() - mAI.left();
			
			if(d < -0.5)
			{
				mView = mView.left();
			}
			else if(d > 0.5)
			{
				mView = mView.right();
			}
		}
	}
}
