package dave.evo.eval.life;

public interface AI
{
	public abstract void update( );

	public abstract double forward( );
	public abstract double left( );
	public abstract double right( );
	
	public abstract void setView(boolean left, boolean front, boolean right);
	public abstract void setResource(double food, double water);
	public abstract void setSmell(double intensity);
}
