package dave.evo.eval.life;

import java.util.stream.Stream;

import dave.evo.eval.maze.BaseWorld;
import dave.evo.eval.maze.View;
import dave.util.RNG;
import dave.util.math.IVec2;

public class World extends BaseWorld
{
	private final double[] mSmell;
	private final IVec2 mFood, mWater;
	private final int mDistance;
	private final Entity mEntity;
	private final LifeOptions mOptions;
	
	public World(AI ai, RNG rng, LifeOptions o)
	{
		super(o.world_size(), rng);
		
		int s = o.world_size();
		
		mSmell = new double[size() * size()];
		mOptions = o;
		
		int v = rng.nextInt();
		int x0 = 1, x1 = 2 * s - 1;
		int y0 = 1, y1 = 2 * s - 1;
		boolean f1 = ((v & 1) == 0);
		boolean f2 = ((v & 2) == 0);
		
		mFood  = new IVec2(f1 ? x0 : x1, f2 ? y0 : y1);
		mWater = new IVec2(f1 ? x1 : x0, f2 ? y1 : y0);
		mDistance = distance(mFood, mWater);
		
		computeSmell(mFood, 1);
		
		mEntity = new Entity(ai, mDistance, this, mOptions);
	}
	
	public IVec2 food() { return mFood; }
	public IVec2 water() { return mWater; }
	public Entity entity() { return mEntity; }
	
	public double smell(int x, int y) { return mSmell[x + y * width()]; }
	public double smell(IVec2 p) { return smell(p.x, p.y); }
	
	public boolean done() { return !mEntity.alive(); }
	
	public double fitness()
	{
		double min = mOptions.life_minimum(mDistance);
		double max = mOptions.life_expectancy(mDistance);
		
		return Math.max(0, Math.min(1, (mEntity.lifetime() - min) / (max - min)));
	}
	
	public void tick()
	{
		mEntity.tick();
		
		if(mEntity.position().equals(mFood))
		{
			mEntity.eat();
		}
		else if(mEntity.position().equals(mWater))
		{
			mEntity.drink();
		}
	}
	
	private void computeSmell(IVec2 p, double v)
	{
		if(wall(p))
			return;
		
		if(smell(p) >= v)
			return;
		
		mSmell[p.x + p.y * width()] = v;
		
		Stream.of(View.values())
			.forEach(d -> computeSmell(p.add(d.v), v * (1 - SMELL_DECAY)));
	}
	
	private static final double SMELL_DECAY = 0.1;
}
