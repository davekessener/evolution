package dave.evo.eval.life;

public abstract class BaseAI implements AI
{
	private double mForward, mLeft, mRight;
	private final boolean[] mView;
	private double mFood, mWater, mSmell;
	
	protected BaseAI()
	{
		mForward = mLeft = mRight = 0;
		mFood = mWater = mSmell = 0;
		mView = new boolean[] { true, true, true };
	}
	
	protected void setForward(double v) { mForward = v; }
	protected void setLeft(double v) { mLeft = v; }
	protected void setRight(double v) { mRight = v; }

	protected boolean viewLeft() { return mView[0]; }
	protected boolean viewFront() { return mView[1]; }
	protected boolean viewRight() { return mView[2]; }
	protected double food() { return mFood; }
	protected double water() { return mWater; }
	protected double smell() { return mSmell; }

	@Override
	public double forward()
	{
		return mForward;
	}

	@Override
	public double left()
	{
		return mLeft;
	}

	@Override
	public double right()
	{
		return mRight;
	}

	@Override
	public void setView(boolean left, boolean front, boolean right)
	{
		mView[0] = left;
		mView[1] = front;
		mView[2] = right;
	}

	@Override
	public void setResource(double food, double water)
	{
		mFood = food;
		mWater = water;
	}

	@Override
	public void setSmell(double intensity)
	{
		mSmell = intensity;
	}
}
