package dave.evo.eval.life;

import dave.evo.common.NeuralNet;

public class NeuralNetAI extends BaseAI
{
	private final NeuralNet mAI;
	
	public NeuralNetAI(NeuralNet nn)
	{
		if(nn.inputs() != 6 || nn.outputs() != 3)
			throw new IllegalArgumentException();
		
		mAI = nn;
	}

	@Override
	public void update()
	{
		double[] r = mAI.evaluate(viewLeft() ? 1 : 0, viewFront() ? 1 : 0, viewRight() ? 1 : 0, smell(), food(), water());
		
		setLeft(r[0]);
		setForward(r[1]);
		setRight(r[2]);
	}
}
