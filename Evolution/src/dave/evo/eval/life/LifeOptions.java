package dave.evo.eval.life;

import dave.util.config.Default;

public interface LifeOptions
{
	@Default("10.0")
	public abstract double hunger_offset_factor( );
	
	@Default("0.0025")
	public abstract double hunger_growth_rate( );
	
	@Default("5")
	public abstract int world_size( );
	
	@Default("5")
	public abstract int turns( );
	
	public default double life_minimum(double distance)
	{
		return distance * hunger_offset_factor() + Math.sqrt(2 * distance / hunger_growth_rate());
	}
	
	public default double life_expectancy(double distance)
	{
		return distance * hunger_offset_factor() + 1.0 / (2 * hunger_growth_rate());
	}
}
