package dave.evo.eval.life;

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Stream;

import dave.evo.eval.maze.View;
import dave.util.log.Logger;
import dave.util.math.IVec2;

public class SimpleAI extends BaseAI
{
	private final Map<IVec2, Type> mWorld;
	private final Deque<Action> mActions;
	private IVec2 mFood, mWater;
	private View mView;
	private IVec2 mPosition;
	private State mState;
	private boolean mDone;
	
	public SimpleAI()
	{
		mWorld = new HashMap<>();
		mView = View.UP;
		mPosition = IVec2.ORIGIN;
		mState = new StateSearchFood();
		mActions = new LinkedList<>();
	}

	@Override
	public void update()
	{
		updateWorld();
		
		setForward(0);
		setLeft(0);
		setRight(0);
		
		mDone = false;
		
		Logger.DEFAULT.log("Starting step");
		Logger.DEFAULT.log("Viewing %s - %s - %s", viewLeft() ? "W" : "F", viewFront() ? "W" : "F", viewRight() ? "W" : "F");
		
		while(!mDone)
		{
			if(!mActions.isEmpty())
			{
				Action a = mActions.poll();
				
				Logger.DEFAULT.log("Performing %s", a.getClass().getSimpleName());
				
				a.perform();
			}
			else
			{
				mState.update();
			}
		}

		Logger.DEFAULT.log("Finishing step");
		
		updateView();
	}
	
	private void updateWorld()
	{
		IVec2 forward = mPosition.add(mView.v);
		IVec2 left = mPosition.add(mView.left().v);
		IVec2 right = mPosition.add(mView.right().v);
		
		mWorld.putIfAbsent(mPosition, Type.FLOOR);
		mWorld.putIfAbsent(forward, viewFront() ? Type.WALL : Type.FLOOR);
		mWorld.putIfAbsent(left, viewLeft() ? Type.WALL : Type.FLOOR);
		mWorld.putIfAbsent(right, viewRight() ? Type.WALL : Type.FLOOR);
	}
	
	private void updateView()
	{
		if(forward() > 0.5)
		{
			if(!viewFront())
			{
				mPosition = mPosition.add(mView.v);
			}
		}
		else
		{
			double d = right() - left();
			
			if(d < -0.5)
			{
				mView = mView.left();
			}
			else if(d > 0.5)
			{
				mView = mView.right();
			}
		}
		
		Logger.DEFAULT.log("@%s facing %s", mPosition, mView.v);
	}
	
	private void faceDirection(View v)
	{
		if(v != mView)
		{
			if(v == mView.left())
			{
				mActions.add(new ActionTurn(true));
			}
			else if(v == mView.right())
			{
				mActions.add(new ActionTurn(false));
			}
			else
			{
				mActions.add(new ActionTurnAround());
			}
		}
	}
	
	private void takeStep()
	{
		mActions.add(new ActionStep());
	}
	
	@Override
	protected void setForward(double forward)
	{
		super.setForward(forward);
		
		mDone = true;
	}
	
	@Override
	protected void setLeft(double left)
	{
		super.setLeft(left);
		
		mDone = true;
	}
	
	@Override
	protected void setRight(double right)
	{
		super.setRight(right);
		
		mDone = true;
	}
	
	private void goTo(IVec2 p)
	{
		Map<IVec2, Integer> shortest = new HashMap<>();
		Queue<Pos> open = new PriorityQueue<>(1, (p0, p1) -> Integer.compare(p0.d, p1.d) * 10 + 1);
		
		open.add(new Pos(mPosition, null));
		
		while(true)
		{
			Pos q = open.poll();
			
			if(p.equals(q.p))
			{
				Logger.DEFAULT.log("From %s go to %s", mPosition, p);
				
				gotoImpl(q);
				
				return;
			}
			else
			{
				Integer d = shortest.get(q.p);
				
				if(d == null || d > q.d)
				{
					shortest.put(q.p, q.d);
					
					Stream.of(View.values())
						.map(v -> q.p.add(v.v))
						.filter(v -> mWorld.get(v) == Type.FLOOR)
						.forEach(v -> open.add(new Pos(v, q)));
				}
			}
		}
	}
	
	private View gotoImpl(Pos p)
	{
		if(p.previous == null)
		{
			return mView;
		}
		else
		{
			View v1 = gotoImpl(p.previous);
			View v2 = View.of(p.p.sub(p.previous.p));
			
			if(v1 != v2)
			{
				if(v1.left() == v2)
				{
					mActions.add(new ActionTurn(true));
				}
				else if(v1.right() == v2)
				{
					mActions.add(new ActionTurn(false));
				}
				else
				{
					mActions.add(new ActionTurnAround());
				}
				
				Logger.DEFAULT.log("Turning %s from %s to %s", (v1.left() == v2) ? "LEFT" : "RIGHT", v1, v2);
			}
			
			mActions.add(new ActionStep());
			
			Logger.DEFAULT.log("Stepping from %s to %s", p.previous.p, p.p);
			
			return v2;
		}
	}
	
	private static enum Type
	{
		FLOOR,
		WALL
	}
	
	private static class Pos
	{
		public final IVec2 p;
		public final int d;
		public final Pos previous;
		
		public Pos(IVec2 p, Pos l)
		{
			this.p = p;
			this.d = (l == null ? 0 : l.d + 1);
			this.previous = l;
		}
	}
	
	private interface Action
	{
		public abstract void perform();
	}
	
	private class ActionTurn implements Action
	{
		private final boolean mLeft;
		
		public ActionTurn(boolean left)
		{
			mLeft = left;
		}
		
		@Override
		public void perform()
		{
			if(mLeft)
			{
				SimpleAI.this.setLeft(1);
			}
			else
			{
				SimpleAI.this.setRight(1);
			}
		}
	}
	
	private class ActionTurnAround implements Action
	{
		@Override
		public void perform()
		{
			SimpleAI.this.mActions.push(new ActionTurn(true));
			SimpleAI.this.mActions.push(new ActionTurn(true));
		}
	}
	
	private class ActionStep implements Action
	{
		@Override
		public void perform()
		{
			SimpleAI.this.setForward(1);
		}
	}
	
	private interface State
	{
		public abstract void update();
	}
	
	private class StateSearchFood implements State
	{
		private final Map<IVec2, Double> mSmells;
		
		public StateSearchFood()
		{
			mSmells = new HashMap<>();
		}
		
		@Override
		public void update()
		{
			double smell = SimpleAI.this.smell();
			
			if(smell == 1)
			{
				SimpleAI.this.mFood = mPosition;
				SimpleAI.this.mState = new StateSearchWater();
				
				return;
			}
			
			mSmells.put(mPosition, smell);
			
			double best = smell;
			View dir = null, unknown = null;
			for(int i = 0 ; i < View.VIEWS ; ++i)
			{
				View d = View.values()[i];
				IVec2 p = mPosition.add(d.v);
				Double s = mSmells.get(p);
				
				if(s != null)
				{
					if(s > best)
					{
						best = s;
						dir = d;
					}
				}
				else
				{
					if(mWorld.get(p) != Type.WALL)
					{
						if(unknown == null)
						{
							unknown = d;
						}
					}
				}
			}
			
			if(dir == null)
			{
				dir = unknown;
			}
			
			SimpleAI.this.faceDirection(dir);
			SimpleAI.this.takeStep();
		}
	}
	
	private class StateSearchWater implements State
	{
		@Override
		public void update()
		{
			if(SimpleAI.this.water() == 1)
			{
				SimpleAI.this.mWater = mPosition;
				SimpleAI.this.mState = new StateGotoFood();
				
				return;
			}
			
			if(!viewRight())
			{
				mActions.add(new ActionTurn(false));
			}
			else if(!viewFront())
			{
			}
			else if(!viewLeft())
			{
				mActions.add(new ActionTurn(true));
			}
			else
			{
				mActions.add(new ActionTurnAround());
			}
			
			mActions.add(new ActionStep());
		}
	}
	
	private abstract class StateGoto implements State
	{
		private final IVec2 mGoal;
		
		public StateGoto(IVec2 g)
		{
			mGoal = g;
		}
		
		@Override
		public void update()
		{
			SimpleAI.this.goTo(mGoal);
		}
	}
	
	private class StateGotoFood extends StateGoto
	{
		public StateGotoFood()
		{
			super(SimpleAI.this.mFood);
		}
		
		@Override
		public void update()
		{
			super.update();
			
			SimpleAI.this.mState = new StateGotoWater();
		}
	}
	
	private class StateGotoWater extends StateGoto
	{
		public StateGotoWater()
		{
			super(SimpleAI.this.mWater);
		}
		
		@Override
		public void update()
		{
			super.update();
			
			SimpleAI.this.mState = new StateGotoFood();
		}
	}
}
