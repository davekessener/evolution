package dave.evo.eval.ttt;

import java.util.stream.IntStream;

import dave.evo.common.NeuralNet;

public class NeuralNetPlayer implements TicTacToePlayer
{
	private final NeuralNet mNN;
	
	public NeuralNetPlayer(NeuralNet nn)
	{
		mNN = nn;
	}

	@Override
	public int decide(TicTacToe game)
	{
		double[] board = IntStream.of(game.getField()).mapToDouble(i -> i).toArray();
		double[] play = mNN.apply(board);
		
		int t = -1; double v = 0;
		for(int i = 0 ; i < 9 ; ++i)
		{
			int x = i % 3, y = i / 3;
			
			if(game.get(x, y) != 0)
				continue;
			
			if(t == -1 || v < play[i])
			{
				t = i;
				v = play[i];
			}
		}
		
		return t;
	}
}
