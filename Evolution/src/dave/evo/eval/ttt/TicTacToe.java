package dave.evo.eval.ttt;

import java.util.stream.IntStream;

public class TicTacToe
{
	private final int[] mField;
	private int mTurn;
	
	public TicTacToe()
	{
		mField = new int[3 * 3];
		mTurn = 1;
	}
	
	public int[] getField() { return IntStream.of(mField).map(i -> i * mTurn).toArray(); }
	public int get(int x, int y) { return mField[x + y * 3]; }
	
	public boolean tie()
	{
		for(int i = 0 ; i < 9 ; ++i)
		{
			if(mField[i] == 0)
				return false;
		}
		
		return true;
	}
	
	public boolean set(int x, int y)
	{
		mField[x + y * 3] = mTurn;
		
		mTurn *= -1;
		
		for(int i = 0 ; i < 3 ; ++i)
		{
			int t = get(i, 0);
			
			if(t != 0 && t == get(i, 1) && t == get(i, 2))
				return true;
			
			t = get(0, i);

			if(t != 0 && t == get(1, i) && t == get(2, i))
				return true;
		}
		
		int t = get(1, 1);
		
		if(t == 0)
			return false;
		
		if(get(0, 0) == t && t == get(2, 2))
			return true;
		
		if(get(2, 0) == t && t == get(0, 2))
			return true;
		
		return false;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		
		for(int row = 0 ; row < 3 ; ++row)
		{
			if(row > 0)
				sb.append("\n---+---+---\n");
			
			for(int col = 0 ; col < 3 ; ++col)
			{
				if(col > 0)
					sb.append(" |");
				
				int v = get(col, row);
				
				sb.append(' ').append(v == 0 ? ' ' : (v == 1 ? 'X' : 'O'));
			}
		}
		
		return sb.toString();
	}
}
