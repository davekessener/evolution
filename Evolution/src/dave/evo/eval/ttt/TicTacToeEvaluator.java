package dave.evo.eval.ttt;

import java.util.function.BiFunction;

import dave.evo.common.NeuralNet;
import dave.evo.eval.MatchResult;
import dave.util.RNG;

public class TicTacToeEvaluator implements BiFunction<NeuralNet, NeuralNet, MatchResult>
{
	private final RNG mRandom;
	private final TicTacToePlayer mRandomPlayer;
	
	public TicTacToeEvaluator(RNG rng)
	{
		mRandom = rng;
		mRandomPlayer = new RandomPlayer(mRandom);
	}
	
	@Override
	public MatchResult apply(NeuralNet t, NeuralNet u)
	{
		TicTacToePlayer p1 = new NeuralNetPlayer(t);
		TicTacToePlayer p2 = new NeuralNetPlayer(u);
		
		MatchResult r1 = evaluate(p1, p2);
		MatchResult r2 = evaluate(p2, p1).reverse();
		MatchResult r3 = evaluate(p1, mRandomPlayer).maskSecond();
		MatchResult r4 = evaluate(mRandomPlayer, p1).reverse().maskSecond();
		
		return MatchResult.average(r1, r2, r3, r4);
	}
	
	private MatchResult evaluate(TicTacToePlayer p1, TicTacToePlayer p2)
	{
		TicTacToe game = new TicTacToe();
		
		while(!game.tie())
		{
			double r = 0;
			
			if((r = turn(p1, game)) != 0)
				return new MatchResult(r, 0);
			
			if(game.tie())
				break;
			
			if((r = turn(p2, game)) != 0)
				return new MatchResult(0, r);
		}
		
		return new MatchResult(0.5, 0.5);
	}
	
	private double turn(TicTacToePlayer p, TicTacToe game)
	{
		int i = p.decide(game);
		int x = i % 3, y = i / 3;
		
		if(game.get(x, y) != 0)
			return -1;
		
		if(game.set(x, y))
			return 1;
		
		return 0;
	}
}
