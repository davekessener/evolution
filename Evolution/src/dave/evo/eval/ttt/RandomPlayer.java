package dave.evo.eval.ttt;

import dave.util.RNG;

public class RandomPlayer implements TicTacToePlayer
{
	private final RNG mRandom;
	
	public RandomPlayer(RNG rng)
	{
		mRandom = rng;
	}

	@Override
	public int decide(TicTacToe game)
	{
		if(game.tie())
			throw new IllegalStateException();
		
		while(true)
		{
			int i = ((int) (9 * mRandom.nextDouble())) % 9;
			int x = i % 3, y = i / 3;
			
			if(game.get(x, y) == 0)
				return i;
		}
	}
}
