package dave.evo.eval.ttt;

public interface TicTacToePlayer
{
	public abstract int decide(TicTacToe game);
}
