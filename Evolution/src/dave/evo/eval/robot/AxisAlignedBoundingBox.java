package dave.evo.eval.robot;

import dave.util.math.Vec2;

public class AxisAlignedBoundingBox
{
	private final Vec2 mMin, mMax;
	
	protected AxisAlignedBoundingBox(Vec2 a, Vec2 b)
	{
		mMin = new Vec2(Math.min(a.x, b.x), Math.min(a.y, b.y));
		mMax = new Vec2(Math.max(a.x, b.x), Math.max(a.y, b.y));
	}
	
	public Vec2 min() { return mMin; }
	public Vec2 max() { return mMax; }
	public double width() { return mMax.x - mMin.x; }
	public double height() { return mMax.y - mMin.y; }
	
	public boolean includes(AxisAlignedBoundingBox aabb)
	{
		return includes(aabb.mMin) && includes(aabb.mMax);
	}
	
	public boolean includes(Vec2 p)
	{
		return mMin.x <= p.x && p.x <= mMax.x && mMin.y <= p.y && p.y <= mMax.y;
	}
	
	public boolean intersects(AxisAlignedBoundingBox aabb)
	{
		return intersection(aabb) != null;
	}
	
	public AxisAlignedBoundingBox intersection(AxisAlignedBoundingBox aabb)
	{
		double x0 = Math.max(mMin.x, aabb.mMin.x);
		double y0 = Math.max(mMin.y, aabb.mMin.y);
		double x1 = Math.min(mMax.x, aabb.mMax.x);
		double y1 = Math.min(mMax.y, aabb.mMax.y);
		
		if(x0 >= x1 || y0 >= y1)
			return null;
		
		return new AxisAlignedBoundingBox(new Vec2(x0, y0), new Vec2(x1, y1));
	}
	
	public double intersects(Vec2 p, Vec2 v)
	{
		double min = Double.NEGATIVE_INFINITY, max = Double.POSITIVE_INFINITY;
		
		for(int i = 0 ; i < 2 ; ++i)
		{
			double px = p.get(i);
			double vx = v.get(i);
			double x0 = mMin.get(i);
			double x1 = mMax.get(i);
			
			if(vx == 0)
			{
				if(px < x0 || px > x1)
				{
					return -1;
				}
			}
			else
			{
				if(vx < 0)
				{
					double t = x0; x0 = x1; x1 = t;
				}
				
				double t0 = (x0 - px) / vx;
				double t1 = (x1 - px) / vx;
				
				min = Math.max(min, t0);
				max = Math.min(max, t1);
				
				if(min >= max)
				{
					return -1;
				}
			}
		}
		
		if(min < 0 || max < 0)
			return -1;
		
		return min;
	}
	
	public static AxisAlignedBoundingBox fromCoords(Vec2 a, Vec2 b)
	{
		return new AxisAlignedBoundingBox(a, b);
	}
	
	public static AxisAlignedBoundingBox fromSpan(Vec2 p, Vec2 d)
	{
		return new AxisAlignedBoundingBox(p, p.add(d));
	}
}
