package dave.evo.eval.robot;

import java.util.function.Function;

import dave.util.RNG;

public class MazeWorldGenerator implements Function<RobotController, World>
{
	private final RNG mRandom;
	private final SimulationOptions mOptions;
	
	public MazeWorldGenerator(RNG rng, SimulationOptions o)
	{
		mRandom = rng;
		mOptions = o;
	}
	
	@Override
	public World apply(RobotController ai)
	{
		return new MazeWorld(ai, mOptions.world_size(), mOptions.world_size(), mRandom, mOptions);
	}
}
