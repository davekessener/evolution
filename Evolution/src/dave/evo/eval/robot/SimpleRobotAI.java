package dave.evo.eval.robot;

public class SimpleRobotAI extends RobotControllerBase
{
	@Override
	public void update()
	{
		double[] distances = distances();
		double forward = 0, left = 0, right = 0;
		
		switch(getSensor())
		{
			case SENSOR_FRONT:
				forward = 1;
				left += (1 - distances[3]) * 0.2;
				right += (1 - distances[1]) * 0.2;
				break;
				
			case SENSOR_BACK:
				right = 1;
				break;
				
			case SENSOR_RIGHT:
				right = 0.5;
				break;
				
			case SENSOR_LEFT:
				left = 0.5;
				break;
				
			default:
				forward = 1;
				left += ((1 - distances[4]) + 0.5 * (1 - distances[3])) * 0.05;
				right += ((1 - distances[0]) + 0.5 * (1 - distances[1])) * 0.05;
				left *= 0.5 + (1 - distances[2]);
				right *= 0.5 + (1 - distances[2]);
				break;
		}

		setForward(forward);
		setLeft(left);
		setRight(right);
	}
	
	private int getSensor()
	{
		int sensor = -1;
		
		for(int i = 0 ; i < sensors().length ; ++i)
		{
			if(sensors()[i] > 0.5)
			{
				if(sensor != -1)
					throw new IllegalStateException();
				
				sensor = i;
			}
		}

		return sensor;
	}
}
