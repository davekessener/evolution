package dave.evo.eval.robot;

import dave.util.MathUtils;
import dave.util.math.Matrix;
import dave.util.math.Vec2;
import dave.util.property.Property;
import dave.util.property.SimpleProperty;

public abstract class World
{
	private final Entity mRobot;
	private final Terrain mTerrain;
	private final SimulationOptions mOptions;
	
	protected World(RobotController ai, Vec2 p, Terrain t, SimulationOptions o)
	{
		mRobot = new Entity(ai, p, o.radius());
		mOptions = o;
		mTerrain = t;
		
		if(mTerrain.clip(p, mRobot.radius))
			throw new IllegalArgumentException();
	}
	
	public Entity robot( ) { return mRobot; }
	public Terrain terrain( ) { return mTerrain; }
	public SimulationOptions options( ) { return mOptions; }
	
	public void update()
	{
		updateInputs();
		
		mRobot.ai.update();
		
		handlePhysics();
	}
	
	public abstract boolean done( );
	public abstract double fitness( );
	
	protected void updateInputs()
	{
		for(int i = 0 ; i < 5 ; ++i)
		{
			double d = mTerrain.distanceTo(mRobot.position.get(), MathUtils.rotate2D(mRobot.forward.get(), VIEW_ANGLE * (i - 2)));
			
			d = Math.max(0, Math.min((d - mRobot.radius) / mOptions.view_distance(), 1));
			
			mRobot.ai.setDistance(i, d);
		}
		
		mRobot.ai.setSensor(-1);
	}
	
	protected void handlePhysics()
	{
		double view_angle = (mOptions.field_of_view() * Math.PI / 180) * (mRobot.ai.right() - mRobot.ai.left());
		Vec2 forward = MathUtils.rotation2D(view_angle).mul(Matrix.from(mRobot.forward.get())).toVec2();
		
		mRobot.forward.set(forward);
		
		Vec2 position = mRobot.position.get().add(forward.scale(mRobot.ai.forward() * mOptions.movement_speed()));
		
		if(!mTerrain.clip(position, mRobot.radius))
		{
			mRobot.position.set(position);
		}
	}
	
	public static class Entity
	{
		public final RobotController ai;
		public final Property<Vec2> position, forward;
		public final double radius;
		
		public Entity(RobotController ai, Vec2 position, double radius)
		{
			this.ai = ai;
			this.radius = radius;
			this.position = new SimpleProperty<>(position);
			this.forward = new SimpleProperty<>(new Vec2(0, 1));
		}
	}

	private static final double VIEW_ANGLE = Math.PI / 4;
}
