package dave.evo.eval.robot;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import dave.util.math.Vec2;

public class Terrain
{
	private final AxisAlignedBoundingBox mBounds;
	private final List<AxisAlignedBoundingBox> mBlocks;
	
	public Terrain(double w, double h)
	{
		if(w <= 0 || h <= 0)
			throw new IllegalArgumentException();
		
		mBounds = AxisAlignedBoundingBox.fromSpan(Vec2.ORIGIN, new Vec2(w, h));
		mBlocks = new ArrayList<>();
	}

	public double width() { return mBounds.width(); }
	public double height() { return mBounds.height(); }
	
	public Stream<AxisAlignedBoundingBox> blocks() { return mBlocks.stream(); }
	
	public void addBlock(AxisAlignedBoundingBox aabb)
	{
		if(!mBounds.includes(aabb))
			throw new IllegalArgumentException();
		
		mBlocks.add(aabb);
	}
	
	public double distanceTo(Vec2 p, Vec2 d)
	{
		if(!mBounds.includes(p))
			throw new IllegalArgumentException();
		if(d.x == 0 && d.y == 0)
			throw new IllegalArgumentException();
		
		for(AxisAlignedBoundingBox aabb : mBlocks)
		{
			double t = aabb.intersects(p, d);
			
			if(t >= 0)
				return t;
		}
		
		double dx = -1, dy = -1;
		Vec2 min = mBounds.min(), max = mBounds.max();

		if(d.x != 0)
		{
			dx = ((d.x < 0 ? min.x : max.x) - p.x) / d.x;
		}

		if(d.y != 0)
		{
			dy = ((d.y < 0 ? min.y : max.y) - p.y) / d.y;
		}
		
		if(d.x == 0)
		{
			return dy;
		}
		
		if(d.y == 0)
		{
			return dx;
		}
		
		return Math.min(dx, dy);
	}
	
	public boolean clip(Vec2 p, double r)
	{
		Vec2 d = (new Vec2(r, r));
		AxisAlignedBoundingBox bb = AxisAlignedBoundingBox.fromCoords(p.sub(d), p.add(d));
		
		if(p.x < r || p.y < r || p.x > mBounds.width() - r || p.y > mBounds.height() - r)
			return true;
		
		double max = r * r * FACTOR;
		
		for(AxisAlignedBoundingBox aabb : mBlocks)
		{
			AxisAlignedBoundingBox i = aabb.intersection(bb);
			
			if(i != null)
			{
				if(i.width() * i.height() >= max)
					return true;
				
				double ih = r - i.height();
				double iw = r - i.width();
				
				if(ih < 0)
					continue;
				
				if(r * r < iw * iw + ih * ih)
					return true;
			}
		}
		
		return false;
	}
	
	private static final double SQRT_TWO = Math.sqrt(2);
	private static final double FACTOR = 1.5 - SQRT_TWO;
}
