package dave.evo.eval.robot;

import dave.util.config.Default;

public interface SimulationOptions
{
	@Default("0.5")
	public abstract double radius( );
	
	@Default("15")
	public abstract double field_of_view( );
	
	@Default("0.1")
	public abstract double movement_speed( );
	
	@Default("10")
	public abstract double view_distance( );
	
	@Default("10")
	public abstract double world_size( );
	
	@Default("4")
	public abstract int food_count( );
	
	@Default("1000")
	public abstract int simulation_ticks( );
	
	@Default("25")
	public abstract int simulation_turns( );
	
	@Default("0.0")
	public abstract double time_scale( );
}
