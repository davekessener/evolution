package dave.evo.eval.robot;

public interface RobotController extends Robot
{
	public abstract void update( );
	
	public abstract void setDistance(int i, double v);
	public abstract void setSensor(int i);
}
