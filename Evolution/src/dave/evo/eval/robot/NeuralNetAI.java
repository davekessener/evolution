package dave.evo.eval.robot;

import dave.evo.common.NeuralNet;

public class NeuralNetAI extends RobotControllerBase
{
	private final NeuralNet mNN;
	
	public NeuralNetAI(NeuralNet nn)
	{
		mNN = nn;
		
		if(nn.inputs() != 9 || nn.outputs() != 3)
			throw new IllegalArgumentException();
		
		mNN.reset();
	}
	
	@Override
	public void update()
	{
		double[] in = new double[4 + 5];
		double[] sensors = sensors();
		double[] distances = distances();

		in[0] = sensors[SENSOR_FRONT];
		in[1] = sensors[SENSOR_RIGHT];
		in[2] = sensors[SENSOR_BACK];
		in[3] = sensors[SENSOR_LEFT];
		
		for(int i = 0 ; i < 5 ; ++i)
		{
			in[4 + i] = distances[i];
		}
		
		double[] out = mNN.apply(in);

		setLeft(out[0]);
		setForward(out[1]);
		setRight(out[2]);
	}
}
