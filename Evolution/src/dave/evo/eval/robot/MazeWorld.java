package dave.evo.eval.robot;

import dave.util.RNG;
import dave.util.math.Vec2;

public class MazeWorld extends World
{
	private final Vec2 mGoal;
	private final double mScale;
	
	public MazeWorld(RobotController ai, double w, double h, RNG rng, SimulationOptions o)
	{
		super(ai, new Vec2(o.radius() * 1.5, o.radius() * 1.5), new Terrain(w, h), o);
		
		mGoal = new Vec2(w - o.radius() * 1.5, h - o.radius() * 1.5);
		mScale = Math.sqrt(w * w + h * h);
		
		double r = o.radius() * 3;
		
		if(w < 4 * r || h < 5 * r)
			throw new IllegalArgumentException(String.format("%.3f x %.3f doesn't fit %.3f!", w, h, r));
		
		double border = 3 * r;
		double crossover = border + rng.nextDouble() * (h - 2 * border);
		
		terrain().addBlock(AxisAlignedBoundingBox.fromCoords(new Vec2(r, 0), new Vec2(w, crossover - r/2)));
		terrain().addBlock(AxisAlignedBoundingBox.fromCoords(new Vec2(0, crossover + r/2), new Vec2(w - r, h)));
	}

	@Override
	public boolean done()
	{
		return distance() <= options().radius() * 2;
	}

	@Override
	public double fitness()
	{
		return 1 - Math.max(0, distance() - 2 * options().radius()) / mScale;
	}
	
	private double distance()
	{
		return mGoal.sub(robot().position.get()).length();
	}
}
