package dave.evo.eval.robot;

public abstract class RobotControllerBase implements RobotController
{
	private final double[] mDistances, mSensors;
	private final double[] mDirections;
	
	protected RobotControllerBase()
	{
		mDistances = new double[5];
		mSensors = new double[4];
		mDirections = new double[3];
	}
	
	@Override
	public double forward()
	{
		return mDirections[0];
	}
	
	@Override
	public double left()
	{
		return mDirections[1];
	}
	
	@Override
	public double right()
	{
		return mDirections[2];
	}
	
	protected void setForward(double v) { mDirections[0] = v; }
	protected void setLeft(double v) { mDirections[1] = v; }
	protected void setRight(double v) { mDirections[2] = v; }
	public double[] sensors() { return mSensors; }
	public double[] distances() { return mDistances; }

	@Override
	public void setDistance(int i, double v)
	{
		if(v < 0 || v > 1)
			throw new IllegalArgumentException();
		
		mDistances[i] = v;
	}

	@Override
	public void setSensor(int i)
	{
		if(i < -1 || i >= mSensors.length)
			throw new IllegalArgumentException();
		
		for(int j = 0 ; j < mSensors.length ; ++j)
		{
			mSensors[j] = (i == j ? 1 : 0);
		}
	}

	public static final int SENSOR_FRONT = 0;
	public static final int SENSOR_RIGHT = 1;
	public static final int SENSOR_BACK = 2;
	public static final int SENSOR_LEFT = 3;
}
