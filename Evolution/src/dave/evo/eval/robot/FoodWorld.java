package dave.evo.eval.robot;

import java.util.ArrayDeque;
import java.util.List;
import java.util.Optional;
import java.util.Queue;

import dave.util.MathUtils;
import dave.util.math.Vec2;

public class FoodWorld extends World
{
	private final Queue<Vec2> mFood;
	private final int mSize;
	private int mActiveSensor;
	
	public FoodWorld(RobotController ai, double w, double h, List<Vec2> f, SimulationOptions o)
	{
		super(ai, new Vec2(w / 2, h / 2), new Terrain(w, h), o);
		
		mFood = new ArrayDeque<>();
		mSize = f.size();
		mActiveSensor = -1;
		
		f.forEach(mFood::add);
	}
	
	public Optional<Vec2> getCurrentFood() { return Optional.ofNullable(mFood.peek()); }
	public int getActiveSensor() { return mActiveSensor; }

	@Override
	public boolean done()
	{
		return mFood.isEmpty();
	}
	
	@Override
	public double fitness()
	{
		return (mSize - mFood.size()) / (double) mSize;
	}
	
	@Override
	protected void updateInputs()
	{
		super.updateInputs();
		
		Vec2 forward = robot().forward.get();
		Vec2 food = mFood.peek().sub(robot().position.get()).normalized();
		double a = MathUtils.angle(forward, food);
		
		if(a < -Math.PI || a > Math.PI)
			throw new IllegalStateException("" + (a * 180 / Math.PI));
		
		if(a < 0)
			a += 2 * Math.PI;
		
		a += 45 * Math.PI / 180;
		
		if(a >= 2 * Math.PI)
			a -= 2 * Math.PI;
		
		int i = (int) Math.floor(4 * (a / (2 * Math.PI)));
		
		robot().ai.setSensor(i);
		
		mActiveSensor = i;
	}
	
	@Override
	protected void handlePhysics()
	{
		super.handlePhysics();
		
		if(mFood.peek().sub(robot().position.get()).lengthSquared() <= robot().radius * robot().radius)
		{
			mFood.poll();
		}
	}
}
