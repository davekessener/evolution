package dave.evo.eval.robot;

import java.util.function.Function;
import java.util.function.ToDoubleFunction;

import dave.evo.common.NeuralNet;
import dave.util.Averager;
import dave.util.MathUtils;

public class RobotEvaluator implements ToDoubleFunction<NeuralNet>
{
	private final Function<RobotController, World>[] mLevelGen;
	private final SimulationOptions mOptions;
	
	@SafeVarargs
	public RobotEvaluator(SimulationOptions o, Function<RobotController, World> ... l)
	{
		mLevelGen = l;
		mOptions = o;
	}
	
	@Override
	public double applyAsDouble(NeuralNet nn)
	{
		Averager avg = new Averager();
		
		for(int k = 0 ; k < mLevelGen.length ; ++k)
		{
			for(int i = 0 ; i < (mOptions.simulation_turns() / mLevelGen.length) + 1 ; ++i)
			{
				World w = mLevelGen[k].apply(new NeuralNetAI(nn));
				int c;
				
				for(c = mOptions.simulation_ticks() ; c > 0 ; --c)
				{
					if(w.done())
						break;
					
					w.update();
				}
				
				double time = MathUtils.lerp(1, c / (double) mOptions.simulation_ticks(), mOptions.time_scale());
				
				avg.add(w.fitness() * time);
			}
		}
		
		return avg.get();
	}
}
