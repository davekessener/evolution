package dave.evo.eval.robot;

public interface Robot
{
	public abstract double forward( );
	public abstract double left( );
	public abstract double right( );
}
