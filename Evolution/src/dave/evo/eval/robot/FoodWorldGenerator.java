package dave.evo.eval.robot;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import dave.util.RNG;
import dave.util.math.Vec2;

public class FoodWorldGenerator implements Function<RobotController, World>
{
	private final RNG mRandom;
	private final SimulationOptions mOptions;
	
	public FoodWorldGenerator(RNG rng, SimulationOptions o)
	{
		mRandom = rng;
		mOptions = o;
	}
	
	@Override
	public World apply(RobotController ai)
	{
		double l = mOptions.world_size();
		double r = mOptions.radius();
		Vec2 p = new Vec2(l / 2, l / 2);
		List<Vec2> food = new ArrayList<>();
		
		for(int i = 0 ; i < mOptions.food_count() ; ++i)
		{
			double t = l - 2 * r;
			double d = (5 * r) * (5 * r);
			Vec2 v = null;
			
			while(true)
			{
				v = new Vec2(r + mRandom.nextDouble() * t, r + mRandom.nextDouble() * t);
				
				if(v.sub(p).lengthSquared() >= d || (i > 0 && v.sub(food.get(i - 1)).lengthSquared() >= d))
					break;
			}
			
			food.add(v);
		}
		
		return new FoodWorld(ai, l, l, food, mOptions);
	}
}
