package dave.evo.eval.maze;

import dave.evo.common.NeuralNet;

public class NeuralNetAI extends RunnerControllerBase
{
	private final NeuralNet mAI;
	
	public NeuralNetAI(NeuralNet ai)
	{
		mAI = ai;
		
		if(ai.inputs() != 3 || ai.outputs() != 3)
			throw new IllegalArgumentException();
		
		mAI.reset();
	}
	
	@Override
	public void update()
	{
		double[] out = mAI.evaluate((viewLeft() ? 1 : -1), (viewFront() ? 1 : -1), (viewRight() ? 1 : -1));
		
		setLeft(out[0]);
		setForward(out[1]);
		setRight(out[2]);
	}

	@Override
	public void reset()
	{
		mAI.reset();
	}
}
