package dave.evo.eval.maze;

import dave.util.config.Default;

public interface MazeOptions
{
	@Default("10")
	public abstract int maze_size( );
	
	@Default("25")
	public abstract int turns( );
}
