package dave.evo.eval.maze;

import dave.util.RNG;

public class RandomRunnerAI extends RunnerControllerBase
{
	private final RNG mRandom;
	
	public RandomRunnerAI(RNG rng)
	{
		mRandom = rng;
	}
	
	@Override
	public void update()
	{
		this.setForward(mRandom.nextDouble());
		this.setLeft(mRandom.nextDouble());
		this.setRight(mRandom.nextDouble());
	}
}
