package dave.evo.eval.maze;

public class SimpleRunnerAI extends RunnerControllerBase
{
	private State mState = State.FORWARD;
	
	@Override
	public void update()
	{
		switch(mState)
		{
			case FORWARD:
				if(viewFront() || !viewRight())
				{
					mState = State.RIGHT;
				}
				break;
				
			case RIGHT:
				if(viewFront())
				{
					mState = State.LEFT;
				}
				else
				{
					mState = State.FORWARD;
				}
				break;
				
			case LEFT:
				if(!viewFront())
				{
					mState = State.FORWARD;
				}
				break;
				
			default:
				throw new IllegalStateException();
		}
		
		setForward(mState == State.FORWARD ? 1 : 0);
		setLeft(mState == State.LEFT ? 1 : 0);
		setRight(mState == State.RIGHT ? 1 : 0);
	}

	@Override
	public void reset()
	{
		super.reset();
		
		mState = State.FORWARD;
	}
	
	private static enum State
	{
		FORWARD,
		RIGHT,
		LEFT
	}
}
