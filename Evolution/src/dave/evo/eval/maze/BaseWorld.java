package dave.evo.eval.maze;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Stream;

import dave.util.RNG;
import dave.util.math.IVec2;

public abstract class BaseWorld implements MazeWorld
{
	private final boolean[] mMaze;
	private final int mSize;
	
	public BaseWorld(int s, RNG rng)
	{
		mMaze = Maze.create(s, s, rng);
		mSize = s * 2 + 1;
	}
	
	public int size() { return mSize; }

	@Override
	public int width()
	{
		return mSize;
	}

	@Override
	public int height()
	{
		return mSize;
	}

	@Override
	public boolean wall(int x, int y)
	{
		return !((x < 0 || x >= mSize || y < 0 || y >= mSize) || mMaze[x + y * mSize]);
	}

	@Override
	public int distance(IVec2 p, IVec2 q)
	{
		if(wall(p) || wall(q))
			throw new IllegalArgumentException();
		
		Queue<Pos> track = new PriorityQueue<>(1, (p0, p1) -> Integer.compare(p0.distance, p1.distance) * 10 + 1);
		
		track.add(new Pos(p, IVec2.ORIGIN, 0));
		
		while(!track.isEmpty())
		{
			Pos here = track.poll();
			
			if(here.coords.equals(q))
				return here.distance;
			
			Stream.of(IVec2.SIDES)
				.filter(v -> !v.equals(here.from))
				.map(v -> here.coords.add(v))
				.filter(there -> !wall(there))
				.forEach(there -> track.add(new Pos(there, here.coords.sub(there), here.distance + 1)));
		}
		
		throw new IllegalStateException("No path!");
	}
	
	private static class Pos
	{
		public final IVec2 coords;
		public final IVec2 from;
		public final int distance;
		
		public Pos(IVec2 coords, IVec2 from, int distance)
		{
			this.coords = coords;
			this.from = from;
			this.distance = distance;
		}
	}
}
