package dave.evo.eval.maze;

import dave.util.math.IVec2;

public enum View
{
	UP(0, 1),
	RIGHT(1, 0),
	DOWN(0, -1),
	LEFT(-1, 0);
	
	public final int dx, dy;
	public final IVec2 v;
	
	public View left() { return View.values()[(ordinal() + VIEWS - 1) % VIEWS]; }
	public View right() { return View.values()[(ordinal() + 1) % VIEWS]; }
	
	public static View of(IVec2 p)
	{
		for(int i = 0 ; i < VIEWS ; ++i)
		{
			if(values()[i].v.equals(p))
				return values()[i];
		}
		
		return null;
	}
	
	private View(int dx, int dy)
	{
		this.dx = dx;
		this.dy = dy;
		this.v = new IVec2(dx, dy);
	}
	
	public static final int VIEWS = View.values().length;
}
