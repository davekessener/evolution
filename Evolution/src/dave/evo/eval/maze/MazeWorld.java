package dave.evo.eval.maze;

import dave.util.math.IVec2;

public interface MazeWorld
{
	public abstract int width( );
	public abstract int height( );
	public abstract boolean wall(int x, int y);
	public abstract int distance(IVec2 p, IVec2 q);
	
	public default boolean wall(IVec2 p) { return wall(p.x, p.y); }
}
