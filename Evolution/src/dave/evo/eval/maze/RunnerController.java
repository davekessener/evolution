package dave.evo.eval.maze;

public interface RunnerController extends Runner
{
	public abstract void update( );
	public abstract void reset( );

	public abstract void setView(boolean left, boolean front, boolean right);
}
