package dave.evo.eval.maze;

import java.util.function.ToDoubleFunction;

import dave.evo.common.NeuralNet;
import dave.util.Averager;
import dave.util.RNG;

public class MazeEvaluator implements ToDoubleFunction<NeuralNet>
{
	private final MazeOptions mOptions;
	private final RNG mRandom;
	
	public MazeEvaluator(MazeOptions o, RNG rng)
	{
		mOptions = o;
		mRandom = rng;
	}

	@Override
	public double applyAsDouble(NeuralNet nn)
	{
		return applyAsAI(new NeuralNetAI(nn));
	}
	
	public double applyAsAI(RunnerController ai)
	{
		Averager avg = new Averager();
		
		for(int i = 0 ; i < mOptions.turns() ; ++i)
		{
			World w = new World(mOptions.maze_size(), ai, mRandom);
			int fields = w.width() * w.height() / 2;
			
			for(int c = fields ; c > 0 ; --c)
			{
				if(w.done())
					break;
				
				if(w.tick())
				{
					c = fields;
				}
			}
			
			avg.add(w.fitness());
		}
		
		return avg.get();
	}
}
