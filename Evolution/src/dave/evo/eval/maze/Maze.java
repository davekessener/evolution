package dave.evo.eval.maze;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import dave.util.RNG;
import dave.util.math.IVec2;

public final class Maze
{
	private final boolean[] mMaze;
	private final int mWidth, mHeight;
	private final List<IVec2> mOpen;
	private final RNG mRandom;
	
	private Maze(int w, int h, RNG rng)
	{
		int aw = w * 2 + 1, ah = h * 2 + 1;
		
		mWidth = w;
		mHeight = h;
		mMaze = new boolean[aw * ah];
		mRandom = rng;
		mOpen = new ArrayList<>();
		
		Arrays.fill(mMaze, false);
	}
	
	private void generate()
	{
		IVec2 start = new IVec2(mWidth / 2, mHeight / 2);
		
		mMaze[idx(start.x * 2 + 1, start.y * 2 + 1)] = true;
		
		mOpen.add(start);
		
		while(!mOpen.isEmpty())
		{
			int i = (int) Math.floor(mRandom.nextDouble() * mOpen.size());
			
			visit(mOpen.remove(i));
		}
	}
	
	private void visit(IVec2 o)
	{
		Stream.of(DIRS).forEach(d -> {
			IVec2 p = o.add(d);
			
			if(valid(p))
			{
				int px = p.x * 2 + 1;
				int pz = p.y * 2 + 1;
				int i = idx(px, pz);
				
				if(!mMaze[i])
				{
					mMaze[idx(o.x + p.x + 1, o.y + p.y + 1)] = true;
					mMaze[i] = true;
					
					mOpen.add(p);
				}
			}
		});
	}
	
	private boolean valid(IVec2 p) { return valid(p.x, p.y); }
	private boolean valid(int x, int z) { return x >= 0 && z >= 0 && x < mWidth && z < mHeight; }
	
	private int idx(int x, int z) { return x + z * (mWidth * 2 + 1); }
	
	private static final IVec2[] DIRS = new IVec2[] { IVec2.LEFT, IVec2.RIGHT, IVec2.UP, IVec2.DOWN };
	
	public static boolean[] create(int w, int h, RNG rng)
	{
		Maze maze = new Maze(w, h, rng);
		
		maze.generate();
		
		return maze.mMaze;
	}
}
