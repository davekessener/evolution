package dave.evo.eval.maze;

public abstract class RunnerControllerBase implements RunnerController
{
	private double mForward, mLeft, mRight;
	private boolean[] mView;
	
	protected RunnerControllerBase()
	{
		mForward = mLeft = mRight = 0;
		mView = new boolean[3];
	}
	
	protected void setForward(double f) { mForward = f; }
	protected void setLeft(double f) { mLeft = f; }
	protected void setRight(double f) { mRight = f; }
	protected boolean viewLeft() { return mView[0]; }
	protected boolean viewFront() { return mView[1]; }
	protected boolean viewRight() { return mView[2]; }
	
	@Override
	public double forward()
	{
		return mForward;
	}

	@Override
	public double left()
	{
		return mLeft;
	}

	@Override
	public double right()
	{
		return mRight;
	}

	@Override
	public void setView(boolean left, boolean front, boolean right)
	{
		mView[0] = left;
		mView[1] = front;
		mView[2] = right;
	}
	
	@Override
	public void reset()
	{
		mView[0] = mView[1] = mView[2] = false;
		mForward = mLeft = mRight = 0;
	}
}
