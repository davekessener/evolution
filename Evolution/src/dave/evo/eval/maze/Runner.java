package dave.evo.eval.maze;

public interface Runner
{
	public abstract double forward( );
	public abstract double left( );
	public abstract double right( );
}
