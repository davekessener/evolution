package dave.evo.eval.maze;

import java.util.HashSet;
import java.util.Set;

import dave.util.RNG;
import dave.util.math.IVec2;

public class World extends BaseWorld
{
	private final RunnerController mAI;
	private final int[] mDistances;
	private final IVec2 mGoal;
	private final Set<IVec2> mVisited;
	private final int mDistance;
	private int mX, mY;
	private View mView;
	private int mNearest;
	
	public World(int size, RunnerController ai, RNG rng)
	{
		super(size, rng);
		
		mAI = ai;
		
		mAI.reset();
		
		int v = (rng.nextInt() & 0xFF);
		int m = (size() - 2);
		
		mView = View.values()[v & 3];
		mX = ((v & 0x4) == 0 ? 1 : m);
		mY = ((v & 0x8) == 0 ? 1 : m);
		mGoal = new IVec2(((v & 0x4) != 0 ? 1 : m), ((v & 0x8) != 0 ? 1 : m));
		
		mVisited = new HashSet<>();
		mDistance = distance(position(), mGoal);
		mNearest = mDistance;
		
		mVisited.add(new IVec2(mX, mY));
		
		mDistances = new int[width() * height()];
		
		for(int i = 0 ; i < mDistances.length ; ++i)
		{
			mDistances[i] = Integer.MAX_VALUE;
		}
		
		fillInDistances(mGoal, 0);
	}
	
	public IVec2 position() { return new IVec2(mX, mY); }
	public IVec2 goal() { return mGoal; }
	public View view() { return mView; }
	
	public boolean done()
	{
		return mX == mGoal.x && mY == mGoal.y;
	}
	
	public double fitness()
	{
		double nearest = (mNearest / (double) mDistance);
		double now = (mDistances[mX + mY * width()] / (double) mDistance);
		
		nearest = 1 - Math.max(0, Math.min(nearest, 1));
		now = 1 - Math.max(0, Math.min(now, 1));
		
		return (nearest + now) / 2;
	}
	
	public boolean tick()
	{
		int nx = mX + mView.dx, ny = mY + mView.dy;
		
		mAI.setView(wallLookingAt(mView.left()), wallLookingAt(mView), wallLookingAt(mView.right()));
		
		mAI.update();
		
		if(mAI.forward() > 0.5)
		{
			if(!wall(nx, ny))
			{
				mX = nx;
				mY = ny;
			}
		}
		else
		{
			double d = (mAI.right() - mAI.left());
			
			if(d < -0.5)
			{
				mView = mView.left();
			}
			else if(d > 0.5)
			{
				mView = mView.right();
			}
		}
		
		mNearest = Math.min(mNearest, mDistances[mX + mY * width()]);
		
		return mVisited.add(new IVec2(mX, mY));
	}
	
	private boolean wallLookingAt(View v) { return wall(mX + v.dx, mY + v.dy); }
	
	private void fillInDistances(IVec2 p, int d)
	{
		if(wall(p))
			return;
		
		int i = p.x + p.y * width();
		
		if(mDistances[i] <= d)
			return;
		
		mDistances[i] = d;
		
		fillInDistances(new IVec2(p.x + 1, p.y), d + 1);
		fillInDistances(new IVec2(p.x - 1, p.y), d + 1);
		fillInDistances(new IVec2(p.x, p.y + 1), d + 1);
		fillInDistances(new IVec2(p.x, p.y - 1), d + 1);
	}
}
