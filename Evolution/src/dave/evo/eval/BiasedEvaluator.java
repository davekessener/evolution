package dave.evo.eval;

import java.util.function.ToDoubleFunction;

import dave.evo.common.NeuralNet;
import dave.util.Averager;
import dave.util.MathUtils;
import dave.util.log.Logger;

public abstract class BiasedEvaluator<C> implements ToDoubleFunction<NeuralNet>
{
	private final int mTurns;
	private final double mBiasValue;
	private double mBias;
	
	protected BiasedEvaluator(double bias, int turns)
	{
		mTurns = turns;
		mBiasValue = bias;
		mBias = -1;
	}

	public abstract double evaluate(C nn);
	
	@Override
	public double applyAsDouble(NeuralNet nn)
	{
		if(mBias == -1)
		{
			Averager avg = new Averager();
			
			for(int i = 0 ; i < 1000 ; ++i)
			{
				avg.add(computeBias());
			}
			
			mBias = avg.get();
			
			LOG.info("Evaluator '%s' computed a bias of %.3f", getClass().getSimpleName(), mBias);
		}
		
		Averager avg = new Averager();
		
		for(int i = 0 ; i < mTurns ; ++i)
		{
			avg.add(evaluate(wrap(nn)));
		}
		
		return MathUtils.clamp(computeResult(avg.get()));
	}
	
	private double computeResult(double f)
	{
		if(mBiasValue > 0)
		{
			if(f <= mBias)
			{
				return MathUtils.lerp(0, mBiasValue, f / mBias);
			}
			else
			{
				return MathUtils.lerp(mBiasValue, 1, (f - mBias) / (1 - mBias));
			}
		}
		else
		{
			return f;
		}
	}
	
	protected abstract C wrap(NeuralNet nn);
	protected abstract double computeBias( );
	
	protected static final Logger LOG = Logger.get("eval");
}
