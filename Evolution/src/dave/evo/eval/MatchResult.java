package dave.evo.eval;

public class MatchResult
{
	public final double first, second;
	
	public MatchResult(double first, double second)
	{
		this.first = first;
		this.second = second;
	}
	
	public MatchResult reverse()
	{
		return new MatchResult(second, first);
	}
	
	public MatchResult maskSecond()
	{
		return new MatchResult(first, 0);
	}
	
	public static MatchResult average(MatchResult ... r)
	{
		double a = 0, b = 0;
		
		for(int i = 0 ; i < r.length ; ++i)
		{
			a += r[i].first;
			b += r[i].second;
		}
		
		return new MatchResult(a / r.length, b / r.length);
	}
}
