package dave.evo.eval.seq;

import java.util.LinkedList;
import java.util.Queue;

public class SimpleSequencer extends BaseSequencer
{
	private final Queue<Double> mMemory = new LinkedList<>();
	
	@Override
	public void update()
	{
		double o = 0;
		
		if(getEnabled() < 0)
		{
			mMemory.add(getSignal());
		}
		else if(!mMemory.isEmpty())
		{
			o = mMemory.poll();
		}
		
		setDecision(o);
	}

	@Override
	public void reset()
	{
		mMemory.clear();
	}
}
