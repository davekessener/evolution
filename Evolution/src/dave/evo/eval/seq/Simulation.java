package dave.evo.eval.seq;

import dave.util.Averager;
import dave.util.MathUtils;
import dave.util.RNG;

public final class Simulation
{
	public static double evaluate(int l, Sequencer ai, RNG rng)
	{
		Averager r = new Averager();
		double[] expected = new double[l];
		
		for(int i = 0 ; i < l ; ++i)
		{
			expected[i] = ((rng.nextInt() & 1) == 0 ? 1 : -1);
			
			ai.enable(-1);
			ai.signal(expected[i]);
			
			ai.update();
			
			r.add(d(0, ai.leftright()));
		}
		
		double phase_1 = 1 - r.get();
		double phase_2 = 0;
		
		for(int i = 0 ; i < l ; ++i)
		{
			ai.enable(1);
			ai.signal(0);
			
			ai.update();
			
			double d = d(expected[l - i - 1], ai.leftright());
			
			phase_2 += (1 - d);
			
			if(d > 0.5)
				break;
		}
		
		phase_2 /= l;
		
		return MathUtils.lerp(phase_1, phase_2, 0.8);
	}
	
	private static double d(double v0, double v1)
	{
		return MathUtils.clamp(0.5 * Math.abs(v0 - v1));
	}
	
	private Simulation( ) { }
}
