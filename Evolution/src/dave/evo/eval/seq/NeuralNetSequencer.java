package dave.evo.eval.seq;

import dave.evo.common.NeuralNet;

public class NeuralNetSequencer extends BaseSequencer
{
	private final NeuralNet mAI;
	
	public NeuralNetSequencer(NeuralNet nn)
	{
		if(nn.inputs() != 2 || nn.outputs() != 1)
			throw new IllegalArgumentException();
		
		mAI = nn;
		
		mAI.reset();
	}

	@Override
	public void update()
	{
		double[] r = mAI.evaluate(getSignal(), getEnabled());
		
		setDecision(to_tanh(r[0]));
	}
	
	private static double to_tanh(double v) { return v * 2 - 1; }

	@Override
	public void reset()
	{
		mAI.reset();
	}
}
