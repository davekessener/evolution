package dave.evo.eval.seq;

public interface Sequencer
{
	public abstract double leftright( );
	
	public abstract void update( );
	
	public abstract void signal(double s);
	public abstract void enable(double v);
	
	public abstract void reset( );
}
