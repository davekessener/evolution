package dave.evo.eval.seq;

import dave.util.RNG;

public class RandomSequencer extends BaseSequencer
{
	private final RNG mRandom;
	
	public RandomSequencer(RNG rng)
	{
		mRandom = rng;
	}

	@Override
	public void update()
	{
		setDecision(mRandom.nextDouble() * 2 - 1);
	}

	@Override
	public void reset()
	{
		setDecision(0);
	}
}
