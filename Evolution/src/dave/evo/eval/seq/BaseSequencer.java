package dave.evo.eval.seq;

public abstract class BaseSequencer implements Sequencer
{
	private double mInput, mOutput, mEnabled;
	
	protected BaseSequencer()
	{
		mInput = mOutput = mEnabled = 0;
	}
	
	protected void setDecision(double v) { mOutput = v; }
	protected double getSignal() { return mInput; }
	protected double getEnabled() { return mEnabled; }

	@Override
	public double leftright()
	{
		return mOutput;
	}

	@Override
	public void signal(double s)
	{
		mInput = s;
	}

	@Override
	public void enable(double v)
	{
		mEnabled = v;
	}
}
