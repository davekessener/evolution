package dave.evo.eval.seq;

import dave.evo.common.NeuralNet;
import dave.evo.config.EvaluationOptions;
import dave.evo.eval.BiasedEvaluator;
import dave.util.Averager;
import dave.util.RNG;

public class SequenceEvaluator extends BiasedEvaluator<Sequencer>
{
	private final SequenceOptions mOptions;
	private final RNG mRandom;
	
	public SequenceEvaluator(SequenceOptions o, RNG rng, EvaluationOptions eo)
	{
		super(eo.fitness_bias(), o.turns());
		
		mOptions = o;
		mRandom = rng;
	}
	
	@Override
	public double evaluate(Sequencer ai)
	{
		Averager avg = new Averager();
		int from = mOptions.min_length(), to = mOptions.max_length();
		
		for(int j = from ; j <= to ; ++j)
		{
			ai.reset();
			
			avg.add(Simulation.evaluate(j, ai, mRandom));
		}
		
		return avg.get();
	}

	@Override
	protected Sequencer wrap(NeuralNet nn)
	{
		return new NeuralNetSequencer(nn);
	}

	@Override
	protected double computeBias()
	{
		return evaluate(new RandomSequencer(mRandom));
	}
}
