package dave.evo.eval.seq;

import dave.util.config.Default;

public interface SequenceOptions
{
	@Default("2")
	public abstract int min_length( );
	
	@Default("6")
	public abstract int max_length( );
	
	@Default("25")
	public abstract int turns( );
}
