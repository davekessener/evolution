package dave.evo.test;

import static org.junit.Assert.*;

import org.junit.Test;

import dave.evo.eval.robot.Terrain;
import dave.util.math.Vec2;

public class RobotUT
{
	@Test
	public void testBounds()
	{
		Terrain t = new Terrain(10, 10);
		Vec2 p = new Vec2(5, 5);

		assertEquals(5, t.distanceTo(p, new Vec2(1, 0)), 0.0001);
		assertEquals(5, t.distanceTo(p, new Vec2(0, 1)), 0.0001);
		assertEquals(5, t.distanceTo(p, new Vec2(0, -1)), 0.0001);
		assertEquals(Math.sqrt(50) / Math.sqrt(2), t.distanceTo(p, new Vec2(-1, -1)), 0.0001);
	}
}
