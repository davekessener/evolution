package dave.evo.test;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import org.junit.Test;

import dave.evo.dna.BiasGene;
import dave.evo.dna.GeneSequence;
import dave.evo.dna.Gene;
import dave.evo.dna.HiddenGene;
import dave.evo.dna.InputGene;
import dave.evo.dna.LinkGene;
import dave.evo.dna.OutputGene;
import dave.evo.nn.BasicNeuralNet;
import dave.json.JSON;
import dave.evo.common.NeuralNet;

public class GenomeUT extends BaseUT
{
	@Test
	public void testXOR()
	{
		test(build());
	}
	
	@Test
	public void testSer()
	{
		test((NeuralNet) JSON.deserialize(JSON.serialize(build())));
	}
	
	@Test
	public void testOrder()
	{
		List<Gene> genes = genome();
		
		for(int i = 0 ; i < 100 ; ++i)
		{
			Collections.shuffle(genes);
			
			test(build(genes));
		}
	}
	
	private static void test(NeuralNet nn)
	{
		Stream.of(
				new double[] { 0, 0, 0 },
				new double[] { 0, 1, 1 },
				new double[] { 1, 0, 1 },
				new double[] { 1, 1, 0 }
			).forEach(i -> {
				double[] o = nn.evaluate(i[0], i[1]);
				
				assertEquals(o.length, 1);
				assertEquals(o[0], i[2], 0.01);
			});
	}
	
	private static NeuralNet build() { return build(genome()); }
	private static NeuralNet build(List<Gene> genes)
	{
		GeneSequence g = new GeneSequence(genes);
			
		return BasicNeuralNet.build(g);
	}
	
	private static List<Gene> genome()
	{
		return Arrays.asList(
			new BiasGene(0),
			new InputGene(1),
			new InputGene(2),
			new OutputGene("SIGMOID", 3),
			new HiddenGene("SIGMOID", 4),
			new HiddenGene("SIGMOID", 5),
			new LinkGene(6, 1, 4, 10.0, true),
			new LinkGene(7, 2, 4, -10.0, true),
			new LinkGene(8, 0, 4, -5.0, true),
			new LinkGene(9, 1, 5, -10.0, true),
			new LinkGene(10, 2, 5, 10.0, true),
			new LinkGene(11, 0, 5, -5.0, true),
			new LinkGene(12, 4, 3, 10.0, true),
			new LinkGene(13, 5, 3, 10.0, true),
			new LinkGene(14, 0, 3, -5.0, true)
		);
	}
}
