package dave.evo.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.junit.Test;

import dave.evo.common.GeneHistory;
import dave.evo.dna.BiasGene;
import dave.evo.dna.Gene;
import dave.evo.dna.GeneSequence;
import dave.evo.dna.HiddenGene;
import dave.evo.dna.InputGene;
import dave.evo.dna.LinkGene;
import dave.evo.dna.OutputGene;
import dave.evo.nn.BasicNeuralNet;
import dave.evo.common.NeuralNet;
import dave.evo.sys.app.functional.mutate.SplitLinkMutator;
import dave.util.RNG;
import dave.util.XoRoRNG;

public class MutationUT
{
	@Test
	public void test1()
	{
		Container c = new Container("ABS");
		NeuralNet nn = BasicNeuralNet.build(c.genome());
		
		for(int i = -100 ; i < 100 ; ++i)
		{
			double v = i / 10.0;
			double[] r = nn.apply(new double[] { v });
			
			assertEquals(Math.abs(5 * v + 3), r[0], 0.0001);
		}
	}
	
	@Test
	public void test2()
	{
		RNG rng = new XoRoRNG();
		Container c = new Container("SIGMOID");
		Function<GeneSequence, GeneSequence> split = new SplitLinkMutator(c.history, id -> new HiddenGene("SIGMOID", id), rng);
		GeneSequence g = split.apply(c.genome());
		
		assertEquals(g.genes().count(), 3 + 2 + 4);
	}
	
	@Test
	public void test3()
	{
		RNG rng = new XoRoRNG();
		Container c = new Container("SIGMOID");
		Function<GeneSequence, GeneSequence> split = new SplitLinkMutator(c.history, id -> new HiddenGene("SIGMOID", id), rng);
		GeneSequence g = c.genome();
		
		for(int i = 0 ; i < 100 ; ++i)
		{
			g = split.apply(g);
		}
		
		assertEquals(g.genes().count(), 3 + 2 + 4 * 100);
	}
	
	private static class Container
	{
		public final GeneHistory history;
		public final List<Gene> genes;
		
		public Container(String a)
		{
			history = new GeneHistory();
			genes = new ArrayList<>();
			
			genes.add(new BiasGene(0));
			genes.add(new InputGene(history.nextID()));
			genes.add(new OutputGene(a, history.nextID()));
			
			link(0, 2, 3);
			link(1, 2, 5);
		}
		
		public void link(long from, long to, double w)
		{
			genes.add(new LinkGene(history.link(from, to), from, to, w, true));
		}
		
		public GeneSequence genome()
		{
			return new GeneSequence(genes);
		}
	}
}
