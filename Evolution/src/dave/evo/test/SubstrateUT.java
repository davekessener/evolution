package dave.evo.test;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import dave.evo.common.NetworkInterface;
import dave.evo.common.NeuralNet;
import dave.evo.common.Options;
import dave.evo.config.EvaluationOptions;
import dave.evo.dna.GeneSequence;
import dave.evo.eval.seq.SequenceEvaluator;
import dave.evo.eval.seq.SequenceOptions;
import dave.evo.nn.Activation;
import dave.evo.nn.BasicNeuralNet;
import dave.evo.nn.FlatNeuralNet;
import dave.evo.nn.es.Link;
import dave.evo.nn.es.QuadMap;
import dave.evo.nn.es.SubstrateGenerator;
import dave.evo.nn.hyper.CPPNN;
import dave.evo.nn.hyper.Substrate;
import dave.evo.sys.app.script.SimpleMazeGoal;
import dave.evo.util.EvoUtils;
import dave.evo.util.FXUtils;
import dave.evo.util.HypercubeVisualizer;
import dave.json.JSON;
import dave.json.JsonObject;
import dave.json.JsonUtils;
import dave.util.Utils;
import dave.util.XoRoRNG;
import dave.util.config.Configuration;
import dave.util.image.Canvas;
import dave.util.image.Colors;
import dave.util.image.SimpleGraphics;
import dave.util.log.Logger;
import dave.util.math.Matrix;
import dave.util.math.Vec2;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

@SuppressWarnings({"unused"})
public class SubstrateUT extends BaseUT
{
	private final Configuration<Options> mConfig = new Configuration<>(Options.class);
	
	@Before
	public void setupCfg()
	{
		mConfig.set("max_division", 3);
	}
	
	@Test
	public void run() throws IOException
	{
		JsonObject json = (JsonObject) JsonUtils.fromFile(new File("best.json"));
		GeneSequence[] dna = json.getArray("chromosomes").stream()
			.map(JSON::deserialize)
			.map(o -> (GeneSequence) o)
			.toArray(l -> new GeneSequence[l]);
		
		CPPNN cppn = new CPPNN(BasicNeuralNet.build(dna[0]));
		NetworkInterface io = SimpleMazeGoal.SIMPLE_MAZE_IO;
		SubstrateGenerator gen = new SubstrateGenerator(io, mConfig.proxy());

//		io.inputs.forEach(i -> save_neuron("i", i, visualize(cppn, i.x, i.y)));
//		io.outputs.forEach(i -> save_neuron("o", i, visualize_out(cppn, i.x, i.y)));
		
		for(int i = 0 ; i < dna.length ; ++i)
		{
			Substrate s = gen.apply(new CPPNN(BasicNeuralNet.build(dna[i])));
			
			LOG.info("%d", i);
			s.hidden().forEach(h -> LOG.info("Hidden: (%.3f, %.3f)", h.x, h.y));
		}
	}
	
	private static void save_neuron(String t, Vec2 p, Canvas img)
	{
		save(String.format("cppn_%s_%.3f_%.3f.png", t, p.x, p.y), FXUtils.toImage(img));
	}
	
//	@Test
	public void printSeq()
	{
		GeneSequence dna = pattern2();
		CPPNN cppn = new CPPNN(BasicNeuralNet.build(dna));

		Vec2 i0 = new Vec2(-0.5, -1);
		Vec2 i1 = new Vec2( 0.5, -1);
		Vec2 o = new Vec2(0, 1);
		
		save("cppn_i0.png", FXUtils.toImage(visualize(cppn, i0.x, i0.y)));
		save("cppn_i1.png", FXUtils.toImage(visualize(cppn, i1.x, i1.y)));
		save("cppn_o.png", FXUtils.toImage(visualize_out(cppn, o.x, o.y)));
		
		for(int i = 1 ; i <= 4 ; ++i)
		{
			save("cppn_h0_" + i + ".png", FXUtils.toImage(visualize(cppn, -0.5, (i * 0.2) * 2 - 1)));
			save("cppn_h1_" + i + ".png", FXUtils.toImage(visualize(cppn,  0.5, (i * 0.2) * 2 - 1)));
		}
	}
	
//	@Test
	public void test()
	{
		LOG.info("generating dna");
		
		GeneSequence dna = pattern2();
		CPPNN cppnn = new CPPNN(BasicNeuralNet.build(dna));
		
		LOG.info("constructing quadmap");
		
		QuadMap qm = new QuadMap(0, 0, true, cppnn, mConfig.proxy());
		
		LOG.info("visualizing image");
		
		Canvas img = visualize(cppnn);
		int w = img.width(), h = img.height();
		
		LOG.info("saving raw image");
		
		save("cppn_raw.png", FXUtils.toImage(img));
		
		LOG.info("culling");
		
		qm.cull();
		
		LOG.info("drawing marks");
		
		SimpleGraphics gc = new SimpleGraphics(img);

		gc.setFill(Colors.RED);
		gc.setStroke(Colors.RED);
		follow(gc, qm.root());
		
		gc.setFill(Colors.GREEN);
		qm.prune().forEach(l -> {
			int x = (int) (w * 0.5 * (l.x1 + 1));
			int y = (int) (h * 0.5 * (l.y1 + 1));
			
			gc.fillCircle(x, y, 3);
		});
		
		LOG.info("saving marked image");
		
		save("cppn_marked.png", FXUtils.toImage(img));
		
		LOG.info("DONE");
	}
	
	private static void follow(SimpleGraphics gc, QuadMap.Point p)
	{
		System.out.println(Utils.repeat("  ", p.level()) +
			String.format("@(%.3f, %.3f) Lvl%d r=%.3f val=%.3f var=%.3f", p.x(), p.y(), p.level(), p.radius(), p.value(), p.variance()));
		
		if(p.leaf())
		{
			int x = (int) (gc.width() * 0.5 * (p.x() + 1));
			int y = (int) (gc.height() * 0.5 * (p.y() + 1));
			int rx = (int) (0.5 * gc.width() * p.radius());
			int ry = (int) (0.5 * gc.height() * p.radius());
			
			gc.strokeRect(x - rx, y - rx, Math.min(gc.width() - 1, x + ry + 1), Math.min(gc.height() - 1, y + ry + 1));
			gc.fillCircle(x, y, 3);
		}
		else
		{
			p.children().forEach(c -> follow(gc, c));
		}
	}

	private static Canvas visualize(CPPNN cppnn) { return visualize(cppnn, 0, 0); }
	private static Canvas visualize_out(CPPNN cppnn, double x1, double y1) { return visualize(invert(cppnn), x1, y1); }
	private static Canvas visualize(CPPNN cppnn, double x0, double y0)
	{
		HypercubeVisualizer viz = (new HypercubeVisualizer.Builder())
			.setResolutionW(1000)
			.setResolutionZ(1000)
			.build();
			
		return viz.generateImage(cppnn, x0, y0);
	}
	
	private static CPPNN invert(CPPNN cppn)
	{
		return new CPPNN(new NeuralNet() {
			@Override
			public int inputs()
			{
				return 5;
			}

			@Override
			public int outputs()
			{
				return 1;
			}

			@Override
			public double[] apply(double[] in)
			{
				return new double[] { 0.5 * (1 + cppn.at(in[2], in[3], in[0], in[1])) };
			}
		});
	}
	
	private static void save(String path, Image img)
	{
		Utils.run(() -> ImageIO.write(SwingFXUtils.fromFXImage(img, null), "PNG", new File(path)));
	}
	
	private static GeneSequence pattern2()
	{
		GeneSequence.Builder b = new GeneSequence.Builder();

		long x0 = b.addInput();
		long y0 = b.addInput();
		long x1 = b.addInput();
		long y1 = b.addInput();
		b.addInput(); // l
		
		long o = b.addOutput("TANH");

		long h1 = b.addHidden("GAUSS");
		long h2 = b.addHidden("SINE");
		long h3 = b.addHidden("ABS");

		b.link(x0, h1, 1.5);
		b.link(x1, h1, 1.5);

		b.link(0, h2, Math.PI / 2);
		b.link(y0, h2, 5 * Math.PI);
		b.link(y1, h2, 5 * Math.PI);
		
		b.link(x0, h3, 1);
		b.link(x1, h3, 1);

		b.link(h1, o, -3);
		b.link(h2, o, -3);
		b.link(h3, o, 1);
		b.link(x1, o, 1);
		b.link(y1, o, 1);
		b.link(0, o, 2);
		
		return b.build();
	}
	
	private static GeneSequence pattern1()
	{
		GeneSequence.Builder b = new GeneSequence.Builder();

		long x0 = b.addInput();
		long y0 = b.addInput();
		long x1 = b.addInput();
		long y1 = b.addInput();
		long l = b.addInput();
		
		long o = b.addOutput("TANH");
		
		long sine = b.addHidden("SINE");
		long abs_x = b.addHidden("ABS");
		long abs_y = b.addHidden("ABS");
		
		double rad = 2 * Math.PI;
		
		b.link(0, sine, 1.75 * -rad / 2);
		b.link(l, sine, 1.75 * rad);

		b.link(0, abs_x, 1);
		b.link(x1, abs_x, -1);
		b.link(0, abs_y, 1);
		b.link(y1, abs_y, -1);
		
		b.link(0, o, -5);
		b.link(sine, o, 10);
		b.link(abs_x, o, 7);
		b.link(abs_y, o, 3);
		
		return b.build();
	}
}
