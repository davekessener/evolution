package dave.evo.test;

import org.junit.Test;

import dave.evo.common.Options;
import dave.evo.eval.maze.MazeEvaluator;
import dave.evo.eval.maze.MazeOptions;
import dave.evo.eval.maze.RandomRunnerAI;
import dave.evo.eval.maze.RunnerController;
import dave.evo.eval.seq.RandomSequencer;
import dave.evo.eval.seq.SequenceEvaluator;
import dave.evo.eval.seq.SequenceOptions;
import dave.evo.eval.seq.Sequencer;
import dave.util.Averager;
import dave.util.RNG;
import dave.util.XoRoRNG;
import dave.util.config.Configuration;

public class BiasUT extends BaseUT
{
	private final Options options = (new Configuration<>(Options.class)).proxy();
	
	@Test
	public void computeBiasMazeSimple()
	{
		Configuration<MazeOptions> config = new Configuration<>(MazeOptions.class);
		RNG rng = new XoRoRNG();
		Averager avg = new Averager();
		MazeEvaluator eval = new MazeEvaluator(config.proxy(), rng);
		RunnerController ai = new RandomRunnerAI(rng);
		
		for(int i = 0 ; i < 1000000 ; ++i)
		{
			avg.add(eval.applyAsAI(ai));
		}
		
		LOG.info("Bias for maze.simple: %.3f", avg.get());
	}
	
	@Test
	public void computeBiasSequencing()
	{
		Configuration<SequenceOptions> config = new Configuration<>(SequenceOptions.class);
		RNG rng = new XoRoRNG();
		Averager avg = new Averager();
		SequenceEvaluator eval = new SequenceEvaluator(config.proxy(), rng, options);
		Sequencer ai = new RandomSequencer(rng);
		
		for(int i = 0 ; i < 1000000 ; ++i)
		{
			avg.add(eval.evaluate(ai));
		}
		
		LOG.info("Bias for sequencing: %.3f", avg.get());
	}
}
