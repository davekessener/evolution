package dave.evo.test;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import dave.evo.common.Library;
import dave.util.ShutdownService;
import dave.util.ShutdownService.Priority;
import dave.util.log.LogBase;
import dave.util.log.LogSink;
import dave.util.log.Logger;
import dave.util.log.Severity;
import dave.util.log.service.LogService;

public abstract class BaseUT
{
	@BeforeClass
	public static void setup()
	{
		LogService.INSTANCE.initialize(Library.DEBUG);
		
		LogBase.INSTANCE.registerSink(e -> (e.severity.level() >= Severity.INFO.level()), LogSink.build());
		LogBase.INSTANCE.start();
		
		ShutdownService.INSTANCE.register(Priority.LAST, LogBase.INSTANCE::stop);
	}
	
	@AfterClass
	public static void teardown()
	{
		ShutdownService.INSTANCE.shutdown();
	}
	
	protected static final Logger LOG = Logger.get("ut");
}
