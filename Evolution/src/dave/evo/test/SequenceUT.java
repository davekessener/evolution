package dave.evo.test;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import dave.evo.common.NeuralNet;
import dave.evo.eval.seq.NeuralNetSequencer;
import dave.evo.eval.seq.Sequencer;
import dave.json.JSON;
import dave.json.JsonObject;
import dave.json.JsonUtils;
import dave.util.RNG;
import dave.util.XoRoRNG;
import dave.util.log.Stdout;

public class SequenceUT extends BaseUT
{
	@Test
	public void runSeq() throws IOException
	{
		JsonObject json = (JsonObject) JsonUtils.fromFile(new File("snapshot.json"));
		NeuralNet nn = (NeuralNet) JSON.deserialize(json.get("neural-net"));
		Sequencer ai = new NeuralNetSequencer(nn);
		RNG rng = new XoRoRNG();
		
		Stdout.printf("Seed: %s\n", rng.getSeed());
		
		for(int i = 1 ; i < 9 ; ++i)
		{
			double[] e = new double[i];
			double r = 0;
			
			Stdout.printf("SEQ: n=%d\n", i);
			
			for(int j = 0 ; j < i ; ++j)
			{
				e[j] = ((rng.nextInt() & 1) == 0 ? 1 : -1);
				
				ai.enable(-1);
				ai.signal(e[j]);
				
				ai.update();
				
				double t = ai.leftright();
				
				Stdout.printf("(1) %d/%d: %.1f -> %.2f\n", (j+1), i, e[j], t);
				
				r += 0.2 * (1 - Math.abs(t));
			}
			
			for(int j = 0 ; j < i ; ++j)
			{
				ai.enable(1);
				ai.signal(0);
				
				ai.update();
				
				double t = ai.leftright();
				
				Stdout.printf("(2) %d/%d: %.1f -> %.3f\n", (j+1), i, e[i - j - 1], t);
				
				r += 0.8 * (1 - 0.5 * Math.abs(t - e[i - j - 1]));
			}
			
			r /= i;
			
			Stdout.printf("F = %.3f\n\n", r);
		}
	}
}
