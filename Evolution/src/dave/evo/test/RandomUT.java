package dave.evo.test;

import static org.junit.Assert.*;

import org.junit.Test;

import dave.util.RNG;
import dave.util.Variancer;
import dave.util.XoRoRNG;

public class RandomUT
{
	@Test
	public void testDoubleRange()
	{
		RNG rng = new XoRoRNG();
		
		for(int i = 0 ; i < 10000000 ; ++i)
		{
			double v = rng.nextDouble();
			
			assertTrue(v >= 0 && v < 1);
		}
	}
	
	@Test
	public void testGauss()
	{
		RNG rng = new XoRoRNG();
		Variancer avg = new Variancer();
		
		for(int i = 0 ; i < 10000000 ; ++i)
		{
			avg.add(rng.nextGauss());
		}
		
		assertEquals(0, avg.mean(), 0.001);
		assertEquals(1, avg.deviation(), 0.001);
	}
}
