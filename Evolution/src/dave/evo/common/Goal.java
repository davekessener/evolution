package dave.evo.common;

import dave.util.RNG;

public interface Goal
{
	public abstract NetworkInterface getInterface( );
	public abstract Agent getEvaluator(MessageBus bus, RNG rng, Options o);
}
