package dave.evo.common;

import java.util.function.Function;

public interface NeuralNet extends Function<double[], double[]>
{
	public abstract int inputs( );
	public abstract int outputs( );
	public abstract double[] apply(double[] in);
	
	public default void reset( ) { }
	
	public default double[] evaluate(double ... in) { return apply(in); }
}
