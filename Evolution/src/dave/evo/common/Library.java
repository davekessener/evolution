package dave.evo.common;

public final class Library
{
	public static final boolean DEBUG = false;
	
	public static final String TITLE = "AI Maze Explorer";
	public static final int VERSION = 5;
	
	public static final String CONFIG = "config.json";
	public static final String SCRIPT = "script.json";
	
	public static final class Modules
	{
		public static final String CONCURRENCY = "module.concurrency";
		public static final String ESHYPERNEAT = "module.es-hyper-neat";
		public static final String FLAT = "module.flat-nn";
		public static final String CONTROL = "module.control";
		public static final String MEMORY = "module.memory";
		public static final String GOAL = "module.goal";
		
		private Modules( ) { }
	}
	
	public static final class Policies
	{
		public static final String PERFECTION = "policy.perfection";
		public static final String LOGGING = "policy.logging";
		public static final String LIMITED = "policy.limited";
		
		private Policies( ) { }
	}
	
	public static final class Memories
	{
		public static final String LSTM = "memory.lstm";
		public static final String GRUMB = "memory.gru-mb";
		
		private Memories( ) { }
	}
	
	public static final class Goals
	{
		public static final String XOR = "goal.xor";
		public static final String TICTACTOE = "goal.tic-tac-toe";
		public static final String COUNTING = "goal.counting";
		public static final String MAZE = "goal.maze.simple";
		public static final String LIFE = "goal.maze.full";
		public static final String ROBOT = "goal.robot";
		public static final String SEQUENCE = "goal.sequencing";
		
		private Goals( ) { }
	}
	
	public static final class Agents
	{
		public static final class GenotypeCreator
		{
			public static final String ID = "genotype.creator";

			public static final String CREATE  = ID + ".create";
			public static final String MUTATE  = ID + ".mutate";
			public static final String BREED   = ID + ".breed";
			public static final String CREATED = ID + ".created";
			
			private GenotypeCreator( ) { }
		}

		public static final class ChromosomeCreator
		{
			public static final String ID = "chromosome.creator";

			public static final String CREATE  = ID + ".create";
			public static final String MUTATE  = ID + ".mutate";
			public static final String BREED   = ID + ".breed";
			public static final String CREATED = ID + ".created";
			
			private ChromosomeCreator( ) { }
		}
		
		public static final class PhenotypeCreator
		{
			public static final String ID = "phenotype.creator";

			public static final String INSTANTIATE = ID + ".instantiate";
			public static final String CREATED     = ID + ".created";
			
			private PhenotypeCreator( ) { }
		}
		
		public static final class PopulationCreator
		{
			public static final String ID = "population.creator";
			
			public static final String CREATE  = ID + ".create";
			public static final String IMPROVE = ID + ".improve";
			public static final String RESET = ID + ".reset";
			public static final String CREATED = ID + ".created";
			
			private PopulationCreator( ) { }
		}
		
		public static final class Evaluator
		{
			public static final String ID = "evaluator";
			
			public static final String EVALUATE = ID + ".evaluate";
			public static final String EVALUATED = ID + ".evaluated";
			
			private Evaluator( ) { }
		}
		
		public static final class Controller
		{
			public static final String ID = "controller";
			
			public static final String GET = ID + ".get";
			
			private Controller( ) { }
		}
		
		public static final class Storage
		{
			public static final String ID = "storage";
			
			public static final String REGISTER = ID + ".register";
			public static final String SAVE     = ID + ".save";
			public static final String SAVED    = ID + ".saved";
			public static final String LOAD     = ID + ".load";
			public static final String LOADED   = ID + ".loaded";
			
			private Storage( ) { }
		}
		
		private Agents( ) { }
	}
	
	public static final class State
	{
		public static final String SEED = "seed";
		public static final String HISTORY = "history";
		public static final String POPULATION = "population";
		
		private State( ) { }
	}
	
	public static final class App
	{
		public static final String APP = "app";
		public static final String RNG = "app.random";
		public static final String OPTIONS = "app.options";
		public static final String CONFIG = "app.config";
		public static final String HISTORY = "app.history";
		public static final String BUS = "app.bus";
		public static final String RUNNING = "app.running";
		public static final String EVALUATION_IO = "app.interface.evaluation";
		public static final String CHROMOSOME_IO = "app.interface.chromosomes";
		public static final String HIDDEN_CREATE = "app.hidden.create";
		public static final String POLICY = "app.control.policy";
		public static final String STORAGE_FILE = "app.storage.path";
		public static final String AGENT_EVALUATE = "app.agent.evaluate";
		public static final String AGENT_CONTROL = "app.agent.control";
		public static final String AGENT_STORAGE = "app.agent.storage";
		public static final String AGENT_PHENOTYPE_CREATE = "app.agent.create_phenotype";
		public static final String AGENT_GENOTYPE_CREATE = "app.agent.create_genotype";
		public static final String AGENT_POPULATION_CREATE = "app.agent.create_population";
		public static final String AGENT_CHROMOSOME_CREATE = "app.agent.create_chromosome";
		public static final String EVALUATE = "app.evaluate";
		public static final String PHENOTYPE_CREATE = "app.phenotype.create";
		public static final String GENOTYPE_CREATE = "app.genotype.create";
		public static final String GENOTYPE_MUTATE = "app.genotype.create";
		public static final String GENOTYPE_BREED = "app.genotype.create";
		public static final String GENES_CREATE = "app.genes.creator";
		public static final String GENES_MUTATE = "app.genes.mutator";
		public static final String GENES_BREED = "app.genes.breeder";
		public static final String CHROMOSOME_CREATOR = "app.nn.chromosomes";
		public static final String NN_CREATE = "app.nn.create";
		
		private App( ) { }
	}
	
	private Library( ) { }
}
