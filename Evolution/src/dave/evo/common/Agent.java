package dave.evo.common;

import java.util.function.Consumer;

import dave.util.Actor;
import dave.util.Identifiable;

public interface Agent extends Identifiable, Actor, Consumer<Message>
{
}
