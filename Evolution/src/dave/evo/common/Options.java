package dave.evo.common;

import dave.evo.config.EvaluationOptions;
import dave.evo.config.FlatOptions;
import dave.evo.config.GeneOptions;
import dave.evo.config.HypercubeOptions;
import dave.evo.config.PopulationOptions;
import dave.evo.config.SpeciationOptions;
import dave.evo.config.StorageOptions;
import dave.evo.config.SubstrateOptions;
import dave.util.config.Default;

public interface Options extends
	GeneOptions,
	SpeciationOptions,
	PopulationOptions,
	SubstrateOptions,
	HypercubeOptions,
	StorageOptions,
	EvaluationOptions,
	FlatOptions
{
	@Default("false")
	public abstract boolean quit_on_success( );
	
	@Default("true")
	public abstract boolean save_progress( );
}
