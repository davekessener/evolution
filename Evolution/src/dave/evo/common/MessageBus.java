package dave.evo.common;

import dave.util.Actor;

public interface MessageBus extends Actor
{
	public abstract void register(Agent a);
	public abstract void unregister(Agent a);
	
	public abstract void accept(Message msg);
}
