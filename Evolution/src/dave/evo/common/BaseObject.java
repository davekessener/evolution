package dave.evo.common;

import dave.util.Inspectable;
import dave.util.log.service.DebugObject;

public abstract class BaseObject extends DebugObject implements Inspectable
{
}
