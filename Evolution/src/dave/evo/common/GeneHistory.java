package dave.evo.common;

import java.util.HashMap;
import java.util.Map;

import dave.json.Container;
import dave.json.JsonCollectors;
import dave.json.JsonNumber;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Restorable;
import dave.json.Saver;

@Container
public class GeneHistory implements Restorable
{
	private final Map<Long, Long> mSplit;
	private final Map<Link, Long> mJoin;
	private long mNextID;
	
	public GeneHistory()
	{
		mSplit = new HashMap<>();
		mJoin = new HashMap<>();
		mNextID = 1;
	}
	
	@Override
	@Saver
	public JsonValue save()
	{
		JsonObject json = new JsonObject();
		
		json.putLong("next_id", mNextID);
		json.put("split", mSplit.entrySet().stream().collect(JsonCollectors.ofObject((o, e) -> o.putLong("" + e.getKey(), e.getValue()))));
		json.put("join", mJoin.entrySet().stream().collect(JsonCollectors.ofObject((o, e) -> o.putLong(e.getKey().toString(), e.getValue()))));
		
		return json;
	}
	
	@Override
	public void load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		mNextID = o.getLong("next_id");
		
		mSplit.clear();
		o.getObject("split").forEach((id, v) -> mSplit.put(Long.parseLong(id), ((JsonNumber) v).getLong()));
		
		mJoin.clear();
		o.getObject("join").forEach((id, v) -> mJoin.put(Link.from(id), ((JsonNumber) v).getLong()));
	}
	
	public long nextID() { return mNextID++; }
	
	public long link(long from, long to)
	{
		Link link = new Link(from, to);
		Long id = mJoin.get(link);
		
		if(id == null)
		{
			mJoin.put(link, id = nextID());
		}
		
		return id;
	}
	
	public long split(long link)
	{
		Long id = mSplit.get(link);
		
		if(id == null)
		{
			mSplit.put(link, id = nextID());
		}
		
		return id;
	}
	
	private static class Link
	{
		public final long from;
		public final long to;
		
		public Link(long from, long to)
		{
			this.from = from;
			this.to = to;
		}
		
		@Override
		public int hashCode()
		{
			return (Long.hashCode(from) * 3) ^ (Long.hashCode(to) * 7);
		}
		
		@Override
		public boolean equals(Object o)
		{
			if(o instanceof Link)
			{
				Link l = (Link) o;
				
				return from == l.from && to == l.to;
			}
			
			return false;
		}
		
		@Override
		public String toString()
		{
			return "" + from + "->" + to;
		}
		
		public static Link from(String v)
		{
			String[] p = v.split("->");
			
			return new Link(Long.parseLong(p[0]), Long.parseLong(p[1]));
		}
	}
}
