package dave.evo.common;

import java.util.Objects;

public class Message
{
	public final String from, to;
	public final String subject;
	public final Message reply;
	public final Object payload, info;
	
	public Message(String from, String to, String subject) { this(from, to, subject, null, null, null); }
	public Message(String from, String to, String subject, Message reply, Object payload, Object info)
	{
		this.from = from;
		this.to = to;
		this.subject = subject;
		this.reply = reply;
		this.payload = payload;
		this.info = info;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T get()
	{
		return (T) payload;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T info()
	{
		return (T) info;
	}
	
	@Override
	public int hashCode()
	{
		return (from.hashCode() * 3) ^ (to.hashCode() * 7) ^ (subject.hashCode() * 11);
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Message)
		{
			Message msg = (Message) o;
			
			return from.equals(msg.from)
				&& to.equals(msg.to)
				&& subject.equals(msg.subject)
				&& Objects.equals(reply, msg.reply)
				&& Objects.equals(payload, msg.payload)
				&& Objects.equals(info, msg.info);
		}
		
		return false;
	}
	
	@Override
	public String toString()
	{
		return String.format("{%s -> %s: %s [%s] | %s, %s}", from, to, subject, payload, reply, info);
	}
}
