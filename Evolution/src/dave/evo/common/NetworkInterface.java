package dave.evo.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dave.util.MathUtils;
import dave.util.math.Vec2;

public class NetworkInterface
{
	public final Vec2 bias;
	public final List<Vec2> inputs, outputs;
	
	public NetworkInterface(Vec2 bias, List<Vec2> inputs, List<Vec2> outputs)
	{
		this.bias = bias;
		this.inputs = Collections.unmodifiableList(new ArrayList<>(inputs));
		this.outputs = Collections.unmodifiableList(new ArrayList<>(outputs));
	}
	
	protected NetworkInterface(NetworkInterface io)
	{
		this.bias = io.bias;
		this.inputs = io.inputs;
		this.outputs = io.outputs;
	}
	
	public static NetworkInterface buildSimple(int in, int out)
	{
		Builder b = new Builder(0, 0);
		
		for(int i = 0 ; i < in ; ++i)
		{
			double p = (in == 1 ? 0.5 : i / (in - 1.0));
			
			b.addInput(MathUtils.lerp(-1, 1, p), -1);
		}
		
		for(int i = 0 ; i < out ; ++i)
		{
			double p = (out == 1 ? 0.5 : i / (out - 1.0));
			
			b.addOutput(MathUtils.lerp(-1, 1, p), 1);
		}
		
		return b.build();
	}
	
	public static class Builder
	{
		private final Vec2 mBias;
		private final List<Vec2> mInputs, mOutputs;
		
		public Builder(double x, double y) { this(new Vec2(x, y)); }
		public Builder(Vec2 bias)
		{
			mBias = bias;
			mInputs = new ArrayList<>();
			mOutputs = new ArrayList<>();
		}
		
		public Builder addInput(double x, double y) { return addInput(new Vec2(x, y)); }
		public Builder addInput(Vec2 v) { mInputs.add(v); return this; }
		
		public Builder addOutput(double x, double y) { return addOutput(new Vec2(x, y)); }
		public Builder addOutput(Vec2 v) { mOutputs.add(v); return this; }
		
		public NetworkInterface build()
		{
			return new NetworkInterface(mBias, mInputs, mOutputs);
		}
	}
}
