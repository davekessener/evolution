package dave.evo.common;

import dave.evo.dna.Population;

public interface Policy
{
	public abstract boolean done(Population pop);
}
