package dave.evo.dna;

import dave.evo.util.DifferenceTracker;
import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saver;

@Container
public class InputGene extends NeuronGene
{
	public InputGene(long m)
	{
		super(m);
	}
	
	@Override
	public String toString()
	{
		return String.format("Input (Gene %d)", this.marker());
	}
	
	@Override
	@Saver
	public JsonValue save()
	{
		return super.save();
	}
	
	@Loader
	public static InputGene load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		long m = o.getLong("marker");
		
		return new InputGene(m);
	}

	public static void difference(InputGene a, InputGene b, DifferenceTracker t)
	{
	}
}
