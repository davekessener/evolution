package dave.evo.dna;

import dave.evo.util.DifferenceTracker;
import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saver;

@Container
public class BiasGene extends NeuronGene
{
	public BiasGene(long m)
	{
		super(m);
	}
	
	@Override
	public String toString()
	{
		return String.format("Bias (Gene %d)", this.marker());
	}
	
	@Override
	@Saver
	public JsonValue save()
	{
		return super.save();
	}
	
	@Loader
	public static BiasGene load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		long m = o.getLong("marker");
		
		return new BiasGene(m);
	}
	
	public static void difference(BiasGene a, BiasGene b, DifferenceTracker t)
	{
	}
}
