package dave.evo.dna;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import dave.evo.util.DifferenceTracker;
import dave.util.SevereException;

public interface Gene
{
	public abstract long marker( );
	
	public static void difference(Gene a, Gene b, DifferenceTracker t)
	{
		Class<?> c = a.getClass();
		
		if(!c.equals(b.getClass()))
			throw new IllegalArgumentException(String.format("%s != %s", a.getClass().getName(), b.getClass().getName()));
		
		try
		{
			Method m = c.getMethod("difference", c, c, DifferenceTracker.class);
			
			m.invoke(null, a, b, t);
		}
		catch(NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
		{
			throw new SevereException(e);
		}
	}
	
	public static enum Difference
	{
		WEIGHT,
		ACTIVATION
	}
}
