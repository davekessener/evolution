package dave.evo.dna;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import dave.evo.common.BaseObject;
import dave.evo.common.GeneHistory;
import dave.evo.common.Library;
import dave.evo.config.SpeciationOptions;
import dave.json.Container;
import dave.json.JSON;
import dave.json.JsonArray;
import dave.json.JsonCollectors;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;
import dave.util.log.Logger;
import dave.util.log.service.LogService;
import dave.util.stream.StreamUtils;

@Container
public class GeneSequence extends BaseObject implements Chromosome, Saveable
{
	private final List<Gene> mGenes;
	
	public GeneSequence(List<Gene> genes)
	{
		mGenes = new ArrayList<>(genes);
		
		mGenes.sort((g0, g1) -> Long.compare(g0.marker(), g1.marker()));
		
		if(Library.DEBUG)
		{
			try
			{
				validate();
			}
			catch(IllegalStateException ex)
			{
				String s = genes().map(Object::toString).collect(Collectors.joining(", "));
				
				log(TAG_INFO, "%s", s);
				
				LogService.Entry e = LogService.INSTANCE.retrieve(getObjectID(), GeneSequence.TAG_INFO);
				
				Stream.of(e.caller).forEach(c -> Logger.DEFAULT.error("%s", c.toString()));
				
				throw ex;
			}
		}
	}
	
	private void validate()
	{
		StreamUtils.product(genes(), genes())
			.filter(p -> p.first != p.second && p.first.marker() == p.second.marker())
			.findFirst().ifPresent(p -> {
				Logger.DEFAULT.error("Malformed genome (dupe gene %s - %s): %s", p.first.toString(), p.second.toString(), toString());
				
				genes().map(StreamUtils.stream_with_index()).forEach(g -> Logger.DEFAULT.error("[%02d] %s", g.index, g.value));
				
				throw new IllegalStateException("Duplicate gene: " + p.first + " - " + p.second);
		});
		
		genes()
			.filter(g -> (g instanceof LinkGene))
			.map(lg -> (LinkGene) lg)
			.filter(lg -> !genes()
				.filter(g -> (g instanceof NeuronGene))
				.anyMatch(g -> g.marker() == lg.from() || g.marker() == lg.to()))
			.findFirst()
			.ifPresent(lg -> {
				Logger.DEFAULT.error("Malformed genome (dangling link %s): %s", lg.toString(), toString());
				genes().map(StreamUtils.stream_with_index()).forEach(g -> Logger.DEFAULT.error("[%02d] %s", g.index, g.value));
				
				throw new IllegalStateException("Dangling link: " + lg);
			});
		
		genes()
			.filter(g -> (g instanceof ConnectedNeuronGene))
			.map(g -> (ConnectedNeuronGene) g)
			.filter(cn -> !genes()
				.filter(g -> (g instanceof LinkGene))
				.map(g -> (LinkGene) g)
				.anyMatch(lg -> lg.to() == cn.marker()))
			.findFirst()
			.ifPresent(cn -> {
				Logger.DEFAULT.error("Malformed genome (unconnected neuron %s): %s", cn.toString(), toString());
				genes().map(StreamUtils.stream_with_index()).forEach(g -> Logger.DEFAULT.error("[%02d] %s", g.index, g.value));
				
				throw new IllegalStateException("Unconnected neuron: " + cn);
			});
	}
	
	@Override
	@Saver
	public JsonValue save()
	{
		return mGenes.stream().map(JSON::serialize).collect(JsonCollectors.ofArray());
	}
	
	@Loader
	public static GeneSequence load(JsonValue json)
	{
		JsonArray a = (JsonArray) json;
		
		return new GeneSequence(a.stream().map(e -> (Gene) JSON.deserialize(e)).collect(Collectors.toList()));
	}
	
	public Stream<Gene> genes() { return mGenes.stream(); }
	public int size() { return mGenes.size(); }
	public Gene get(int i) { return mGenes.get(i); }
	
	@Override
	public String toString()
	{
		return String.format("Chromosome-%08x[%d]", hashCode(), size());
	}
	
	public static double difference(GeneSequence g0, GeneSequence g1, SpeciationOptions o)
	{
		if(g0.size() < g1.size())
		{
			GeneSequence t = g0 ; g0 = g1; g1 = t;
		}
		
		return (new GeneAlignment(g0, g1)).difference(o);
	}
	
	public static class Builder
	{
		private final GeneHistory mHistory;
		private final List<Gene> mGenes;
		
		public Builder()
		{
			mHistory = new GeneHistory();
			mGenes = new ArrayList<>();
			
			mGenes.add(new BiasGene(0));
		}
		
		public long addInput()
		{
			long id = mHistory.nextID();
			
			mGenes.add(new InputGene(id));
			
			return id;
		}
		
		public long addOutput(String activation)
		{
			long id = mHistory.nextID();
			
			mGenes.add(new OutputGene(activation, id));
			
			return id;
		}
		
		public long addHidden(String activation)
		{
			long id = mHistory.nextID();
			
			mGenes.add(new HiddenGene(activation, id));
			
			return id;
		}
		
		public long link(long from, long to, double w)
		{
			long id = mHistory.link(from, to);
			
			mGenes.add(new LinkGene(id, from, to, w, true));
			
			return id;
		}
		
		public GeneSequence build()
		{
			return new GeneSequence(mGenes);
		}
	}
	
	public static final String TAG_INFO = "genome.info";
}
