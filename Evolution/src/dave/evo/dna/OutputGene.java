package dave.evo.dna;

import dave.evo.util.DifferenceTracker;
import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saver;

@Container
public class OutputGene extends ConnectedNeuronGene
{
	public OutputGene(String a, long m)
	{
		super(a, m);
	}
	
	@Override
	public String toString()
	{
		return String.format("Output (Gene %d) [%s]", this.marker(), activation());
	}
	
	@Override
	@Saver
	public JsonValue save()
	{
		return super.save();
	}
	
	@Loader
	public static OutputGene load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		long m = o.getLong("marker");
		String a = o.getString("activation");
		
		return new OutputGene(a, m);
	}

	public static void difference(OutputGene a, OutputGene b, DifferenceTracker t)
	{
		ConnectedNeuronGene.difference(a, b, t);
	}
}
