package dave.evo.dna;

import java.lang.reflect.InvocationTargetException;

import dave.evo.config.SpeciationOptions;
import dave.util.SevereException;

public interface Chromosome
{
	public static double difference(Chromosome c1, Chromosome c2, SpeciationOptions o)
	{
		Class<? extends Chromosome> c = c1.getClass();
		
		if(!c.equals(c2.getClass()))
			throw new IllegalStateException(String.format("%s != %s", c.getSimpleName(), c2.getClass().getSimpleName()));
		
		try
		{
			return (double) c.getMethod("difference", c, c, SpeciationOptions.class).invoke(null, c1, c2, o);
		}
		catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e)
		{
			throw new SevereException(e);
		}
	}
}
