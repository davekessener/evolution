package dave.evo.dna;

import java.util.Map;

import dave.evo.config.SpeciationOptions;
import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;
import dave.util.math.Matrix;

@Container
public class WeightMatrix implements Chromosome, Saveable
{
	private final Matrix mMatrix;
	private final String mActivation;
	
	public WeightMatrix(Matrix m, String a)
	{
		mMatrix = m;
		mActivation = a;
	}
	
	public String activation() { return mActivation; }
	public Matrix matrix() { return mMatrix; }
	
	@Override
	@Saver
	public JsonValue save()
	{
		JsonObject json = new JsonObject();
		
		json.putString("activation", mActivation);
		json.put("matrix", mMatrix.save());
		
		return json;
	}

	@Loader
	public static WeightMatrix load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		String a = o.getString("activation");
		Matrix m = Matrix.load(o.get("matrix"));
		
		return new WeightMatrix(m, a);
	}
	
	public static double difference(WeightMatrix a, WeightMatrix b, SpeciationOptions o)
	{
		if(a.mMatrix.width() != b.mMatrix.width() || a.mMatrix.height() != b.mMatrix.height())
			throw new IllegalArgumentException();
		
		int w = a.mMatrix.width(), h = a.mMatrix.height();
		double v = 0;
		
		for(int y = 0 ; y < h ; ++y)
		{
			for(int x = 0 ; x < w ; ++x)
			{
				double t = a.mMatrix.get(x, y) - b.mMatrix.get(x, y);
				
				v += t * t;
			}
		}
		
		double dw =  v / (w * h);
		double da = (a.mActivation.equals(b.mActivation) ? 0 : 1);
		Map<String, Double> weights = o.distance_weights();
		
		return dw * weights.get("weights") + da * weights.get("activation");
	}
}
