package dave.evo.dna;

import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Stream;

import dave.json.Container;
import dave.json.JsonCollectors;
import dave.json.JsonObject;
import dave.json.JsonArray;
import dave.json.JsonValue;
import dave.json.Loadable;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class Species implements Saveable, Loadable
{
	private final Set<Entry> mMembers;
	
	public Species()
	{
		mMembers = new TreeSet<>((e0, e1) -> Double.compare(e1.fitness, e0.fitness) * 10 - 1);
	}
	
	@Override
	@Saver
	public JsonValue save()
	{
		return mMembers.stream().map(this::save).collect(JsonCollectors.ofArray());
	}
	
	@Override
	public void load(JsonValue json)
	{
		mMembers.clear();

		((JsonArray) json).forEach(v -> {
			JsonObject o = (JsonObject) v;
			
			double f = o.getDouble("fitness");
			Genome g = Genome.load(o.get("genome"));
			
			add(g, f);
		});
	}
	
	@Loader
	public static Species create(JsonValue json)
	{
		Species r = new Species();
		
		r.load(json);
		
		return r;
	}
	
	public int size() { return mMembers.size(); }
	public Stream<Entry> entries() { return mMembers.stream(); }
	public double fitness() { return entries().mapToDouble(e -> e.fitness).sum() / size(); }
	
	public void add(Genome g, double f)
	{
		mMembers.add(new Entry(f, g));
	}
	
	private JsonValue save(Entry e)
	{
		JsonObject json = new JsonObject();
		
		json.putNumber("fitness", e.fitness);
		json.put("genome", e.genome.save());
		
		return json;
	}
	
	public final class Entry
	{
		public final double fitness;
		public final Genome genome;
		
		public Entry(double fitness, Genome genome)
		{
			this.fitness = fitness;
			this.genome = genome;
		}
		
		public double adjustedFitness()
		{
			return fitness / mMembers.size();
		}
		
		public Species species()
		{
			return Species.this;
		}
	}
}
