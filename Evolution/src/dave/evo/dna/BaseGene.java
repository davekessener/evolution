package dave.evo.dna;

import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Saveable;

public abstract class BaseGene implements Gene, Saveable
{
	private final long mMarker;
	
	protected BaseGene(long m)
	{
		mMarker = m;
	}
	
	@Override
	public long marker()
	{
		return mMarker;
	}
	
	@Override
	public JsonValue save()
	{
		JsonObject json = new JsonObject();
		
		json.putLong("marker", mMarker);
		
		return json;
	}
}
