package dave.evo.dna;

import dave.evo.util.DifferenceTracker;
import dave.json.Container;
import dave.json.JsonConstant;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saver;

@Container
public class LinkGene extends BaseGene
{
	private final long mFrom, mTo;
	private final double mWeight;
	private final boolean mActive;
	
	public LinkGene(long m, long from, long to, double w, boolean a)
	{
		super(m);
		
		mFrom = from;
		mTo = to;
		mWeight = w;
		mActive = a;
	}
	
	@Override
	public String toString()
	{
		return String.format("Link (Gene %d): %d -> %d (%.3f) [%s]", this.marker(), mFrom, mTo, mWeight, mActive ? "ON " : "OFF");
	}
	
	@Override
	@Saver
	public JsonValue save()
	{
		JsonObject json = (JsonObject) super.save();
		
		json.putLong("from", mFrom);
		json.putLong("to", mTo);
		json.putNumber("weight", mWeight);
		json.put("active", JsonConstant.of(mActive));
		
		return json;
	}
	
	@Loader
	public static LinkGene load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		long m = o.getLong("marker");
		long from = o.getLong("from");
		long to = o.getLong("to");
		double w = o.getDouble("weight");
		boolean a = (o.get("active") == JsonConstant.TRUE);
		
		return new LinkGene(m, from, to, w, a);
	}
	
	public long from() { return mFrom; }
	public long to() { return mTo; }
	public double weight() { return mWeight; }
	public boolean active() { return mActive; }
	
	public LinkGene activate()
	{
		return new LinkGene(marker(), mFrom, mTo, mWeight, true);
	}
	
	public LinkGene deactivate()
	{
		return new LinkGene(marker(), mFrom, mTo, mWeight, false);
	}
	
	public static void difference(LinkGene a, LinkGene b, DifferenceTracker t)
	{
		double d = a.weight() - b.weight();
		
		t.add(Gene.Difference.WEIGHT, Math.abs(d) + (a.active() == b.active() ? 0 : 1));
	}
}
