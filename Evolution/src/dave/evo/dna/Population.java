package dave.evo.dna;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import dave.evo.config.SpeciationOptions;
import dave.json.Container;
import dave.json.JsonArray;
import dave.json.JsonCollectors;
import dave.json.JsonValue;
import dave.json.Loadable;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class Population implements Saveable, Loadable
{
	private final SpeciationOptions mOptions;
	private final Set<Species> mSpecies;
	private Genome mBest;
	private double mHighscore;
	private double mSpeciationDelta;
	
	public Population(SpeciationOptions o)
	{
		mSpecies = new TreeSet<>((s0, s1) -> Integer.compare(s1.size(), s0.size()) * 10 - 1);
		mOptions = o;
		mBest = null;
		mHighscore = -1;
		mSpeciationDelta = mOptions.speciation_delta();
	}
	
	@Override
	@Saver
	public JsonValue save()
	{
		return mSpecies.stream().map(Species::save).collect(JsonCollectors.ofArray());
	}
	
	@Override
	public void load(JsonValue json)
	{
		mSpecies.clear();
		
		((JsonArray) json).forEach(v -> mSpecies.add(Species.create(v)));
	}
	
	@Loader
	public static Population load(SpeciationOptions o, JsonValue json)
	{
		Population p = new Population(o);
		
		p.load(json);
		
		return p;
	}
	
	public int speciesCount() { return mSpecies.size(); }
	public int genomeCount() { return mSpecies.stream().mapToInt(Species::size).sum(); }
	public Stream<Species> species() { return mSpecies.stream(); }
	public Optional<Genome> best() { return Optional.ofNullable(mBest); }
	public double highscore() { return mHighscore; }
	
	public void add(Genome g, double f)
	{
		if(f > mHighscore)
		{
			mBest = g;
			mHighscore = f;
		}
		
		for(Species s : mSpecies)
		{
			double d = Genome.difference(g, s.entries().findFirst().get().genome, mOptions);
			
			if(d < mSpeciationDelta)
			{
				s.add(g, f);
				
				return;
			}
		}
		
		Species s = new Species();
		
		s.add(g, f);
		
		mSpecies.add(s);
	}
	
	public void rebalance()
	{
		if(mSpecies.size() >= mOptions.minimum_species_count())
			return;
		
		List<Species.Entry> all = species()
			.flatMap(s -> s.entries())
			.sorted((e0, e1) -> Double.compare(e1.fitness, e0.fitness))
			.collect(Collectors.toList());
		
		while(mSpecies.size() < mOptions.minimum_species_count())
		{
			mSpecies.clear();
			
			all.forEach(e -> add(e.genome, e.fitness));
			
			mSpeciationDelta /= 2;
			
			if(mSpeciationDelta < EPSILON)
				mSpeciationDelta = 0;
		}
		
		mSpeciationDelta = mOptions.speciation_delta();
	}
	
	private static final double EPSILON = 0.01;
}
