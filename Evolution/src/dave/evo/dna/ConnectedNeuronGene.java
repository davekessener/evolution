package dave.evo.dna;

import dave.evo.util.DifferenceTracker;
import dave.json.JsonObject;
import dave.json.JsonValue;

public abstract class ConnectedNeuronGene extends NeuronGene
{
	private final String mActivation;
	
	protected ConnectedNeuronGene(String a, long m)
	{
		super(m);
		
		mActivation = a;
	}
	
	public String activation() { return mActivation; }
	
	@Override
	public JsonValue save()
	{
		JsonObject json = (JsonObject) super.save();
		
		json.putString("activation", mActivation);
		
		return json;
	}
	
	public static void difference(ConnectedNeuronGene a, ConnectedNeuronGene b, DifferenceTracker t)
	{
		t.add(Gene.Difference.ACTIVATION, a.activation().equals(b.activation()) ? 0 : 1);
	}
}
