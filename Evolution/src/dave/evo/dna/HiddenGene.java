package dave.evo.dna;

import dave.evo.util.DifferenceTracker;
import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saver;

@Container
public class HiddenGene extends ConnectedNeuronGene
{
	public HiddenGene(String a, long m)
	{
		super(a, m);
	}
	
	@Override
	public String toString()
	{
		return String.format("Hidden (Gene %d) [%s]", this.marker(), this.activation());
	}
	
	@Override
	@Saver
	public JsonValue save()
	{
		return super.save();
	}
	
	@Loader
	public static HiddenGene load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		long m = o.getLong("marker");
		String a = o.getString("activation");
		
		return new HiddenGene(a, m);
	}
	
	public static void difference(HiddenGene a, HiddenGene b, DifferenceTracker t)
	{
		ConnectedNeuronGene.difference(a, b, t);
	}
}
