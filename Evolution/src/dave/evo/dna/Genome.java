package dave.evo.dna;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import dave.evo.common.BaseObject;
import dave.evo.config.SpeciationOptions;
import dave.json.Container;
import dave.json.JSON;
import dave.json.JsonCollectors;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;
import dave.util.Averager;
import dave.util.stream.StreamUtils;

@Container
public class Genome extends BaseObject implements Saveable
{
	private final List<Chromosome> mChromosomes;
	
	public Genome(List<Chromosome> c)
	{
		mChromosomes = new ArrayList<>(c);
	}
	
	public Stream<Chromosome> chromosomes() { return mChromosomes.stream(); }
	public int size() { return mChromosomes.size(); }
	public Chromosome get(int i) { return mChromosomes.get(i); }
	
	@Override
	@Saver
	public JsonValue save()
	{
		JsonObject json = new JsonObject();
		
		json.put("chromosomes", chromosomes().map(JSON::serialize).collect(JsonCollectors.ofArray()));
		
		return json;
	}
	
	@Loader
	public static Genome load(JsonValue json)
	{
		return new Genome(((JsonObject) json).getArray("chromosomes").stream()
			.map(JSON::deserialize)
			.map(o -> (Chromosome) o)
			.collect(Collectors.toList()));
	}
	
	@Override
	public String toString()
	{
		return String.format("Genome-%08x[%d]", hashCode(), size());
	}

	public static double difference(Genome g0, Genome g1, SpeciationOptions o)
	{
		if(g0.size() != g1.size())
			throw new IllegalArgumentException(String.format("%d != %d", g0.size(), g1.size()));
		
		Averager avg = new Averager();
		
		StreamUtils.zip(g0.chromosomes(), g1.chromosomes())
			.forEach(p -> avg.add(Chromosome.difference(p.first, p.second, o)));
		
		return avg.get();
	}
}
