package dave.evo.dna;

public class GenePair
{
	public final Gene left, right;
	
	public GenePair(Gene left, Gene right)
	{
		this.left = left;
		this.right = right;
	}
}
