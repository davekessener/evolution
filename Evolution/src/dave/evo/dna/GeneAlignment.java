package dave.evo.dna;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import dave.evo.config.SpeciationOptions;
import dave.evo.util.DifferenceTracker;

public class GeneAlignment
{
	private final List<GenePair> mGenes;
	private final int mDisjoint, mExcess, mSize;
	private final DifferenceTracker mDiff;
	
	public GeneAlignment(GeneSequence g0, GeneSequence g1) { this(g0, g1, true); }
	public GeneAlignment(GeneSequence g0, GeneSequence g1, boolean strict)
	{
		if(g0.size() < g1.size())
		{
			if(strict)
			{
				throw new IllegalArgumentException();
			}
			else
			{
				GeneSequence g = g0; g0 = g1; g1 = g;
			}
		}
		
		mGenes = new ArrayList<>();
		mDiff = new DifferenceTracker();
		mSize = g0.size();
		
		int disjoint = 0, excess = 0;
		for(int i0 = 0, i1 = 0 ; i0 < g0.size() || i1 < g1.size() ;)
		{
			boolean e0 = i0 < g0.size();
			boolean e1 = i1 < g1.size();
			boolean just_one = ((e0 && !e1) || (e1 && !e0));
			Gene a = e0 ? g0.get(i0) : null;
			Gene b = e1 ? g1.get(i1) : null;
			GenePair e = null;
			
			if(just_one || a.marker() == b.marker())
			{
				e = new GenePair(a, b);
				++i0; ++i1;
				
				if(just_one)
				{
					++excess;
				}
				else
				{
					Gene.difference(a, b, mDiff);
				}
			}
			else if(a.marker() < b.marker())
			{
				e = new GenePair(a, null);
				++i0;
				++disjoint;
			}
			else
			{
				e = new GenePair(null, b);
				++i1;
				++disjoint;
			}
			
			mGenes.add(e);
		}
		
		mDisjoint = disjoint;
		mExcess = excess;
	}
	
	public Stream<GenePair> genes() { return mGenes.stream(); }
	
	public double difference(double dw, double ew, Map<String, Double> weights, int th)
	{
		double d = 0;
		int s = (mSize >= th ? mSize : 1);
		
		d += dw * mDisjoint / s;
		d += ew * mExcess / s;
		
		for(String k : weights.keySet())
		{
			d += weights.get(k) * mDiff.get(k);
		}
		
		return d;
	}
	
	public double difference(SpeciationOptions o)
	{
		return difference(o.weight_disjoint(), o.weight_excess(), o.distance_weights(), o.large_genome_threshold());
	}
}
