package dave.maze;

import java.io.File;
import java.io.IOException;

import dave.evo.common.NeuralNet;
import dave.evo.eval.maze.NeuralNetAI;
import dave.evo.eval.maze.RunnerController;
import dave.evo.eval.maze.SimpleRunnerAI;
import dave.evo.eval.maze.World;
import dave.json.JSON;
import dave.json.JsonObject;
import dave.json.JsonUtils;
import dave.json.SevereIOException;
import dave.util.RNG;
import dave.util.Utils;
import dave.util.XoRoRNG;
import dave.util.log.Logger;
import dave.util.math.Vec2;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;

public class App
{
	private final Pane mRoot;
	private final Canvas mView;
	private final RunnerController mAI;
	private final World mWorld;
	
	public App()
	{
		RNG rng = new XoRoRNG();
		
		mRoot = new Pane();
		mView = new Canvas();
		mAI = getAI();
		mWorld = new World(5, mAI, rng);

		mRoot.widthProperty().addListener(e -> redraw());
		mRoot.heightProperty().addListener(e -> redraw());
		
		mRoot.getChildren().add(mView);
		
		System.out.println("#" + Utils.repeat("=", mWorld.width()) + "#");
		for(int y = 0 ; y < mWorld.height() ; ++y)
		{
			System.out.print("|");
			
			for(int x = 0 ; x < mWorld.width() ; ++x)
			{
				System.out.print(mWorld.wall(x, y) ? "X" : " ");
			}
			
			System.out.println("|");
		}
		System.out.println("#" + Utils.repeat("=", mWorld.width()) + "#");
	}
	
	public Parent getUI()
	{
		return mRoot;
	}
	
	public void tick()
	{
		if(!mWorld.done())
		{
			mWorld.tick();
			
			Logger.DEFAULT.info("%.1f, %.1f, %.1f -> %.3f", mAI.left(), mAI.forward(), mAI.right(), mWorld.fitness());
			
			redraw();
		}
	}
	
	private void redraw()
	{
		int w = (int) mRoot.getWidth();
		int h = (int) mRoot.getHeight();
		
		if(w <= 0 || h <= 0)
			return;
		
		{
			double r1 = w / (double) h, r2 = mWorld.width() / (double) mWorld.height();
			
			if(r1 >= r2)
			{
				w = (int) (h * r2);
			}
			else
			{
				h = (int) (w / r2);
			}
		}
		
		mView.setWidth(w);
		mView.setHeight(h);
		
		double l = w / (double) mWorld.width();
		
		GraphicsContext gc = mView.getGraphicsContext2D();
		
		for(int y = 0 ; y < mWorld.height() ; ++y)
		{
			for(int x = 0 ; x < mWorld.width() ; ++x)
			{
				fillSquare(gc, x, y, l, mWorld.wall(x, y) ? Color.BLACK : Color.WHITE);
			}
		}
		
		fillSquare(gc, mWorld.position().x, mWorld.position().y, l, Color.RED);
		fillSquare(gc, mWorld.goal().x, mWorld.goal().y, l, Color.GREEN);
		
		Vec2 p = (new Vec2(mWorld.position())).add(new Vec2(0.5, 0.5));
		Vec2 q = p.add((new Vec2(mWorld.view().dx, mWorld.view().dy)).scale(0.5));
		
		strokeLine(gc, p, q, l, Color.BLACK);
	}
	
	private void strokeLine(GraphicsContext gc, Vec2 p, Vec2 q, double l, Paint c)
	{
		gc.setStroke(c);
		gc.strokeLine(p.x * l, p.y * l, q.x * l, q.y * l);
	}
	
	private void fillSquare(GraphicsContext gc, int x, int y, double l, Paint c)
	{
		gc.setFill(c);
		gc.fillRect(x * l, y * l, l, l);
	}
	
	private static RunnerController getAI()
	{
		File best = new File("snapshot.json");
		
		if(best.exists())
		{
			try
			{
				JsonObject json = (JsonObject) JsonUtils.fromFile(best);
				NeuralNet nn = (NeuralNet) JSON.deserialize(json.get("neural-net"));
				
				return new NeuralNetAI(nn);
			}
			catch(IOException e)
			{
				throw new SevereIOException(e);
			}
		}
		else
		{
			return new SimpleRunnerAI();
		}
	}
	
	public static void run(Stage primary)
	{
		App app = new App();
		
		Scene scene = new Scene(app.getUI(), 800, 600);
		
		primary.setTitle("Maze simulator");
		primary.setScene(scene);
		
		scene.setOnKeyPressed(e -> {
			app.tick();
		});
		
		primary.show();
	}
}
