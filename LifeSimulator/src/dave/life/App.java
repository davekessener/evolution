package dave.life;

import java.io.File;

import dave.evo.common.NeuralNet;
import dave.evo.eval.life.AI;
import dave.evo.eval.life.Entity;
import dave.evo.eval.life.LifeOptions;
import dave.evo.eval.life.NeuralNetAI;
import dave.evo.eval.life.SimpleAI;
import dave.evo.eval.life.World;
import dave.json.JSON;
import dave.json.JsonObject;
import dave.json.JsonUtils;
import dave.util.Utils;
import dave.util.XoRoRNG;
import dave.util.config.Configuration;
import dave.util.log.Logger;
import dave.util.math.IVec2;
import dave.util.math.Vec2;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;

public class App
{
	private final Pane mRoot;
	private final Canvas mView;
	private final World mWorld;
	
	public App()
	{
		mRoot = new Pane();
		mView = new Canvas();
		
		Configuration<LifeOptions> config = new Configuration<>(LifeOptions.class);
		
		mWorld = new World(getAI(), new XoRoRNG(), config.proxy());

		mRoot.widthProperty().addListener(o -> redraw());
		mRoot.heightProperty().addListener(o -> redraw());
		
		mRoot.getChildren().add(mView);
	}
	
	public Parent ui()
	{
		return mRoot;
	}
	
	private static AI getAI()
	{
		File best = new File("snapshot.json");
		
		if(best.exists())
		{
			Logger.DEFAULT.info("loading from file");
			
			JsonObject json = (JsonObject) Utils.run(() -> JsonUtils.fromFile(best));
			
			return new NeuralNetAI((NeuralNet) JSON.deserialize(json.get("neural-net")));
		}
		else
		{
			Logger.DEFAULT.info("using simple ai");
			
			return new SimpleAI();
		}
	}
	
	public void tick()
	{
		if(!mWorld.done())
		{
			mWorld.tick();
			
			Entity e = mWorld.entity();
			
			Logger.DEFAULT.info("(%.3f, %.3f) -> (%.3f, %.3f, %.3f): %.3f",
				e.food(), e.water(),
				e.ai().left(), e.ai().forward(), e.ai().right(),
				mWorld.fitness());
			
			redraw();
		}
	}
	
	private void redraw()
	{
		double l = (Math.min(mRoot.getWidth(), mRoot.getHeight()));
		
		if(l <= 0)
			return;
		
		World w = mWorld;
		
		mView.setWidth(l);
		mView.setHeight(l);
		
		double s = l / w.size();
		
		GraphicsContext gc = mView.getGraphicsContext2D();
		
		gc.setFill(Color.BLACK);
		gc.fillRect(0, 0, l, l);
		
		for(int y = 0 ; y < w.height() ; ++y)
		{
			for(int x = 0 ; x < w.width() ; ++x)
			{
				if(!w.wall(x, y))
				{
					fillRect(gc, new IVec2(x, y), s, Color.WHITE.interpolate(Color.GREEN, w.smell(x, y)));
				}
			}
		}
		
		fillRect(gc, w.water(), s, Color.BLUE);
		
		IVec2 p = w.entity().position();
		
		gc.setStroke(Color.GRAY);
		gc.setLineWidth(2);
		gc.strokeRect(p.x * s, p.y * s, s, s);
		
		Vec2 c1 = (new Vec2(p)).add(new Vec2(0.5, 0.5));
		Vec2 c2 = c1.add((new Vec2(w.entity().view().v)).scale(0.5));
		
		gc.setStroke(Color.RED);
		gc.strokeLine(c1.x * s, c1.y * s, c2.x * s, c2.y * s);
	}
	
	private static void fillRect(GraphicsContext gc, IVec2 p, double s, Paint c)
	{
		gc.setFill(c);
		gc.fillRect(p.x * s, p.y * s, s, s);
	}
	
	public static void run(Stage primary)
	{
		App self = new App();
		Scene scene = new Scene(self.ui(), 800, 600);
		
		scene.setOnKeyPressed(e -> {
			switch(e.getCode())
			{
				case SPACE:
					self.tick();
					break;
				
				default:
					break;
			}
		});
		
		primary.setTitle("Life Maze Simulator");
		primary.setScene(scene);
		primary.show();
	}
}
