require 'fileutils'
require 'json'

def reconstitute_hash(h)
	if h.is_a? Hash
		h.map do |k, v|
			[(k.is_a?(String) ? k.to_sym : k), reconstitute_hash(v)]
		end.to_h
	elsif h.is_a? Array
		h.map { |e| reconstitute_hash(e) }
	else
		h
	end
end

def lerp(a, b, f)
	a + f * (b - a)
end

def smooth(a, f)
	d = (1...(a.length-1)).map do |i|
		0.5 * (a[i - 1] + a[i + 1])
	end
	a = a.dup
	(0...d.length).each do |i|
		a[i + 1] = lerp(a[i + 1], d[i], f)
	end
	a
end

class Array
	def sum
		reduce(&:+)
	end
end

class DataPoint
	def initialize(**data)
		@data = data
	end

	def keys
		@data.keys
	end

	def [](k)
		@data[k]
	end

	def map(&block)
		DataPoint.new(@data.map { |k, v| [k, block.call(k, v)] }.to_h)
	end

	def +(dp)
		map { |k, v| v + dp[k] }
	end

	def -(dp)
		self + (dp * -1)
	end

	def *(s)
		map { |k, v| v * s }
	end

	def /(s)
		self * (1.0 / s)
	end

	def **(s)
		map { |k, v| v ** s }
	end

	def to_h
		@data.dup
	end

	def self.build(h)
		DataPoint.new(h)
	end
end

class DataSet
	def initialize
		@data = []
		@average = @deviation = nil
	end

	def add(dp)
		@data.push(dp)
	end

	def max
		DataPoint.new(@data[0].keys.map { |k| [k, @data.map { |e| e[k] }.max] }.to_h)
	end

	def min
		DataPoint.new(@data[0].keys.map { |k| [k, @data.map { |e| e[k] }.min] }.to_h)
	end

	def average
		@average ||= (@data.sum / @data.length)
	end

	def deviation
		@deviation ||= (@data.map { |e| (e - average)**2 }.sum / @data.length)**0.5
	end

	def to_h
		{
			average: average.to_h,
			deviation: deviation.to_h,
			data: @data.map(&:to_h)
		}
	end

	def self.build(h)
		avg, dev, data = h[:average], h[:deviation], h[:data]
		DataSet.new.tap do |ds|
			ds.send :instance_variable_set, :@data, data.map { |d| DataPoint.build(d) }
			ds.send :instance_variable_set, :@average, DataPoint.build(avg)
			ds.send :instance_variable_set, :@deviation, DataPoint.build(dev)
		end
	end
end

class Entry
	attr_reader :points, :tags

	def initialize(tags)
		@tags = tags
		@points = []
	end

	def add(e)
		@points.push(e)
	end

	def to_h
		{
			tags: @tags,
			data: @points.map(&:to_h)
		}
	end

	def self.build(h)
		tags, data = h[:tags], h[:data]
		Entry.new(tags).tap do |e|
			data.each { |p| e.add(DataSet.build(p)) }
		end
	end
end

class Manager
	def initialize
		@data = []
	end

	def add(e)
		@data.push(e)
	end

	def get(*tags)
		@data.select { |e| tags.all? { |t| e.tags.include? t } }
	end

	def inspect
		@data.each { |e| puts "[#{e.tags.map(&:to_s).join(', ')}]" }
	end

	def to_h
		@data.map(&:to_h)
	end

	def self.build(h)
		Manager.new.tap do |m|
			h.each { |e| m.add(Entry.build(e)) }
		end
	end
end

def log(s)
	STDOUT.write("#{s}\n")
	STDOUT.flush
end

