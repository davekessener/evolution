require_relative 'graph'

class Array
	def *(a)
		flat_map { |p| a.map { |q| [p, q] } }
	end
end

def to_ml(a)
	"[#{a.map(&:to_s).join(', ')}]"
end

def n_sample(n, a)
	int = ->(x) {
		x *= a.length.to_f
		p = x - x.floor
		i1 = x.floor
		i2 = i1 + 1

		if i1 < 0
			a[0]
		elsif i2 >= a.length
			a[-1]
		else
			a[i1] + p * (a[i2] - a[i1])
		end
	}

	(0...n).map { |i| int.(i / (n-1).to_f) }
end

data = reconstitute_hash(JSON.parse(File.open('results.json', 'r', &:read)))
m = Manager.build(data)

i = 0
n = (ARGV[0] || "500").to_i
File.open('display.m', 'w') do |f|
	i += 1

	f.write("figure('visible', 'off')\n\n")

#	data.each do |entry|
#		e = entry[:data]
#
#		f.write("y = [#{e.map { |t| t[:average][:fitness].to_s }.join(', ')}]\n")
#		f.write("plot(y)\n")
#		f.write("print -dpng img/#{entry[:tags].join('_')}_#{i}.png\n\n")
#	end

#	f.write("x = #{to_ml(n_sample(100, (1..500).to_a))}\n\n")
	f.write("x = [1:#{n}]\n\n")

	goals = ['counting', 'maze.simple', 'sequencing']
	gas = ['flat-nn', 'neat', 'es-hyper-neat']
	mem = ['rnn', 'lstm', 'gru-mb']
	((goals * gas) + (goals * mem) + (gas * mem)).each do |tags|
		puts tags.join(' ')

		counting = m.get(*tags)
		
		raise "ERROR" if counting.empty?

		(0...counting.length).each do |i|
			plot = counting[i].points.map { |p| p.average[:fitness] }
			max = 0
#			plot.map! { |v| max = [max, v].max }
			f.write("y#{i} = #{to_ml plot[0...n]}\n")
			plot = counting[i].points.map { |p| p.deviation[:fitness] }
			f.write("e#{i} = #{to_ml plot[0...n]}\n")
		end
	
#		f.write("\nplot(#{(0...counting.length).map { |i| "x, y#{i}, '-'" }.join(', ')})\n")
		f.write("\nerrorbar(#{(0...counting.length).map { |i| "x, y#{i}, e#{i}, '~'" }.join(', ')})\n")
		f.write("axis([0, #{n}, 0, 1.5])\n")
		f.write("lg = legend(#{counting.map { |e| "'#{e.tags.join(' ')}'" }.join(', ')})\n")
		f.write("legend(lg, 'location', 'southeast')\n")
		f.write("print -dpng img/result_#{tags.join('_')}.png\n\n")
	end

	puts "# ====================================="

	f.write("x = #{to_ml(n_sample(100, (1..500).to_a))}\n\n")

	((goals * gas) * mem).each do |tags|
		puts tags.join(' ')

		counting = m.get(*tags.flatten)

		if counting.length != 1
			puts "[WARN] #{counting.length} ENTRIES FOR #{tags.join(' ')}!"
			next
		end

		plot = [
			counting[0].points.map { |p| p.min[:fitness] },
			counting[0].points.map { |p| p.average[:fitness] },
			counting[0].points.map { |p| p.max[:fitness] },
			counting[0].points.map { |p| p.deviation[:fitness] }
		]

		plot.map! { |p| n_sample(100, p) }

		(0...plot.length).each do |i|
			f.write("y#{i} = #{to_ml plot[i][0...n]}\n")
		end

		f.write("\nerrorbar(x, y1, y3, '~')\n")
		f.write("hold on\n")
		f.write("plot(x, y0)\n")
		f.write("plot(x, y2)\n")
		f.write("axis([0, #{n}, 0, 1.5])\n")
		f.write("hold off\n")
		f.write("print -dpng img/single_#{tags.join('_')}.png\n\n")
	end
end

