require_relative 'graph'

m = Manager.new
tmp = {}

Dir.glob('*.txt') do |f|
	tags = f.split(/__/)[0...3]
	id = tags.join('__')
	e = []
	
	log "Reading '#{tags.join('__')}_#{f.split(/__/)[-1]}' ..."

	File.open(f, 'r', &:read).split(/\n/).each do |line|
		line.strip!

		next if line.empty?

		if line =~ /^population: counter=([0-9]+), time=([0-9]+)ms, highscore=([0-9]+(\.[0-9]+)?)$/
			idx, time, fitness = $1.to_i, $2.to_i, $3.to_f

			e.push({
				idx: idx,
				time: time,
				fitness: fitness
			})
		else
			puts "[WARN] Could not match '#{line}'!"
		end
	end
	
	(tmp[id] ||= []).push(e)
end

log "Processing ..."

tmp.each do |id, e|
#	next if e.length != 40

#	l = e.map { |t| t.length }.max
	l = 200

	e.each do |t|
		raise "WTF: #{id}" if t.empty?

		while t.length < l
			t.push({
				idx: t[-1][:idx] + 1,
				time: 0,
				fitness: t[-1][:fitness]
			})
		end
	end

	entry = Entry.new(id.split(/__/))

	(0...l).each do |i|
		d = DataSet.new

		e.each do |t|
			d.add(DataPoint.new(t[i]))
		end

		entry.add(d)
	end

	m.add(entry)
end

log "Writing result ..."

data = m.to_h

File.open('results.json', 'w') do |f|
	f.write(JSON.pretty_generate(data))
end


