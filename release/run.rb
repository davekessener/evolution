require 'fileutils'
require 'json'

class Configuration
	attr_reader :filename

	def initialize(fn)
		@filename = fn
		@config = {}
	end

	def [](id)
		@config[id]
	end

	def []=(id, v)
		@config[id] = v
	end

	def save
		File.open(@filename, 'w') do |f|
			f.write(JSON.pretty_generate(@config))
		end
	end
end

class Part
	attr_reader :name

	def initialize(name, &block)
		@name, @callback = name, block
	end

	def apply
		@callback.call
	end
end

@memory_models = [
	Part.new('rnn') { }
]

['lstm', 'gru-mb'].each do |mem|
	@memory_models.push(Part.new(mem) do
		$script['module.memory'] = { type: "memory.#{mem}", capacity: ($params[:memory_capacity] || 5) }
	end)
end

@gas = [
    Part.new('flat-nn') do
        $script['module.flat-nn'] = nil
    end,
	Part.new('neat') { },
	Part.new('es-hyper-neat') do
		$script['module.es-hyper-neat'] = nil
	end
]

@goals = ['counting', 'sequencing', 'maze.simple'].map do |goal|
	Part.new(goal) do
		$script["module.goal"] = { goal: "goal.#{goal}" }
		$params[:memory_capacity] = (goal == 'sequencing' ? 15 : 3)
	end
end

MAX_GENS = 500
MAX_FITNESS = 0.99

def default_script
	Configuration.new('script.json').tap do |cfg|
		cfg['module.control'] = {
			'policy.limited' => {
				'max-tries' => MAX_GENS,
				'max-fitness' => MAX_FITNESS
			},
			'policy.logging' => nil
		}
	end
end

def default_config
	Configuration.new('config.json').tap do |cfg|
		cfg['quit_on_success'] = true
		cfg['save_progress'] = false
	end
end

def log(s)
    STDOUT.write(s)
    STDOUT.flush
end

@gas.each do |gas|
	@goals.each do |goal|
		@memory_models.each do |mem|
			10.times do |i|
				if File.exist? 'done.flag'
					log("ABORTING!\n")
					exit
				end

				$params = {}
				$script = default_script
				$config = default_config
	
				goal.apply
				gas.apply
				mem.apply
				
				id = "#{goal.name} #{gas.name} #{mem.name} #{$params.each.map { |k, v| "#{k}.#{v}" }.join('_')} #{'%03d' % i}"
				id = id.strip.gsub(/ +/, '__')
				fn = "#{id}.txt"
				tmp = "#{id}.tmp"
	
				$config.save
				$script.save
	
				log("#{Time.now.strftime('%Y-%m-%d %H:%M:%S')} Running #{goal.name} with #{gas.name} and #{mem.name} (#{i})\n")
	
				if not File.exist? fn
					FileUtils.rm_f('state.json')
	
					cmd = "java -jar ../evolution.jar --config \"#{$config.filename}\" --script \"#{$script.filename}\""
                    File.open("#{id}.log", 'w') do |log_f|
                        File.open(tmp, 'w') do |tmp_f|
                            IO.popen(cmd) do |io|
                                io.each do |line|
                                    log_f.write(line)

                                    if line =~ /^population:/
                                        tmp_f.write(line)

                                        if line =~ /counter=([0-9]+)/
                                            c = $1.to_i
                                            log("#{'%.1f%%' % (100 * c.to_f / MAX_GENS)}     \r")
                                        end
                                    end
                                end

                                io.close
                            end
                        end
                    end
	
					raise unless $?.success?
	
					FileUtils.mv(tmp, fn)
				end
			end
		end
	end
end

